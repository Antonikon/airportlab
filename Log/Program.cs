﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Network;
using System.IO;

namespace Log
{
    public class Log : BasicService
    {
        List<StreamWriter> Writers = new List<StreamWriter>();
        static string[] Levels = {"ERROR", "WARNING", "INFO", "DEBUG"};
        public int MaxLogLevel = 3;

        public Log():
            base("Log") //как обращаются другие
        {
            var time = DateTime.Now;
            string prefix = $"{time.Year}-{time.Month}-{time.Day}-{time.Hour}-{time.Minute}-";
            Directory.CreateDirectory("Logs");
            Directory.SetCurrentDirectory("Logs");
            foreach (var name in Levels) {
                var fullName = prefix + name + ".txt";
                Writers.Add(new StreamWriter(fullName));
            }

            Bind<string>("Write", WriteLog);
            Bind<Tuple<int, string>>("AddLog", AddLog);
            Console.WriteLine("Logs started");
        }

        public void WriteLog(string str) {
            AddLog(new Tuple<int, string>(3, str));
        }

        public void AddLog(Tuple<int, string> args) {
            int level = args.Item1;
            if (level < 0) level = 0;
            if (level > MaxLogLevel) return;
            if (level >= Levels.Length) level = Levels.Length - 1;
            Writers[level].WriteLine($"{DateTime.Now.ToString("HH:mm:ss")}: {args.Item2}");
            Writers[level].Flush();
            Console.WriteLine($"[{Levels[level]}] {args.Item2}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Config.IsLocal = false;
            var l = new Log();
            Console.WriteLine("Enter max log level: [default = 3]");
            l.MaxLogLevel = int.Parse(Console.ReadLine());
            l.Start(false);
        }
    }
}
