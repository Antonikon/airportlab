﻿using Platform.Network;
using System;
using System.Collections.Generic;
using System.IO;

namespace Aircraft_Ground_Handling
{
    using L = LogWriter;
    /*
     Самолет: Plane
        Список от самолета для обслуживания:  Людей выгрузить , Багаж выгрузить в кг, Топливо залить, Тип самолета, Рейс
        (int,int,int,int,string) кортеж на 5 элементов.
        тип 1 - гражданский, 2 - другой (только топливо)
        Приходит запрос на обслуживание - значит самолет уже сидит и ждет, надо отправить follow me.


        Накопитель
        после конца регистрации получает автобусы с номером рейса и выгружает в них соответствующих людей


        Регистрация 
        Отпоравляет мне количество каждого типа еды (3 типа: 1,2,3. Regular, Vegan, Cheap)
        Tuple<string,int,int,int> рейс и количество каждого типа еды для Catering
     
     */


    public static class LogWriter
    {
        private static readonly string Path = Environment.CurrentDirectory + "\\log.log";
        public static bool WriteLog;
        private static Serv _s;
        public static void Clear()
        {
            File.WriteAllText(Path, string.Empty);
        }

        public static void SetServ(Serv serv)
        {
            _s = serv;
        }
        public static void Msg(string msg)
        {
            if (!WriteLog) return;
            _s.RemoteCall("Log", "Write", "AGH " + msg);
            try
            {
                using (StreamWriter w = File.AppendText(Path))
                {
                    w.WriteLine("AGH    " + DateTime.Now + "    " + msg);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

    public class Plane
    {
        public int PeopleToUnload;
        public int LuggageToUnload;
        public int FuelToRefill;
        public int PlaneType;
        public string Flight;

        public bool ReadyForLoad;
        public bool WasCalled;

        public int[] Food = new int[3];
        //Stages:
        //meet and greet 
        //Unload people/luggage 
        public bool PeopleUnloaded;
        public bool LuggageUnloaded;
        //Refuel
        //Load people/luggage/food
        public bool PeopleLoaded;
        public bool LuggageLoaded;
        public bool FoodLoaded;
        //deice
        //takeoff

        public bool ReadyToGo;

        public int PeopleToLoad;
        public bool HasLuggageToLoad;
        public Plane(int peopleToUnload, int luggageToUnload, int fuelToRefill, int planeType, string flight)
        {
            this.PeopleToUnload = peopleToUnload;
            this.LuggageToUnload = luggageToUnload;
            this.FuelToRefill = fuelToRefill;
            this.PlaneType = planeType;
            this.Flight = flight;
        }
    }

    public class Serv : BasicService
    {
        public const int ParkingSize = 3;
        public const int ParkingLowest = 41;
        public Serv() : base("AGH")
        {
            Bind<(int, int, int, int, string)>("SendNeeds", PlaneNeeds);
            Bind<(string, int, int, int, int, bool)>("RegistrationAndFood", RegistrationAndFood);
            Bind<int>("PlaneParked", PlaneParked);
            Bind<int>("PassengersUnloaded", PassengersUnloaded);
            Bind<int>("LuggageUnloaded", LuggageUnloaded);
            Bind<int>("Fueled", Fueled);
            Bind<int>("PeopleLoaded", PeopleLoaded);
            Bind<int>("LuggageLoaded", LuggageLoaded);
            Bind<int>("FoodLoaded", FoodLoaded);
            Bind<(int, bool)>("Deiced", Deiced);
            Bind<string>("ReadyToTakeoff", ReadyToTakeoff);
            Bind<int>("Taken", Taken);
            Parking.Add(ParkingLowest, null);
            Parking.Add(ParkingLowest + 1, null);
            Parking.Add(ParkingLowest + 2, null);
            UnloadBusCount.Add(ParkingLowest, 0);
            UnloadBusCount.Add(ParkingLowest + 1, 0);
            UnloadBusCount.Add(ParkingLowest + 2, 0);
            LoadBusCount.Add(ParkingLowest, 0);
            LoadBusCount.Add(ParkingLowest + 1, 0);
            LoadBusCount.Add(ParkingLowest + 2, 0);
        }
        public List<Plane> ParkingQueue = new List<Plane>();
        public Dictionary<int, Plane> Parking = new Dictionary<int, Plane>();
        public Dictionary<int, int> UnloadBusCount = new Dictionary<int, int>();
        public Dictionary<int, int> LoadBusCount = new Dictionary<int, int>();

        public void PlaneNeeds((int, int, int, int, string) needs)
        {
            L.Msg($"Flight: {needs.Item5} arrived. Type: {needs.Item4}");
            Plane plane = new Plane(needs.Item1, needs.Item2, needs.Item3, needs.Item4, needs.Item5);
            Console.WriteLine(plane.Flight);
            //RemoteCall("test", "AddPlane", needs); //TODO for test

            for (int i = ParkingLowest; i < ParkingLowest + ParkingSize; i++)
            {
                if (Parking[i] == null)
                {
                    //RemoteCall("test", "ParkToSlot", (i, plane.Flight)); 
                    RemoteCall("FollowMe", "ParkToSlot", (i, plane.Flight));
                    Parking[i] = plane;
                    L.Msg($"Followme Flight: {needs.Item5} to Parking Slot: {i}");
                    return;
                }
            }


            //RemoteCall("test", "EscortToQueue", plane.Flight); 
            RemoteCall("FollowMe", "EscortToQueue", plane.Flight);
            ParkingQueue.Add(plane);
            L.Msg($"Followme Flight: {needs.Item5} to Queue");
        }

        public void PlaneParked(int parkingId)
        {
            if (Parking[parkingId].PlaneType == 2)
            {
                L.Msg($"Flight: {Parking[parkingId].Flight} parked to Slot: {parkingId}");
                CallForFuel(parkingId);
                L.Msg($"Call fuel for Flight: {Parking[parkingId].Flight} to Slot: {parkingId} for Fuel: {Parking[parkingId].FuelToRefill}");
            }
            else
            {
                L.Msg($"Flight: {Parking[parkingId].Flight} parked to Slot: {parkingId}");
                if (Parking[parkingId].PeopleToUnload > 0)
                {
                    UnloadBusCount[parkingId] = (int)Math.Ceiling(Parking[parkingId].PeopleToUnload / 30f);
                    //RemoteCall("test", "UnloadPeople", (parkingId, Parking[parkingId].PeopleToUnload, Parking[parkingId].Flight)); //TODO call for p/l unload 
                    RemoteCall("Bus", "UnloadPeople", (parkingId, Parking[parkingId].PeopleToUnload, Parking[parkingId].Flight)); //TODO call for p/l unload 
                    L.Msg($"Call unload passengers for Flight: {Parking[parkingId].Flight} to Slot: {parkingId}. Amount: {Parking[parkingId].PeopleToUnload}");
                }
                else
                {
                    PassengersUnloaded(parkingId);
                }

                if (Parking[parkingId].LuggageToUnload > 0)
                {
                    //RemoteCall("test", "UnloadLuggage", (parkingId, Parking[parkingId].Flight)); //TODO call for p/l unload 
                    RemoteCall("Luggage", "UnloadLuggage", (parkingId, Parking[parkingId].Flight)); //TODO call for p/l unload 
                    L.Msg($"Call unload luggage for Flight: {Parking[parkingId].Flight} to Slot: {parkingId}. Bags: {Parking[parkingId].LuggageToUnload}");
                }
                else
                {
                    LuggageUnloaded(parkingId);
                }
            }
        }

        public void PassengersUnloaded(int parkingId)
        {
            var plane = Parking[parkingId];
            if (plane.PeopleToUnload != 0)
            {
                UnloadBusCount[parkingId]--;

                if (UnloadBusCount[parkingId] < 1)
                {
                    plane.PeopleToUnload = 0;
                    plane.PeopleUnloaded = true;
                    L.Msg($"People unloaded for Flight: {Parking[parkingId].Flight}, Slot: {parkingId}");
                }
            }
            else
            {
                plane.PeopleUnloaded = true;
            }
            if (plane.PeopleUnloaded && plane.LuggageUnloaded)
            {
                CallForFuel(parkingId);
            }
        }

        public void LuggageUnloaded(int parkingId)
        {
            var plane = Parking[parkingId];
            plane.LuggageUnloaded = true;
            if (plane.LuggageToUnload != 0)
            {
                plane.LuggageToUnload = 0;
                L.Msg($"Luggage unloaded for Flight: {Parking[parkingId].Flight}, Slot: {parkingId}");

            }

            if (plane.PeopleUnloaded && plane.LuggageUnloaded)
            {
                CallForFuel(parkingId);
            }
        }

        public void CallForFuel(int parkingId)
        {
            L.Msg($"Call Fuel: {Parking[parkingId].FuelToRefill} for Flight: {Parking[parkingId].Flight} in Slot: {parkingId}");
            //RemoteCall("test", "GetFuel", (parkingId, Parking[parkingId].FuelToRefill));
            RemoteCall("Tanker", "set_job", (parkingId, Parking[parkingId].FuelToRefill));
            //TODO test
        }

        public void Fueled(int parkingId)
        {
            if (Parking[parkingId].PlaneType == 2)
            {
                Parking[parkingId].FuelToRefill = 0;
                //RemoteCall("test", "Deice", parkingId); //TODO deice
                RemoteCall("Deicing", "set_job", parkingId); //TODO deice
                L.Msg($"Flight: {Parking[parkingId].Flight} in Slot: {parkingId} is fueled, Call deice");
            }
            else
            {
                Parking[parkingId].FuelToRefill = 0;
                L.Msg($"Flight: {Parking[parkingId].Flight} in Slot: {parkingId} is fueled, waiting for registration call");
                Parking[parkingId].ReadyForLoad = true;
                if (Parking[parkingId].WasCalled)
                {
                    L.Msg($"Flight: {Parking[parkingId].Flight} in Slot: {parkingId} was already called, Call LoadAll");
                    LoadAll(parkingId, Parking[parkingId].Flight, Parking[parkingId].Food, Parking[parkingId].PeopleToLoad, Parking[parkingId].HasLuggageToLoad);
                }
            }
            RemoteCall("Plane", "Fuel", Parking[parkingId].Flight);
        }

        public void LoadAll(int parkingId, string flight, int[] food, int people, bool hasLuggage)
        {
            if (people != 0)
            {

                LoadBusCount[parkingId] = (int)Math.Ceiling(people / 30f);
                //RemoteCall("test", "LoadPeople", (parkingId, flight, people)); //TODO summon cars for p/l  from accumulator and food Tuple<int,int,int,int> индекс ноды и 3 вида еды
                RemoteCall("Bus", "LoadPeople", (parkingId, flight, people)); //TODO summon cars for p/l  from accumulator and food Tuple<int,int,int,int> индекс ноды и 3 вида еды
            }
            else
            {
                PeopleLoaded(parkingId);
            }

            if (hasLuggage)
            {
                //RemoteCall("test", "LoadLuggage", (parkingId, flight));
                RemoteCall("Luggage", "LoadLuggage", (parkingId, flight));
            }
            else
            {
                LuggageLoaded(parkingId);
            }

            if (food[0] > 0 || food[1] > 0 || food[2] > 0)
            {
                //RemoteCall("test", "set_job", (parkingId,  food[0], food[1], food[2]));
                RemoteCall("Catering", "set_job", (parkingId, food[0], food[1], food[2]));
            }
            else
            {
                FoodLoaded(parkingId);
            }

        }

        public void RegistrationAndFood((string, int, int, int, int, bool) data)
        {
            try
            {
                int parkingId = -1;
                Plane plane = null;

                for (int i = 41; i < 44; i++)
                {
                    if (Parking[i] != null)
                    {
                        if (Parking[i].Flight == data.Item1)
                        {
                            parkingId = i;
                            plane = Parking[i];
                            break;
                        }
                    }
                }


                foreach (Plane queuePlane in ParkingQueue)
                {
                    if (queuePlane.Flight == data.Item1)
                    {
                        plane = queuePlane;
                        parkingId = 0;
                        break;
                    }
                }


                if (plane == null)
                {
                    L.Msg($"!!!!!!!Plane required by registration not found!!!!!!!");
                    return;
                }

                switch (parkingId)
                {
                    case 0:
                        L.Msg($"Registration ended for Flight: {data.Item1} in Queue and Food ( 1: {data.Item2}, 2: {data.Item3}, 3: {data.Item4}");

                        break;
                    case -1:
                        L.Msg($"!!!!!!!Registration ended for Flight: {data.Item1} THAT IS NOT FOUND and Food ( 1: {data.Item2}, 2: {data.Item3}, 3: {data.Item4}!!!!!!!");

                        break;
                    default:
                        L.Msg($"Registration ended for Flight: {data.Item1} in Slot: {parkingId} and Food ( 1: {data.Item2}, 2: {data.Item3}, 3: {data.Item4}");

                        break;
                }

                plane.Food[0] = data.Item2;
                plane.Food[1] = data.Item3;
                plane.Food[2] = data.Item4;
                plane.PeopleToLoad = data.Item5;
                plane.HasLuggageToLoad = data.Item6;

                if (plane.ReadyForLoad)
                {
                    L.Msg($"Call loadAll for Flight: {data.Item1} in Slot: {parkingId}");
                    LoadAll(parkingId, data.Item1, plane.Food, plane.PeopleToLoad, plane.HasLuggageToLoad);
                }
                else
                {
                    L.Msg($"Plane: {data.Item1} was called by registration, but not ready");
                    plane.WasCalled = true;
                }
            }
            catch (Exception e)
            {
                string msg = $"!!!!!!Registration ended for non-existing Flight: {data.Item1}!!!!!!";
                L.Msg(msg);
                Console.WriteLine(msg);
                Console.WriteLine(e);
                throw;
            }

        }

        public void PeopleLoaded(int parkingId)
        {
            LoadBusCount[parkingId]--;
            L.Msg($"People loaded for Flight: {Parking[parkingId].Flight} in Slot: {parkingId}");
            var plane = Parking[parkingId];
            plane.PeopleLoaded = true;
            if (plane.PeopleLoaded && plane.LuggageLoaded && plane.FoodLoaded && LoadBusCount[parkingId] == 0)
            {
                DeicePassenger(parkingId, Parking[parkingId].Flight);
            }
        }

        public void LuggageLoaded(int parkingId)
        {
            L.Msg($"Luggage loaded for Flight: {Parking[parkingId].Flight} in Slot: {parkingId}");
            var plane = Parking[parkingId];
            plane.LuggageLoaded = true;
            if (plane.PeopleLoaded && plane.LuggageLoaded && plane.FoodLoaded)
            {
                DeicePassenger(parkingId, Parking[parkingId].Flight);
            }
        }

        public void FoodLoaded(int parkingId)
        {
            L.Msg($"Food loaded to Flight: {Parking[parkingId].Flight} in Slot: {parkingId}");
            var plane = Parking[parkingId];
            plane.FoodLoaded = true;
            if (plane.PeopleLoaded && plane.LuggageLoaded && plane.FoodLoaded)
            {
                DeicePassenger(parkingId, Parking[parkingId].Flight);
            }

        }

        public void DeicePassenger(int parkingId, string flight)
        {
            L.Msg($"Load complete for Flight: {flight} in Slot: {parkingId}, Deice called");
            //RemoteCall("test", "Deice", parkingId); //TODO deice
            RemoteCall("Deicing", "set_job", parkingId); //TODO deice
        }

        public void Deiced((int, bool) work)
        {
            if (work.Item2)
            {
                L.Msg($"Deicing has finished with Flight: {Parking[work.Item1].Flight} in Slot: {work.Item1}");
            }
            else
            {
                L.Msg($"Deicing decided not to cover Flight: {Parking[work.Item1].Flight} in Slot: {work.Item1} with chemicals");
            }
            var plane = Parking[work.Item1];
            plane.ReadyToGo = true;
            //RemoteCall("test", "EscortToTakeoff", (work.Item1, plane.Flight));    
            RemoteCall("FollowMe", "EscortToTakeoff", (work.Item1, plane.Flight));
            L.Msg($"Followme Flight: {Parking[work.Item1].Flight} in Slot: {work.Item1} to escort to takeoff");
        }

        public void Taken(int parkingId)
        {
            Parking[parkingId] = null;
            if (ParkingQueue.Count > 0)
            {
                // RemoteCall("test", "EscortFromQueue", (work.Item1, ParkingQueue[0].Flight)); 
                RemoteCall("FollowMe", "EscortFromQueue", (parkingId, ParkingQueue[0].Flight));
                L.Msg($"Followme Flight: {ParkingQueue[0].Flight} to proceed from Queue to Slot: {parkingId}");
                Parking[parkingId] = ParkingQueue[0];
                ParkingQueue.RemoveAt(0);
            }
        }

        public void ReadyToTakeoff(string flight)
        {
            L.Msg($"Plane: {flight} is ready to take off");
            RemoteCall("Plane", "Takeoff", flight);
            L.Msg($"Plane: {flight} took off");
        }
    }

    class Program
    {
        public static void StartMain()
        {
            Config.IsLocal = false;
            Serv serv = new Serv();
            L.SetServ(serv);
            serv.Start(true);
            Console.WriteLine(Environment.NewLine + "Service is up!");
            L.Msg("Service is up!");
        }
        public static void StartTest()
        {
            Test test = new Test();
            test.StartTesting();
            Console.WriteLine(Environment.NewLine + "Test service is up!");
        }
        static void Main()
        {
            Console.WriteLine("Press 1 to start main service. Press 2 for main and test. Press 3 for test");
            Console.WriteLine("Startup order for testing: 1 -> 2");
            L.WriteLog = true;
            L.Clear();


            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D1:
                    StartMain();
                    break;
                case ConsoleKey.D2:
                    StartMain();
                    StartTest();
                    break;
                case ConsoleKey.D3:
                    StartTest();
                    break;
            }
        }
    }

}
