﻿using Platform.Network;
using System;
using System.Collections.Generic;
using System.Threading;


namespace Aircraft_Ground_Handling
{
    class PlaneTest
    {
        public string Flight;
        public string Guid;
        public int Node;
        public int People;

        public PlaneTest(string flight, string guid, int people)
        {
            this.Flight = flight;
            this.Guid = guid;
            Node = 2;
            this.People = people;
        }
    }

    //class Ar
    //{
    //    public string flight;
    //    public int flight;
    //    public int flight;
    //    public int flight;

    //}

    class TestService : BasicService
    {

        public TestService() : base("test")
        {
            Bind<(int, string)>("ParkToSlot", ParkToSlot);
            Bind<(int, int, string)>("UnloadPeople", UnloadPeople);
            Bind<(int, string)>("UnloadLuggage", UnloadLuggage);
            Bind<(int, int)>("GetFuel", GetFuel);
            Bind<(int, string, int)>("LoadPeople", LoadPeople);
            Bind<(int, string)>("LoadLuggage", LoadLuggage);
            Bind<(int, string, int, int, int)>("LoadFood", LoadFood);
            Bind<(int, string)>("EscortToTakeoff", EscortToTakeoff);
            Bind<int>("Deice", Deice);
            Bind<string>("EscortToQueue", EscortToQueue);
            Bind<(int, string)>("EscortFromQueue", EscortFromQueue);
            Bind<(int, int, int, int, string)>("AddPlane", AddPlane);

        }

        //public Dictionary<string, PlaneTest> Planes = new Dictionary<string, PlaneTest>();
        public Dictionary<string, Plane> PlanesList = new Dictionary<string, Plane>();
        //TODO fix for multiple buses

        public void AddPlane((int, int, int, int, string) needs)
        {
            Plane p = new Plane(needs.Item1, needs.Item2, needs.Item3, needs.Item4, needs.Item5);
            PlanesList.Add(needs.Item5, p);
            RemoteCall("AGH", "RegistrationAndFood", (needs.Item5, 10, 15, 20, 40, true));
            PlanesList.Remove(needs.Item5);
        }


        public void ParkToSlot((int, string) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "PlaneParked", data.Item1);
        }
        public void UnloadPeople((int, int, string) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "PassengersUnloaded", data.Item1);
        }
        public void UnloadLuggage((int, string) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "LuggageUnloaded", data.Item1);
        }
        public void GetFuel((int, int) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "Fueled", data.Item1);
        }
        public void LoadPeople((int, string, int) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "PeopleLoaded", data.Item1);
        }
        public void LoadLuggage((int, string) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "LuggageLoaded", data.Item1);
        }
        public void LoadFood((int, string, int, int, int) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "FoodLoaded", data.Item1);
        }

        public void Deice(int parkingId)
        {
            Thread.Sleep(100);
            Random random = new Random();
            if (random.Next(2) == 0)
            {
                RemoteCall("AGH", "Deiced", (parkingId, false));
            }
            else
            {
                RemoteCall("AGH", "Deiced", (parkingId, true));
            }
        }

        public void EscortToTakeoff((int, string) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "ReadyToTakeoff", data.Item2);
        }

        public void EscortToQueue(string flight)
        {
            Thread.Sleep(100);
        }

        public void EscortFromQueue((int, string) data)
        {
            Thread.Sleep(100);
            RemoteCall("AGH", "PlaneParked", data.Item1);
        }

    }
    class Test
    {
        public void StartTesting()
        {
            Config.IsLocal = false;
            TestService s = new TestService();
            s.Start(true);
        }
    }
}
