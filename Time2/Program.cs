﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;

using Platform.Network;

namespace Time {
    internal struct Callback {
        public string Service;
        public string Method;
    }

    public class TimeService : BasicService {
        DateTime CurrentTime = new DateTime(2020, 1, 1, 6, 0, 0);
        const int UPS = 20;
        ulong TimeStep = 60;

        SortedDictionary<DateTime, List<Callback>> Events = new SortedDictionary<DateTime, List<Callback>>();
        Dictionary<string, string> MultSubscribers = new Dictionary<string, string>();
        object LockObject = new object();

        public TimeService() :
            base("Time") 
        {
            Bind<Tuple<string, string, DateTime>>("TimeSubscribe", AddEvent);
            Bind<Tuple<string, string>>("MultSubscribe", AddMultSubscriber);
            Bind("WhatTime", GetTime);
            //Bind<UInt64>("set_time_step", SetTimeStep);
            Bind("WhatMult", GetTimeStep);
        }

        //service, method, time
        void AddEvent(Tuple<string, string, DateTime> args) {
            var entry = new Callback();
            entry.Service = args.Item1;
            entry.Method = args.Item2;

            lock (LockObject) {
                if (!Events.ContainsKey(args.Item3)) {
                    Events[args.Item3] = new List<Callback>();
                }
                Events[args.Item3].Add(entry);
            }

            Console.WriteLine($"'{entry.Service}' push method '{entry.Method}' on {args.Item1}");
        }

        void AddMultSubscriber(Tuple <string, string> args) {
            lock (LockObject) {
                MultSubscribers[args.Item1] = args.Item2;
            }
            Console.WriteLine($"'{args.Item1}' push method {args.Item2} on mult change");
        }

        void SetTimeStep(ulong step) {
            lock (LockObject) {
                TimeStep = step;
                int formattedStep = (int)(step / 60);
                foreach (var sub in MultSubscribers) {
                    RemoteCall(sub.Key, sub.Value, formattedStep);
                    Console.WriteLine($"New multipler: Calling {sub.Key}->{sub.Value}");
                }
            }
        }

        int GetTimeStep() {
            lock (LockObject) {
                int formattedStep = (int)(TimeStep / 60);
                return formattedStep;
            }
        }

        DateTime GetTime() {
            lock (LockObject) {
                return CurrentTime;
            }
        }

        public void Run() {
            Start(true);

            Console.WriteLine("Time service started");
            while (true) {
                lock (LockObject) {
                    CurrentTime = CurrentTime.AddSeconds(TimeStep / (float)UPS);
                    while (Events.Count > 0) {
                        var callbackList = Events.First();
                        if (callbackList.Key > CurrentTime) break;
                        foreach (var callback in callbackList.Value) {
                            Console.WriteLine($"{CurrentTime}: calling {callback.Service}->{callback.Method}");
                            RemoteCall(callback.Service, callback.Method, CurrentTime);
                        }
                        Events.Remove(callbackList.Key);
                    }
                }
                Thread.Sleep(1000 / UPS);
            }
        }

        public void ConsoleLoop() {
            while (true) {
                var input = Console.ReadLine();
                int number;
                if (int.TryParse(input, out number)) {
                    SetTimeStep((ulong)number * 60);
                }
            }
        }
    }

    public static class TimeWorker {
        public static void Main(string[] args) {
            Config.IsLocal = false;
            var time = new TimeService();
            Task.Factory.StartNew(time.ConsoleLoop);
            Config.LogLevel = 1;
            time.Run();
        }
    }
}
