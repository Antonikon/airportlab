﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;
using Platform.Network;
using Platform;
using GraphPath;

namespace Deicing
{   
    public static class Weather
    {
        public static int temperature = 1;
        public static object lock_temp = new object();
        public static void CitySelection()
        {
            WebRequest request;
            Console.WriteLine("1) Москва\n" +
                "2) Лондон\n" +
                "3) Сеул\n" +
                "4) Вашингтон\n" +
                "5)Иркутск\n" +
                "6) Нью-Йорк\n" +
                "7) Амстердам\n" +
                "8) Прага\n" +
                "9) Ванкувер\n" +
                "10) Кейптаун\n" +
                "11) Санкт - Петербург\n" +
                "12) Сидней\n" +
                "13) Токио\n" +
                "14) Шанхай\n" +
                "15) Дели\n" +
                "16) Мехико\n" +
                "17) Иерусалим\n" +
                "18) Афины\n" +
                "19) Осло\n" +
                "20) Цюрих\n" +
                "21) Копенгаген\n" +
                "22) Пекин\n" +
                "23) Сингапур\n" +
                "24) Гонконг\n" +
                "25) Женева\n" +
                "26.Париж\n" +
                "27.Рим\n" +
                "28.Венеция\n" +
                "29.Рио-де-Жанейро\n" +
                "30.Стамбул");


            string city = Console.ReadLine();
            switch (city)
            {
                case "1":
                    request = WebRequest.Create(@"http://www.meteoservice.ru/weather/now/moskva");
                    break;
                case "2":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/london");
                    break;
                case "3":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/seoul");
                    break;
                case "4":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/washington");
                    break;
                case "5":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/irkutsk");
                    break;
                case "6":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/new-york");
                    break;
                case "7":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/amsterdam");
                    break;
                case "8":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/praha");
                    break;
                case "9":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/vancouver");
                    break;
                case "10":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/capetown");
                    break;
                case "11":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/sankt-peterburg");
                    break;
                case "12":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/sidney");
                    break;
                case "13":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/tokyo");
                    break;
                case "14":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/shanghai");
                    break;
                case "15":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/delhi");
                    break;
                case "16":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/mexico");
                    break;
                case "17":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/jerusalem");
                    break;
                case "18":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/athens");
                    break;
                case "19":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/oslo");
                    break;
                case "20":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/zurich");
                    break;
                case "21":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/kobenhagen");
                    break;
                case "22":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/beijing");
                    break;
                case "23":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/singapore");
                    break;
                case "24":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/honkong");
                    break;
                case "25":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/geneve");
                    break;
                case "26":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/paris");
                    break;
                case "27":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/rome");
                    break;
                case "28":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/venezia");
                    break;
                case "29":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/rio");
                    break;
                case "30":
                    request = WebRequest.Create(@"https://www.meteoservice.ru/weather/overview/istanbul");
                    break;

                default:
                    Console.WriteLine("По умолчанию выбран город Москва");
                    request = WebRequest.Create(@"http://www.meteoservice.ru/weather/now/moskva");
                    break;
            }

            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    string data = reader.ReadToEnd();
                    string temp = new Regex(@"<span class=""value "">(?<temp>[^<]+)&deg;</span>").Match(data).Groups["temp"].Value;
                    Console.WriteLine("\nТемпература воздуха: " + temp);
                    lock(lock_temp)
                    {
                        try
                        {
                            temperature = Int32.Parse(temp);
                        }
                        catch
                        {
                            temperature = 1;
                            Console.WriteLine("Парсер температуры. Ошибка перевода string в int32");
                        }
                    }
                }
            }
        }
    }

    public class Deicing_serv : BasicService
    {
        public static Pathfinder pathfinder = new Pathfinder();
        readonly Graph graph = new Graph(pathfinder);
        HashSet<Deicing_car> cars = new HashSet<Deicing_car>();

        public Deicing_serv() : base("Deicing")
        {
            Bind<int>("set_job", SetJob);
            Log("Deicing service started");
        }

        public void SetJob(int _aircraft_coordinate)
        {
            if (Weather.temperature <= 0)
            {
                Deicing_car car = new Deicing_car(this, _aircraft_coordinate);
                lock (cars)
                {
                    cars.Add(car);
                }
                Log($"Creating deicing car {car.GUID}");
            }
            else
            {
                RemoteCall("AGH", "Deiced", (_aircraft_coordinate, false));
                Log("Deicing not required.");
            }
        }

        public void Log(string message)
        {
            Console.WriteLine(message);
            RemoteCall("Log", "Write", message);
        }

        public void Run()
        {
            while (true)
            {
                List<Deicing_car> removedCars = new List<Deicing_car>();
                lock (cars)
                {
                    DateTime current_time = RemoteRequest<DateTime>("Time", "WhatTime");
                    foreach (Deicing_car car in cars)
                    {
                        if (car.type == 1) //если машинка в пути
                        {
                            car.UpdatePosition(current_time);
                        }
                        else if (car.type == 2) //если ждет разрешения двигаться дальше (находится на узле)
                        {
                            //car.FreeRoad(); //освобождение дороги
                            //car.SendPosition(); //если машинка остается на узле, то отправлем визуализатору узел
                            car.MovePermission(current_time); //запрос к Службе управления наземным движением на разрешение двигаться дальше
                        }
                        else if (car.type == 3) //если выполняет задание
                        {
                            //car.FreeRoad(); //освобождение дороги
                            //car.SendPosition(); //если машинка остается на узле, то отправлем визуализатору 
                            if (car.job_status == 1) car.StartWork(current_time);
                            else if (car.job_status == 2) car.DoWork(current_time);
                            else car.CreatPathBack(); //создание обратного 
                        }
                        else if (car.type == 5) //если вернулись в гараж
                        {
                            removedCars.Add(car);
                        }
                    }
                    foreach (var car in removedCars)
                    {
                        RemoteCall("GRAPHICS", "Kill", (car.GUID));
                        cars.Remove(car);
                    }
                    
                }
                Thread.Sleep(100);
            }
        }
    }

    class Deicing_car
    {
        Deicing_serv serv;

        float speed = 0.03f; //половина единицы расстояния в секунду

        static int noOfObjects = 0; //номер созданной машины
        public readonly string GUID;

        public int type; //тип движения: 1 - в пути, 2 - жду возможности ехать, 3 - выполняю задание, 4 - на обратном пути 5 - вернулись в гараж
        public int job_status = 1; //состояние обработки задания: 1 - не начала, 2 - в процессе, 3 - завершено

        int location; //узел графа, на котором находится машина в настоящий момент
        int nearest_coordinate; //узел графа, следующий на пути
        readonly int aircraft_coordinate; //узел графа, на котором находится самолет
        List<int> path; //путь машинки - список узлов графа

        DateTime start_time; //время начала пути из конкретного узла
        DateTime end_time; //время расчетного конца пути (достижения следующего узла)
        DateTime end_work; //время конца работы

        public Deicing_car(Deicing_serv _serv, int _aircraft_coordinate)
        {
            serv = _serv;

            noOfObjects++;
            GUID = "d_" + noOfObjects;

            type = 2;

            location = 19; //19 - узел гаража
            aircraft_coordinate = _aircraft_coordinate;
            path = Deicing_serv.pathfinder.GetRoad(location, this.aircraft_coordinate);
            nearest_coordinate = path[0];
            SendPosition();

            //start_time = serv.RemoteRequest<DateTime>("Time", "WhatTime");
            //end_time = start_time.AddSeconds(Deicing_serv.pathfinder.GetRoadLength(location, nearest_coordinate) / speed);
        }
        
        public void UpdatePosition(DateTime current_time)
        {
            if (current_time > end_time)
            {
                FreeRoad();
                MoveNext();
                SendPosition();
            }
            else
            {
                SendProgress(current_time); //если машинка в пути отправляем визуализатору прогресс
            }
        }

        public void MoveNext() //смена узла на следующий из списка пути
        {
            location = nearest_coordinate;
            if (path.Count <= 1)
            {
                //приехали
                if (job_status == 1)
                {
                    type = 3; //делаем работу
                }
                else
                {
                    type = 5; //удаляемся
                    serv.Log("Removing car " + this.GUID);
                }
            }
            else
            {
                type = 2;
                path.RemoveAt(0);                
                nearest_coordinate = path[0];
                serv.Log($"Waiting for road from {location} to {nearest_coordinate}");
            }            
        }

        public void SendProgress(DateTime current_time) //отправку визуализатору прогресса во время движения по ребру графа
        {
            float progress = (float)(current_time.Subtract(start_time).TotalSeconds / end_time.Subtract(start_time).TotalSeconds);
            serv.RemoteCall("GRAPHICS", "UpdatePosition", (this.GUID, this.location, this.nearest_coordinate, progress));
        }

        public void SendPosition() //отправка информации об остановке на узле визуализатору
        {
            serv.RemoteCall("GRAPHICS", "SetPosition", (this.GUID, this.location));
        }

        public void FreeRoad() //освобождение дороги
        {
            serv.RemoteCall("ControlTransport", "FreeRoad", GUID);
            serv.Log("Freeing owned road");
        }

        public void MovePermission(DateTime currentTime) //ожидание разрешения на движение от Службы управления наземным движением
        {
            if (GroundMovementControl(true))
            {
                type = 1;
                start_time = currentTime;
                end_time = start_time.AddSeconds(Deicing_serv.pathfinder.GetRoadLength(location, nearest_coordinate) / speed);
                serv.Log($"Start moving form {location} to {nearest_coordinate}");
            }
        }

        private bool GroundMovementControl(bool type) //взаимодействие со Службой управления наземного движения
        {
            bool resolution; //ответ Службы управления наземного движения
            resolution = serv.RemoteRequest<bool>("ControlTransport", "SetRoad", (this.GUID, type, this.location, this.nearest_coordinate));
            return resolution;            
        }

        public void CreatPathBack() //создание пути от Самолета до Гаража
        {
            path = Deicing_serv.pathfinder.GetRoad(location, 19);
            nearest_coordinate = path[0];
            type = 2;
        }

        public void StartWork(DateTime current_time) //задача времени 
        {
            end_work = current_time.AddSeconds(360);
            job_status = 2;
            serv.Log($"Start doing work for {this.GUID}");
        }

        public void DoWork(DateTime current_time) //выполнение работы
        {
            if (current_time >= end_work)
            {
                job_status = 3;
                serv.Log($"Job done for {this.GUID}");
                serv.RemoteCall("AGH", "Deiced", (aircraft_coordinate, true));
            }
        }

        
        public void GroundService() //уведомление Службы наземного обслуживания об окончании работ
        {
            serv.RemoteCall("AGH", "Deiced", (this.aircraft_coordinate, true));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Config.IsLocal = false;

            Deicing_serv d = new Deicing_serv();
            d.Start(true);
            Task mainCycle = Task.Factory.StartNew(d.Run);
            while (true)
            {
                Weather.CitySelection();
            }
        }
    }
}