﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Plane
{
    public class Aircraft
    {
        public string guidAircraft; //generated GUID for the aircraft
        public string flight; //number of flight
        public int seats; //number of seats
        public int passengers; //amount of passengers on board
        public int type; //1 - civil, 2 - other
        public int fueltankMax; //max amount of fuel
        public int fueltankNow; //amount of fuel now
        public int luggage; //amount of luggage on board
        public int node;

        public List<Person> OnBoard;

        public Aircraft()
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            guidAircraft = "p_" + Guid.NewGuid().ToString();
            fueltankMax = rnd.Next(80, 121);
            Thread.Sleep(20);
            fueltankNow = fueltankMax - rnd.Next(20, 41);
        }

        public void Specify(string fl, int st, int tp)
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            this.flight = fl;
            this.seats = st;
            this.passengers = st - rnd.Next(1, st / 2);
            this.type = tp;
            this.luggage = passengers * rnd.Next(1, 31);
            this.node = 2;
        }

        public void PersonAdd(List<Person> l)
        {
            this.OnBoard = l;
        }
    }

     public class Person //пассажир
    {
        public string name; //Name and Surname "John Brown"
        public string guidPerson; //generated GUID for person
        public string flight; //number of flight
        public int luggage; //laguage mass
        public int state; //1 - no boarding pass, 2 - has a boarding pass, 3 - chill zone, 4 - in the buss, 5 - on plane
        public int food; //1 - normal, 2 - vegan, 3 - McDonalds

        public Person()
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            name = GenerateName(rnd.Next(3,5))+" "+ GenerateName(rnd.Next(5, 7));
            flight = "";
            guidPerson = "passenger_"+Guid.NewGuid().ToString();
            luggage = rnd.Next(1,35);
            state = 1;
            food = rnd.Next(1,4);
        }

        string GenerateName(int len)
        {
            Random r = new Random((int)DateTime.Now.Ticks);
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "e", "y" };
            string Name = "";
            Name += consonants[r.Next(consonants.Length)].ToUpper();
            Name += vowels[r.Next(vowels.Length)];
            int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                Name += consonants[r.Next(consonants.Length)];
                b++;
                Name += vowels[r.Next(vowels.Length)];
                b++;
            }
            return Name;
        }
    }
}
