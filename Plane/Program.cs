﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Platform.Network;


namespace Plane
{
    class Program
    {
        public static List<Aircraft> Unspecified = new List<Aircraft>();
        public static List<Aircraft> Specified = new List<Aircraft>();

        static void Main(string[] args)
        {
            Config.IsLocal = false;
            PlaneService service = new PlaneService();
            service.Start(true);

            Unspecified = Deserialize("saveUnspecified");
            if (Unspecified.Count < 50)
            {
                GeneratePlane();
            }

            //потом убрать
            int counter = 0;
            Console.WriteLine("Enter amount of planes");

            while (true)
            {
                if (Unspecified.Count < 5)
                {
                    GeneratePlane();
                }
                Serialize(Unspecified, "saveUnspecified");

                //потом убрать
                int temp = 0;
                Int32.TryParse(Console.ReadLine(), out temp);
                for (int i = 0; i < temp; i++)
                {
                    string str = "AA000" + counter.ToString();
                    var tup = (60, 1, "AA000" + counter.ToString());
                    service.SpecPlane(tup);
                    counter++;
                }
            }
        }

        public static void GeneratePlane()
        {
            for (int i = 0; i < 100; i++)
            {
                Unspecified.Add(new Aircraft());
            }
        }

        public class PlaneService : BasicService
        {
            public PlaneService() : base("Plane")
            //Указываем имя сервиса, по которому к нему будут обращаться
            {
                Bind<(int,int,string)>("LandPlane", SpecPlane);
                Bind<Tuple<string, int, int>, string>("TakePlane", FollowMe);
                Bind<Tuple<int, string>> ("LeavePlane", UnFollowMe);
                Bind<List<string>, string>("SendPassengers", SendPass);
                Bind<int, string>("SendLuggage", SendLugg);
                Bind<string>("Fuel", Fuel);
                Bind<Tuple<string, List<string>>>("LoadPass", LoadPass);
                Bind<Tuple<string, int>>("LoadLugg", LoadLugg);
                Bind<string>("Takeoff", Takeoff);
            }

            public void Takeoff(string fl)
            {
                Random rnd = new Random((int)DateTime.Now.Ticks);
                int num = 0;
                for (int i = 0; i < Specified.Count; i++)
                {
                    if (Specified[i].flight == fl)
                    {
                        num = i;
                        break;
                    }
                }
                if (Specified[num].OnBoard.Count != 0)
                {
                    for (int i = 0; i < Specified[num].OnBoard.Count; i++)
                    {

                        Specified[num].OnBoard[i].state = 1;
                        Specified[num].OnBoard[i].luggage = rnd.Next(1, 36);
                        Specified[num].OnBoard[i].flight = "";
                        RemoteCall("Passenger", "PassengerReceive", Specified[num].OnBoard[i]);
                    }
                }

                RemoteCall("GRAPHICS", "Kill", Specified[num].guidAircraft);
                RemoteCall("Schedule", "PlaneOut", Specified[num].flight);
                Console.WriteLine("Plane whith flight: "+ Specified[num].flight +" is taking off");
                Unspecified.Add(new Aircraft());
                Specified.RemoveAt(num);
            }

            public void LoadLugg(Tuple<string, int> tuple)
            {
                for (int i = 0; i < Specified.Count; i++)
                {
                    if (Specified[i].flight == tuple.Item1)
                    {
                        Specified[i].luggage = tuple.Item2;

                        string message = "Plane " + Specified[i].flight + " is now loaded with " + Specified[i].luggage + " kg of luggage";
                        Log(message);
                        return;
                    }
                }
            }

            public void LoadPass(Tuple<string, List<string>> tuple)
            {
                string message;
                int num = 0;
                for (int i = 0; i < Specified.Count; i++)
                {
                    if (Specified[i].flight == tuple.Item1)
                    {
                        num = i;
                        break;
                    }
                }
                for (int i = 0; i < tuple.Item2.Count; i++)
                {
                    try
                    {
                        Specified[num].OnBoard.Add(RemoteRequest<Person>("Passenger", "Find", tuple.Item2[i]));
                    }
                    catch
                    {
                        message = "Passenger is offline";
                        LogProblem(message);
                    }
                }
                Specified[num].passengers = Specified[num].OnBoard.Count;
                Console.WriteLine(Specified[num].passengers + " " + Specified[num].OnBoard.Count + " " + Specified[num].node);
                RemoteCall("GRAPHICS", "SetPositionLoad", new Tuple<string, int, int>(Specified[num].guidAircraft, Specified[num].node, Specified[num].OnBoard.Count));

                message = "Plane " + Specified[num].flight + " is now loaded with " + Specified[num].OnBoard.Count + " passengers";
                Log(message);
            }

            public void Fuel(string s)
            {
                for (int i = 0; i < Specified.Count; i++)
                {
                    if (s == Specified[i].flight)
                    {
                        Specified[i].fueltankNow = Specified[i].fueltankMax;
                        string message = "Plane " + Specified[i].flight + " is now fueled";
                        Log(message);
                        return;
                    }
                }
            }

            public int SendLugg(string s)
            {
                int mass = 1;
                
                for (int i = 0; i < Specified.Count; i++)
                {
                    if (s == Specified[i].flight)
                    {
                        mass = Specified[i].luggage;
                        Specified[i].luggage = 0;
                    }
                }

                string message = mass + " kg of luggage removed from Plane " + s;
                Log(message);
                return mass;
            }

            public void UnFollowMe(Tuple<int, string> f)
            {
                var temp = new Aircraft();
                for (int i = 0; i < Specified.Count; i++)
                {
                    if (Specified[i].flight == f.Item2)
                    {
                        Specified[i].node = f.Item1;
                        temp = Specified[i];
                    }
                }

                string message = "FollowMe left Plane " + temp.flight + " at node " + f.Item1;
                Log(message);
                RemoteCall("GRAPHICS", "SetPositionLoad", new Tuple<string, int, int>(temp.guidAircraft, temp.node, temp.passengers));
            }

            public Tuple<string, int, int> FollowMe(string f)
            {
                var temp = new Aircraft();
                for (int i = 0; i < Specified.Count; i++)
                {
                    if (Specified[i].flight == f)
                    {
                        temp = Specified[i];
                    }
                }

                string message = "FollowMe took Plane " + temp.flight;
                Log(message);
                return new Tuple<string, int, int>(temp.guidAircraft, 2, temp.passengers);
            }

            public List<string> SendPass(string s)
            {
                int number = 0;

                List<string> send = new List<string>();
                for (int i = 0; i < Specified.Count; i++)
                {
                    if (Specified[i].flight == s)
                    {
                        number = i;
                    }
                }
                Console.WriteLine(number);
                int counter = 0;
                List<int> toRemove = new List<int>();

                foreach (var item in Specified[number].OnBoard)
                {
                    if ((item.state != 6) && (send.Count < 30))
                    {
                        toRemove.Add(counter);
                        send.Add(item.guidPerson);
                        item.state = 6;
                        RemoteCall("Passenger", "PassengerReceive", item);
                    }
                    counter++;
                }

                for (var i = toRemove.Count - 1; i >= 0; i--)
                {
                    Specified[number].OnBoard.RemoveAt(toRemove[i]);
                }

                Specified[number].passengers = Specified[number].OnBoard.Count;
                RemoteCall("GRAPHICS", "SetPositionLoad", new Tuple<string, int, int>(Specified[number].guidAircraft, Specified[number].node, Specified[number].passengers));
                string message = send.Count + " Passengers removed from Plane " + s;
                Log(message);

                return send;
            }

            public void SpecPlane((int, int, string) cart)
            {
                Random rnd = new Random((int)DateTime.Now.Ticks);
                int id = rnd.Next(0, Unspecified.Count);

                Aircraft temp = Unspecified[id];
                Unspecified.RemoveAt(id);

                temp.Specify(cart.Item3, cart.Item1, cart.Item2);

                try
                {
                    temp.PersonAdd(RemoteRequest<List<Person>>("Passenger", "FillPlane", temp.passengers));
                }
                catch
                {
                    Console.WriteLine("Passenger service is offline");
                }

                Specified.Add(temp);

                string message = "Plane " + temp.flight + " landed at Node 2";
                Log(message);

                RemoteCall("AGH", "SendNeeds", new Tuple<int, int, int, int, string>(temp.passengers, temp.luggage, temp.fueltankMax - temp.fueltankNow, temp.type, temp.flight));
                RemoteCall("GRAPHICS", "SetPositionLoad", new Tuple<string,int,int>(temp.guidAircraft, 2, temp.passengers));
            }

            public void Log(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "Write", message);
            }

            public void LogProblem(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "AddLog", new Tuple<int, string>(1, message));
            }
        }

        public static void Serialize(List<Aircraft> list, string file)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Aircraft>));
            string xml;
            using (StringWriter stringWriter = new StringWriter())
            {
                xmlSerializer.Serialize(stringWriter, list);
                xml = stringWriter.ToString();
            }
            File.WriteAllText(file, xml, Encoding.Default);
        }

        public static List<Aircraft> Deserialize(string file)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Aircraft>));
            List<Aircraft> list = new List<Aircraft>();
            try
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    list = (List<Aircraft>)xmlSerializer.Deserialize(sr);
                }
            }
            catch
            {
                for (int i = 0; i < 100; i++)
                {
                    list.Add(new Aircraft());
                }
            }

            return list;
        }
    }
}
