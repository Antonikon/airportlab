﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Platform.Network;

namespace Passenger
{
    class Program
    {
        public static List<Person> NotInAP = new List<Person>();
        public static List<Person> WaitingInAP = new List<Person>();
        public static List<Person> InVehicle = new List<Person>();
        public static List<Person> LandingPassengers = new List<Person>();
        public static List<Person> SecretPerson = new List<Person>();
        public static List<Person> IssuedPersons = new List<Person>();
        public static List<Person> ComingHome = new List<Person>();
        public static int TimeMult = 1;

        static void Main(string[] args)
        {
            Config.IsLocal = false;
            PassengerService service = new PassengerService();
            service.Start(true);

            Console.WriteLine("Enter 1 to create pepople, 0 to load from save");
            Console.WriteLine("START WAITINGROOM BEFORE ENTERING NUMBER");
            string ch = Console.ReadLine();
            if (ch == "1")
            {
                for (int i = 0; i < 100; i++)
                {
                    NotInAP.Add(new Person());
                    Thread.Sleep(20);
                    Console.WriteLine("Person " + i + " is created");
                }
            }
            else
            {
                NotInAP = Deserialize("saveNotInAP");
            }

            try
            {
                TimeMult = service.RemoteRequest<int>("Time", "WhatMult");
            }
            catch
            {
                string message = "Time is offline";
                service.Log(message);
            }

            service.RemoteCall("Time", "MultSubscribe", new Tuple<string, string>("Passenger", "TimeMultChange"));
            Console.WriteLine(TimeMult + " - time mult");


            List<Person> tempWait = new List<Person>();

            Console.WriteLine("Press F to Start");
            Console.ReadKey();

            Task.Factory.StartNew(() => SendToAnton(service));

            while (true)
            {
                if (NotInAP.Count < 70)
                {
                    GeneratePassenger();
                }
                Serialize(NotInAP, "saveNotInAP");
            }
        } 

        public static void GeneratePassenger()
        {
            for (int i = 0; i < 100; i++)
            {
                NotInAP.Add(new Person());
                Thread.Sleep(20);
                Console.WriteLine("Person " + i + " is created");
            }
        }

        public static void SendToAnton(PassengerService service)
        {
            
            while (true)
            {
                Random rnd = new Random((int)DateTime.Now.Ticks);
                int temp = rnd.Next(0, NotInAP.Count);
                string message = "Person is send to Cashier";
                Person tempP = NotInAP[temp].Clone();
                NotInAP.RemoveAt(temp);
                service.RemoteCall("Cashier", "ReceivePerson", tempP);
                service.Log(message);

                Thread.Sleep(20);

                List<int> ticketed = new List<int>();
                temp = rnd.Next(1, 101);
                for (int i = 0; i < NotInAP.Count; i++)
                {
                    if (NotInAP[i].flight != "")
                    {
                        ticketed.Add(i);
                    }
                }

                if ((temp<90) && (ticketed.Count>0))
                {
                    temp = rnd.Next(0, ticketed.Count);
                    int found = ticketed[temp];
                    tempP = NotInAP[found].Clone();
                    NotInAP.RemoveAt(found);
                }
                else
                {
                    temp = rnd.Next(0, NotInAP.Count);
                    tempP = NotInAP[temp].Clone();
                    NotInAP.RemoveAt(temp);
                }

                service.RemoteCall("Registration", "PassengerRegistration", tempP);
                message = "Person is send to Registration " + tempP.flight;
                Console.WriteLine();
                service.Log(message);
                Console.WriteLine();
                Thread.Sleep(4000 / TimeMult);
            }
        }

        public static void FromNotAPToWait(PassengerService service)
        {
            service.RemoteCall("Waitingroom", "ReceivePassengers", WaitingInAP);
        }

        public class PassengerService : BasicService
        {
            public PassengerService() : base("Passenger")
            //Указываем имя сервиса, по которому к нему будут обращаться
            {
                Bind<Person>("PassengerReceive", Receive);
                Bind<List<Person>, int>("FillPlane", Filler);
                Bind<Person>("LanderReceive", LReceive);
                Bind<int>("TimeMultChange", ChangeMult);
                Bind("SendToWaitingroom", SendWaitingroom);
                Bind<string>("Clear", Clear);
                Bind<Person, string>("Find", Find);
                Bind<string>("RegStart", RegStart);
            }

            public void RegStart(string fl)
            {
                string message = "Registration started and people are coming";
                Log(message);
                //int counter = 0;
                //List<int> id = new List<int>();
                //for (int i = 0; i < NotInAP.Count; i++)
                //{
                //    counter++;
                //}

                //counter = counter / 100 * 80;
               
                //List<int> toRemove = new List<int>();

                //for (int i = 0; i < NotInAP.Count; i++)
                //{
                //    if ((NotInAP[i].flight == fl) && (counter > 0))
                //    {
                //        toRemove.Add(i);
                //        RemoteCall("Registration", "ReceivePerson", NotInAP[i]);
                //        Thread.Sleep(20);
                //    }
                //}


                //for (int i = toRemove.Count - 1; i >= 0; i--)
                //{
                //    NotInAP.RemoveAt(toRemove[i]);
                //}
            }

            public Person Find(string id)
            {
                for (int i = 0; i < InVehicle.Count; i++)
                {
                    if (InVehicle[i].guidPerson == id)
                    {
                        return InVehicle[i];
                    }
                }
                var p = new Person();
                p.state = 5;
                p.guidPerson = id;
                return p;
            }

            public void Clear(string s)
            {
                string message = "Landed Passengers are coming home";
                Log(message);
                string fl = "";
                List<int> toRemove = new List<int>();

                for (int i = 0; i < ComingHome.Count; i++)
                {
                    if (s == ComingHome[i].guidPerson)
                    {
                        fl = ComingHome[i].flight;
                        break;
                    }
                }
                for (int i = 0; i < ComingHome.Count; i++)
                {
                    if (fl == ComingHome[i].flight)
                    {
                        toRemove.Add(i);
                    }
                }

                for (int i = toRemove.Count - 1; i >= 0; i--)
                {
                    ComingHome.RemoveAt(toRemove[i]);
                }
            }

            public void ChangeMult(int t)
            {
                TimeMult = t;
                Console.WriteLine("new time mult is: " + TimeMult);
            }

            public List<Person> Filler(int num)
            {
                string message = "Generated Passengers for landing Plane";
                Log(message);
                var sendList = new List<Person>();
                for (int i = 0; i < num; i++)
                {
                    Thread.Sleep(20);
                    sendList.Add(new Person());
                    sendList[i].state = 5;
                    LandingPassengers.Add(sendList[i]);
                }
                return sendList;
            }

            public void LReceive(Person p)
            {
                int found = -1;
                int st = p.state;
                string id = p.guidPerson;
                for (int i = 0; i < LandingPassengers.Count; i++)
                {
                    if (id == LandingPassengers[i].guidPerson)
                        found = i;
                }
                if (found<0)
                {
                    IssuedPersons.Add(p);
                    return;
                }

                LandingPassengers.RemoveAt(found);
                LandingPassengers.Add(p);
            }

            public void Receive(Person p)
            {
                string message = "Person is receiced and added to a right list";
                Log(message);
                int st = p.state;
                int found = -1;
                string id = p.guidPerson;
                switch (st)
                {
                    case 1:
                        NotInAP.Add(p);
                        break;
                    case 2: //попадают от Антона
                        NotInAP.Add(p);
                        Console.WriteLine(p.guidPerson + " bought a ticket to a flight "+p.flight);
                        break;
                    case 3: //попадают от Антона
                        WaitingInAP.Add(p);
                        RemoteCall("Waitingroom", "ReceivePassengers", WaitingInAP);
                        Console.WriteLine(p.guidPerson + " registered to a flight "+p.flight);
                        break;
                    case 4: //попадают от уведомления из Waitingroom
                        for (int i = 0; i < WaitingInAP.Count; i++)
                        {
                            if (id == WaitingInAP[i].guidPerson)
                                found = i;
                        }
                        WaitingInAP.RemoveAt(found);
                        InVehicle.Add(p);
                        break;
                    case 5:
                        for (int i = 0; i < InVehicle.Count; i++)
                        {
                            if (id == InVehicle[i].guidPerson)
                                found = i;
                        }
                        InVehicle.RemoveAt(found);
                        InVehicle.Add(p);
                        break;
                    case 6:
                        ComingHome.Add(p);
                        break;
                    default:
                        IssuedPersons.Add(p);
                        break;
                }
            }

            public List<Person> SendWaitingroom()
            {
                var sendList = new List<Person>();
                for (int i = 0; i < WaitingInAP.Count; i++)
                {
                    sendList.Add(WaitingInAP[i]);
                }
                return sendList;
            }

            public void Log(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "Write", message);
            }

            public void LogProblem(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "AddLog", new Tuple<int, string>(1, message));
            }
        }

        public static void Serialize(List<Person> list, string file)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Person>));
            string xml;
            using (StringWriter stringWriter = new StringWriter())
            {
                xmlSerializer.Serialize(stringWriter, list);
                xml = stringWriter.ToString();
            }
            File.WriteAllText(file, xml, Encoding.Default);
        }

        public static List<Person> Deserialize(string file)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Person>));
            List<Person> list;
            using (StreamReader sr = new StreamReader(file))
            {
                list = (List<Person>)xmlSerializer.Deserialize(sr);
            }
            return list;
        }
    }
}
