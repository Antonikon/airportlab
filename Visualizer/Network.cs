﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Network;
namespace Visualizer
{
    class Network : BasicService
    {
        public Network(ref Logic logic) : base("GRAPHICS")
        {
            Bind<(string guid, int from, int to, float progress)>("UpdatePosition", logic.UpdatePosition);
            Bind<(string guid, int from, int to, float progress, int load)>("UpdatePositionLoad", logic.UpdatePositionLoad);
            Bind<(string guid, int node)>("SetPosition", logic.SetPosition);
            Bind<(string guid, int node, int load)>("SetPositionLoad", logic.SetPositionLoad);
            Bind<string>("Kill", logic.Kill);
        }

        public void SendLog(string message)
        {
            RemoteCall("Log", "write", message);
        }
    }
}
