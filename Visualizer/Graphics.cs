﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using GraphPath;
namespace Visualizer
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Graphics : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        Dictionary<string, Texture2D> sprites;
        float zoom = 0.035f;
        float camPosX = 1500.0f;
        float camPosY = 1500.0f;
        Matrix transform;
        float nodeScale = 20f;
        float fixScale = 100f;
        float roadThickness = 30f;
        int fps = 0;
        float textScale = 0.03f;
        Graph gr = new Graph();
        Logic logic;
        Network network;
        bool showGUID = false;
        public Graphics()
        {
            logic = new Logic();
            network = new Network(ref logic);
            network.Start(true);
            logic.SetUp(network.SendLog);
            graphics = new GraphicsDeviceManager(this);
            sprites = new Dictionary<string, Texture2D>();

            Content.RootDirectory = "Content";

            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += OnResize;
            graphics.PreferredBackBufferWidth = 900;
            graphics.PreferredBackBufferHeight = 600;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();

            transform = Matrix.CreateTranslation(camPosX, camPosY, 0) * Matrix.CreateScale(zoom);
        }

        private void OnResize(object sender, EventArgs ev)
        {
            graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
            graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            sprites.Add("logo", Content.Load<Texture2D>("Icon"));
            sprites.Add("arrow", Content.Load<Texture2D>("Arrow"));
            sprites.Add("dot", Content.Load<Texture2D>("Dot"));
            sprites.Add("cross", Content.Load<Texture2D>("Cross"));
            sprites.Add("fieldIn", Content.Load<Texture2D>("FieldIn"));
            sprites.Add("fieldOut", Content.Load<Texture2D>("FieldOut"));
            sprites.Add("fuelTank", Content.Load<Texture2D>("FuelTank"));
            sprites.Add("garage", Content.Load<Texture2D>("Garage"));
            sprites.Add("parking", Content.Load<Texture2D>("Parking"));
            sprites.Add("psgStorage", Content.Load<Texture2D>("PassengerStorage"));
            sprites.Add("exit", Content.Load<Texture2D>("Exit"));

            sprites.Add("luggage", Content.Load<Texture2D>("Luggage"));
            sprites.Add("followMe", Content.Load<Texture2D>("FollowMe"));
            sprites.Add("plane", Content.Load<Texture2D>("Plane"));
            sprites.Add("catering", Content.Load<Texture2D>("Catering"));
            sprites.Add("fuelTruck", Content.Load<Texture2D>("FuelTruck"));
            sprites.Add("bus", Content.Load<Texture2D>("Bus"));
            sprites.Add("deicing", Content.Load<Texture2D>("Deicing"));

            sprites.Add("unknown", Content.Load<Texture2D>("Unknown"));

            font = Content.Load<SpriteFont>("Font");
            Logger.Log("Content in visualizer has been loaded.", MsgTypes.Info, network);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            network.Stop();
            logic.Clear();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.F3)) logic.Clear();
            if (Keyboard.GetState().IsKeyDown(Keys.F1)) showGUID = true;
            if (Keyboard.GetState().IsKeyDown(Keys.F2)) showGUID = false;
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (Keyboard.GetState().IsKeyDown(Keys.E)) zoom += 0.001f;
            if (Keyboard.GetState().IsKeyDown(Keys.Q)) zoom -= 0.001f;

            if (Keyboard.GetState().IsKeyDown(Keys.W)) camPosY += 75;
            if (Keyboard.GetState().IsKeyDown(Keys.S)) camPosY -= 75;

            if (Keyboard.GetState().IsKeyDown(Keys.A)) camPosX += 75;
            if (Keyboard.GetState().IsKeyDown(Keys.D)) camPosX -= 75;
            if (zoom > 5) zoom = 5;
            if (zoom < 0.035f) zoom = 0.035f;
            transform = Matrix.CreateTranslation(camPosX, camPosY, 0) * Matrix.CreateScale(zoom);

            fps = (int)(1000 / gameTime.ElapsedGameTime.TotalMilliseconds);
            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(30, 30, 30));

            // TODO: Add your drawing code here
            spriteBatch.Begin(samplerState: SamplerState.PointClamp, transformMatrix: transform);
            foreach (var item in gr.Roads)
            {
                spriteBatch.DrawLine(
                    sprites["dot"],
                    new Vector2(
                        gr.Nodes[item.Key.node1].x * fixScale,
                        gr.Nodes[item.Key.node1].y * fixScale
                        ),
                    new Vector2(
                        gr.Nodes[item.Key.node2].x * fixScale,
                        gr.Nodes[item.Key.node2].y * fixScale
                        ),
                    Color.Red,
                    roadThickness);
            }
            Texture2D temp = sprites["unknown"];
            foreach (var item in gr.Nodes)
            {

                switch (item.Value.type)
                {
                    case NodeTypes.Road:
                        temp = sprites["cross"];
                        break;
                    case NodeTypes.FieldIn:
                        temp = sprites["fieldIn"];
                        break;
                    case NodeTypes.FieldOut:
                        temp = sprites["fieldOut"];
                        break;
                    case NodeTypes.FuelTank:
                        temp = sprites["fuelTank"];
                        break;
                    case NodeTypes.Garage:
                        temp = sprites["garage"];
                        break;
                    case NodeTypes.PassengerStorage:
                        temp = sprites["psgStorage"];
                        break;
                    case NodeTypes.PlaneParking:
                        temp = sprites["parking"];
                        break;
                    case NodeTypes.Exit:
                        temp = sprites["exit"];
                        break;
                }

                float addedScale;
                if (item.Value.type != NodeTypes.Road) addedScale = 3f;
                else addedScale = 0.5f;
                float totalScale = nodeScale * addedScale;
                spriteBatch.Draw(
                    temp,
                    position:
                    new Vector2(
                        item.Value.x * fixScale - sprites["cross"].Width * totalScale / 2,
                        item.Value.y * fixScale - sprites["cross"].Height * totalScale / 2
                        ),
                    color: Color.White,
                    scale: new Vector2(totalScale, totalScale));
            }
            Vector2 textFixVec = new Vector2(0, 4 * fixScale);
            try
            {
                foreach (var item in logic.Cars)
                {
                    CarData data = item.Value;
                    if (item.Key == null || item.Key == "")
                    {
                        Logger.Log($"Car had null guid.", MsgTypes.Error);
                        continue;
                    }
                    switch (item.Value.type)
                    {
                        case CarTypes.Bus:
                            temp = sprites["bus"];
                            break;
                        case CarTypes.Catering:
                            temp = sprites["catering"];
                            break;
                        case CarTypes.FollowMe:
                            temp = sprites["followMe"];
                            break;
                        case CarTypes.Plane:
                            temp = sprites["plane"];
                            break;
                        case CarTypes.DeIcing:
                            temp = sprites["deicing"];
                            break;
                        case CarTypes.FuelTruck:
                            temp = sprites["fuelTruck"];
                            break;
                        case CarTypes.Luggage:
                            temp = sprites["luggage"];
                            break;
                        default:
                            temp = sprites["unknown"];
                            break;
                    }
                    Vector2 pos;
                    if (item.Value.currentNode == null)
                    {
                        if (item.Value.nodeFrom == null || item.Value.nodeTo == null)
                        {
                            Logger.Log($"Car with id = {item.Key} had all null fields : {item.Value.currentNode} . {item.Value.nodeFrom} . {item.Value.nodeTo}", MsgTypes.Warn);
                            continue;
                        }

                        try
                        {
                            Node nodeFrom = gr.Nodes[(int)item.Value.nodeFrom];
                            Node nodeTo = gr.Nodes[(int)item.Value.nodeTo];
                            pos.X = nodeFrom.x + (nodeTo.x - nodeFrom.x) * data.progress;
                            pos.Y = nodeFrom.y + (nodeTo.y - nodeFrom.y) * data.progress;
                        }
                        catch (KeyNotFoundException)
                        {
                            Logger.Log($"Car with id = {item.Key} had wrong node fields.", MsgTypes.Warn);
                            continue;
                        }
                    }
                    else
                    {
                        try
                        {
                            Node nodeCur = gr.Nodes[(int)item.Value.currentNode];
                            pos.X = nodeCur.x;
                            pos.Y = nodeCur.y;
                        }
                        catch (KeyNotFoundException)
                        {
                            Logger.Log($"Car with id = {item.Key} had wrong current node field.", MsgTypes.Warn);
                            continue;
                        }
                    }

                    float addedScale;
                    addedScale = 4.5f;
                    float totalScale = nodeScale * addedScale;

                    pos.X = pos.X * fixScale - temp.Width * totalScale / 2;
                    pos.Y = pos.Y * fixScale - temp.Height * totalScale / 2;
                    spriteBatch.Draw(
                        temp,
                        position: pos,
                        color: Color.White,
                        scale: new Vector2(totalScale, totalScale));
                    if (item.Value.type == CarTypes.Bus || item.Value.type == CarTypes.Plane)
                    {
                        spriteBatch.DrawString(
                            font,
                            item.Value.load.ToString(),
                            position: pos,
                            Color.YellowGreen,
                            0,
                            new Vector2(),
                            new Vector2(totalScale * textScale, totalScale * textScale),
                            SpriteEffects.None,
                            0
                            );
                    }
                    if (showGUID)
                    {
                        spriteBatch.DrawString(
                            font,
                            item.Key,
                            position: pos - textFixVec,
                            Color.LawnGreen,
                            0,
                            new Vector2(),
                            new Vector2(totalScale * textScale, totalScale * textScale),
                            SpriteEffects.None,
                            0
                            );
                    }
                }
                spriteBatch.DrawString(
                            font,
                            fps.ToString(),
                            position: new Vector2(-camPosX, -camPosY),
                            Color.LawnGreen,
                            0,
                            new Vector2(),
                            new Vector2(textScale * 5 / zoom, textScale * 5 / zoom),
                            SpriteEffects.None,
                            0
                            );
            } catch (Exception)
            {
                Logger.Log("Possibly tried to read nonexistent car.", MsgTypes.Error, network);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
