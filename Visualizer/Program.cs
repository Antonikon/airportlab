﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Platform.Network;
namespace Visualizer
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Config.IsLocal = false;
            Config.LogLevel = 1;
            //Console.WriteLine();
            //Logic logic = new Logic();
            //Network network = new Network();
            //network.Start(true);
            using (var game = new Graphics())
                game.Run();
        }
    }
}
