﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Network;

namespace Visualizer
{
    enum MsgTypes
    {
        Info,
        Warn,
        Error,
        Critical
    }
    static class Logger
    {
        public static void Log(string message, MsgTypes type, Network network = null, Action<string> sendFunc = null)
        {
            string append = "";
            switch (type)
            {
                case MsgTypes.Info:
                    append = "info";
                    break;
                case MsgTypes.Warn:
                    append = "warn";
                    break;
                case MsgTypes.Error:
                    append = "error";
                    break;
                case MsgTypes.Critical:
                    append = "critical";
                    break;
            }
            string file = $"log_{append}.txt";
            DateTime time = DateTime.Now;
            string res = $"[{time.ToString()}][GRAPHICS][{append.ToUpper()}] : {message}";
            try
            {
                if (File.Exists(file))
                {
                    using (var sw = File.AppendText(file))
                    {
                        sw.WriteLine(res);
                    }
                }
                else
                {
                    using (var sw = File.CreateText(file))
                    {
                        sw.WriteLine(res);
                    }
                }
                if(network != null)
                {
                    network.SendLog(res);
                }
                if (sendFunc != null)
                {
                    sendFunc(res);
                }
            }
            catch (Exception)
            {
                return;
            }
        }
    }
}
