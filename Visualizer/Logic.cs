﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Network;
namespace Visualizer
{
    public enum CarTypes
    {
        Plane,
        Luggage,
        FollowMe,
        FuelTruck,
        Bus,
        DeIcing,
        Catering,
        Unknown
    }
    public struct CarData
    {
        public int? currentNode;
        public int? nodeFrom;
        public int? nodeTo;
        public float progress;
        public int load;
        public CarTypes type;
    }
    public class Logic
    {
        Action<string> sender;
        ConcurrentDictionary<string, CarData> cars = new ConcurrentDictionary<string, CarData>();
        public IReadOnlyDictionary<string, CarData> Cars => cars;
        public Logic()
        {
        }
        public void SetUp(Action<string> func)
        {
            sender = func;
        }
        CarTypes ParseCarType(string guid)
        {
            switch (guid.Split('_')[0])
            {
                case "d":
                    return CarTypes.DeIcing;
                case "b":
                    return CarTypes.Bus;
                case "l":
                    return CarTypes.Luggage;
                case "f":
                    return CarTypes.FollowMe;
                case "t":
                    return CarTypes.FuelTruck;
                case "c":
                    return CarTypes.Catering;
                case "p":
                    return CarTypes.Plane;
                default: return CarTypes.Unknown;
            }
        }
        public void UpdatePosition((string guid, int from, int to, float progress) data)
        {
            UpdatePositionLoad((data.guid, data.from, data.to, data.progress, 0));
        }

        public void UpdatePositionLoad((string guid, int from, int to, float progress, int load) data)
        {
            //Console.WriteLine(data.ToString());
            Console.WriteLine($"GUID: {data.guid} | Node from: {data.from} | Node to: {data.to} | Progress: {data.progress} | Load: {data.load}");
            Task.Factory.StartNew(() =>
            {
                try
                {
                    if (cars.ContainsKey(data.guid))
                    {
                        CarData temp = cars[data.guid];
                        CarData comp = cars[data.guid];
                        temp.currentNode = null;
                        temp.load = data.load;
                        temp.nodeFrom = data.from;
                        temp.nodeTo = data.to;
                        temp.progress = data.progress;
                        temp.type = ParseCarType(data.guid);
                        //cars[data.guid] = temp;
                        if (!cars.TryUpdate(data.guid, temp, comp))
                        {
                            Logger.Log($"Could not update data in dictionary at key {data.guid}.", MsgTypes.Warn, sendFunc: sender);
                        }
                    }
                    else
                    {
                        if (!cars.TryAdd(data.guid, new CarData
                        {
                            nodeFrom = data.from,
                            nodeTo = data.to,
                            progress = data.progress,
                            type = ParseCarType(data.guid),
                            load = data.load,
                            currentNode = null
                        }))
                        {
                            Logger.Log($"Could not add new data to dictionary at key {data.guid}.", MsgTypes.Warn, sendFunc: sender);
                        }
                    }
                }
                catch (ArgumentNullException)
                {
                    Logger.Log("Car had unknown field(null) in tuple.", MsgTypes.Error, sendFunc: sender);
                }
            });
            //task.Start();
        }

        public void SetPosition((string guid, int node) data)
        {
            SetPositionLoad((data.guid, data.node, 0));
        }
        public void SetPositionLoad((string guid, int node, int load) data)
        {
            Console.WriteLine($"GUID: {data.guid} | Node: {data.node} | Load: {data.load}");
            Task.Factory.StartNew(() =>
            {
                try
                {
                    if (cars.ContainsKey(data.guid))
                    {
                        CarData temp = cars[data.guid];
                        CarData comp = cars[data.guid];
                        temp.currentNode = data.node;
                        temp.load = data.load;
                        temp.nodeFrom = null;
                        temp.nodeTo = null;
                        temp.progress = 0;
                        temp.type = ParseCarType(data.guid);
                        //cars[data.guid] = temp;
                        cars.TryUpdate(data.guid, temp, comp);
                    }
                    else
                    {
                        if (!cars.TryAdd(data.guid, new CarData
                        {
                            nodeFrom = null,
                            nodeTo = null,
                            progress = 0,
                            type = ParseCarType(data.guid),
                            load = data.load,
                            currentNode = data.node
                        }))
                        {
                            Logger.Log($"Could not add new data to dictionary at key {data.guid}.", MsgTypes.Warn, sendFunc: sender);
                        }
                    }
                }
                catch (ArgumentNullException)
                {
                    Logger.Log("Car had unknown field(null) in tuple.", MsgTypes.Error, sendFunc: sender);
                }
            });
            //task.Start();
        }
        public void Kill(string guid)
        {
            Console.WriteLine($"GUID : {guid}");
            Task.Factory.StartNew(() =>
            {
                CarData temp;
                cars.TryRemove(guid, out temp);
            });
            //task.Start();
        }

        public void Clear()
        {
            cars.Clear();
        }
    }
}
