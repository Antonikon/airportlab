﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Network;

namespace Cashier
{
    public class Person //пассажир
    {
        public string name; //Name and Surname "John Brown"
        public string guidPerson; //generated GUID for person
        public string flight; //number of flight
        public int luggage; //laguage mass
        public int state; //1 - no boarding pass, 2 - has a boarding pass, 3 - chill zone, 4 - in the buss, 5 - on plane
        public int food; //1 - normal, 2 - vegan, 3 - McDonalds

        public Person()
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            name = GenerateName(rnd.Next(3, 5)) + " " + GenerateName(rnd.Next(5, 7));
            flight = "";
            guidPerson = "passenger_" + Guid.NewGuid().ToString();
            luggage = rnd.Next(1, 35);
            state = 1;
            food = rnd.Next(1, 4);
        }

        string GenerateName(int len)
        {
            Random r = new Random((int)DateTime.Now.Ticks);
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "e", "y" };
            string Name = "";
            Name += consonants[r.Next(consonants.Length)].ToUpper();
            Name += vowels[r.Next(vowels.Length)];
            int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                Name += consonants[r.Next(consonants.Length)];
                b++;
                Name += vowels[r.Next(vowels.Length)];
                b++;
            }
            return Name;
        }
    }

    class Program
    {
        public static Dictionary<string, List<Person>> flight_lists = new Dictionary<string, List<Person>>(); //словарь списка пассажиров рейсов, ключом выступает название рейса, по которому будет раскидывать людей в нужные списка
        public static Dictionary<string, int> flight_info = new Dictionary<string, int>();

        static void Main(string[] args)
        {
            Config.IsLocal = false;
            CashierService service = new CashierService();
            service.Start(true);

            while (true) { }
        }

        public class CashierService : BasicService
        {
            public CashierService() : base("Cashier")
            {
                Bind<Person>("ReceivePerson", Receive);
                Bind<Tuple<string, int>>("StartSellingTickets", SellsStart);
                Bind<string>("StopSellingTickets", SellsStop);
            }

            public void Receive(Person p)
            {
                string log_message = "";
                Random rnd = new Random((int)DateTime.Now.Ticks);
                p.state = 2;
                if (p.luggage > 30) p.luggage = 30;

                int size = flight_lists.Count;
                //int size = flight_info.Count;    //на случай, если способ Дани не сработает
                while(true)
                {
                    int random_flight = rnd.Next(0, size);
                    //if (random_flight > flight_lists.Count()) flight_lists.Add(new KeyValuePair<string, List<Person>>(flight_info.ElementAt(random_flight).Key, new List<Person>().Add(p))); //на случай, если Данин способ не сработает

                    string random_flight_name = flight_lists.ElementAt(random_flight).Key; //из словаря пассажиров берём случайный рейс (строку)
                    if (flight_info[random_flight_name] > flight_lists[random_flight_name].Count())  //по случайному названию рейса проверяем, заполнены ли места в самолёте, если заполнены, пойдёт новый цикл
                    {
                        p.flight = random_flight_name;
                        flight_lists[random_flight_name].Add(p); //добавление в список пассажиров на рейс нового человека
                        RemoteCall("Registration", "GetFlightList", (random_flight_name, p));
                        Console.WriteLine("--------------------\n{0} now has a ticket -{1}-\n-------------------------", p.guidPerson, p.flight);
                        Console.WriteLine("--------------------\n{0} now has {1} out of {2} reserved seats\n-------------------------", random_flight_name, flight_lists[random_flight_name].Count(), flight_info[random_flight_name]);
                        break;
                    }
                    else { }
                }
                log_message = "Passenger " + p.guidPerson + " has state " + p.state + " now and is sent to passengers";
                //Log(log_message);
                RemoteCall("Passenger", "PassengerReceive", p);
                Console.WriteLine("--------------------\nDanya received {0} with ticket for -{1}-\n-------------------------", p.guidPerson, p.flight);
            }

            public void SellsStart(Tuple<string, int> new_flight_info) //добавляем новые пары ключей-элементов в словари с одинаковым ключом - названием рейса
            {
                //var pair = new KeyValuePair<string, List<Person>>(flight_info.Item1, new List<Person>());
                flight_lists.Add(new_flight_info.Item1, new List<Person>());
                flight_info.Add(new_flight_info.Item1, new_flight_info.Item2);
                Console.WriteLine("The sells has been started on flight {0} with {1} passengers", new_flight_info.Item1, new_flight_info.Item2);
            }

            public void SellsStop(string flight_number) //при конце продажи передаём список на рейс регистрации для проверки, после чего удаляем записи в словарях
            {
                    //создаётся кортеж из номера рейса и прилагающегося к нему списку
                //var copy_list = (flight_number, flight_lists[flight_number]);
                    //регистрации передаётся кортеж
                //RemoteCall("Registration", "GetFlightList", copy_list);
                flight_lists.Remove(flight_number);
                flight_info.Remove(flight_number);
            }

            public void Log(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "Write", message);
            }

            public void LogProblem(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "AddLog", new Tuple<int, string>(1, message));
            }
        }
    }
}