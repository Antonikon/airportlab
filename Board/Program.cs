﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Platform.Network;

namespace Board
{
    class Program
    {
        public static List<(DateTime, string, string, string, int)> Board = new List<(DateTime, string, string, string, int)>();
        static void Main(string[] args)
        {
            Config.IsLocal = false;
            BoardService service = new BoardService();
            service.Start(true);

            service.CreateBoard();
        }

        public class BoardService : BasicService
        {
            public BoardService() : base("Board")
            {
                Bind<(string, int)>("ChangeStatus", ChangeStatus);
            }

            public void CreateBoard()
            {
                Console.Clear();
                Board = RemoteRequest<List<(DateTime, string, string, string, int)>>("Schedule", "GetBoard");
                string st;
                string message = "Табло сформировано";
                for (int i = 0; i < Board.Count; i++)
                {
                    st = GetStatus(Board[i].Item5);                    
                    Console.WriteLine(Board[i].Item1 + " " + Board[i].Item2 + " " + Board[i].Item3 + " " + Board[i].Item4 + " " + st);
                }
                Log(message);
            }

            public void RefreshBoard()
            {
                string message = "Табло обновлено";
                Console.Clear();
                string st;
                for (int i = 0; i < Board.Count; i++)
                {
                    st = GetStatus(Board[i].Item5);
                    Console.WriteLine(Board[i].Item1 + " " + Board[i].Item2 + " " + Board[i].Item3 + " " + Board[i].Item4 + " " + st);
                }
                Log(message);
            }

            public void ChangeStatus((string, int) info)
            {
                if (info.Item2 == 6 || info.Item2 == 8 || info.Item2 == 3) 
                {
                    CreateBoard();
                } else
                {
                    for (int i = 0; i < Board.Count; i++)
                    {
                        if (Board[i].Item2 == info.Item1)
                        {
                            Board[i] = (Board[i].Item1, Board[i].Item2, Board[i].Item3, Board[i].Item4, info.Item2);
                            RefreshBoard();
                        }
                    }
                }
            }

            public string GetStatus(int st)
            {
                string s;
                switch(st)
                {
                    case 3:
                        s = "Идет регистрация.";
                        break;
                    case 4:
                        s = "Идет посадка.";
                        break;
                    case 5:
                        s = "Посадка окончена.";
                        break;
                    case 6:
                        s = "Вылет.";
                        break;
                    case 7:
                        s = "Ожидается.";
                        break;
                    case 8:
                        s = "Прилетел.";
                        break;
                    default:
                        s = "Ы";
                        break;
                }
                return s;
            }

            public void Log(string mess)
            {
                Console.WriteLine(mess);
                //RemoteCall("Log", "Write", mess);
            }

            public void LogPoblem(string message)
            {
                Console.WriteLine(message);
                //RemoteCall("Log", "AddLog", new Tuple<int, string>(1, message));
            }
        }
    }
}
