﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Network;

namespace ControlTransport
{

    public class ControlTransport : BasicService
    {
        HashSet<Tuple<int, int>> Roads = new HashSet<Tuple<int, int>>();
        Dictionary<string, Tuple<int, int>> RoadsByCar = new Dictionary<string, Tuple<int, int>>();

        public ControlTransport() : base("ControlTransport") //как обращаются другие
        {
            Bind<bool, Tuple<string, int, int>>("GetRoad", GetRoad);
            Bind<string>("FreeRoad", FreeRoad);
            Bind<bool, Tuple<string, bool, int, int>>("SetRoad", SetRoad);
            Console.WriteLine("ControlTransport started");
        }

        public bool SetRoad(Tuple<string, bool, int ,int> arg)
        {
            if (arg.Item2 == true) //начало движения
            {
                var t = new Tuple<string, int, int>(arg.Item1, arg.Item3, arg.Item4);
                return GetRoad(t);
            }

            else //конец движения
            {
                FreeRoad(arg.Item1);
                return true;
            }

        }

        public bool GetRoad(Tuple<string, int, int> arg) //функция за начало движения
        {
            var road = new Tuple<int, int>(arg.Item2, arg.Item3);
            if (Roads.Contains(road)) return false;
            if (RoadsByCar.ContainsKey(arg.Item1)) return false;

            Roads.Add(road);
            RoadsByCar.Add(arg.Item1, road);
            Console.WriteLine($"Getting road from {road.Item1} to {road.Item2} for {arg.Item1}");
            return true;
        }

        public void FreeRoad(string str)
        {
            if (!RoadsByCar.ContainsKey(str)) return;

            var road = RoadsByCar[str];
            Roads.Remove(road);
            RoadsByCar.Remove(str);
            Console.WriteLine($"Freeing road from {road.Item1} to {road.Item2} by {str}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Config.IsLocal = false;
            ControlTransport cntr = new ControlTransport();
            Config.LogLevel = 1;
            cntr.Start(false);

            /*BasicClient client = new BasicClient("test_service2");
            var result = client.RemoteRequest<bool>("ControlTransport", "GetRoad", ("car", 0, 1));
            Console.WriteLine(result);
            result = client.RemoteRequest<bool>("ControlTransport", "GetRoad", ("car2", 0, 1));
            Console.WriteLine(result);
            client.RemoteCall("ControlTransport", "FreeRoad", ("car3"));
            client.RemoteCall("ControlTransport", "FreeRoad", ("car"));
            result = client.RemoteRequest<bool>("ControlTransport", "GetRoad", ("car2", 0, 1));
            Console.WriteLine(result);
            Console.WriteLine();
            result = client.RemoteRequest<bool>("ControlTransport", "SetRoad", ("car2", true, 1, 2));
            Console.WriteLine(result);
            client.RemoteRequest<bool>("ControlTransport", "SetRoad", ("car2", false, 234, 123));*/
  

        }
    }
}
