﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Platform.Network;
using Platform.Network.Modules;

namespace time
{
    public class TimeService : BasicService
    {
        Task task;
        CancellationTokenSource cancelTokenSource;
        CancellationToken token;

        DateTime time = new DateTime(2020, 1, 1, 6, 0, 0);
        int coef = 1;

        List<Tuple<string, string, DateTime>> subsTime = new List<Tuple<string, string, DateTime>>();
        List<Tuple<string, string>> subsMult = new List<Tuple<string, string>>();

        public TimeService() : base("Time")
        {
            Bind("WhatTime", SendTime);
            Bind("WhatMult", SendMult);
            Bind<Tuple<string,string,DateTime>>("TimeSubscribe", AddSubscriberTime);
            Bind<Tuple<string, string>>("MultSubscribe", AddSubscriberMult);
        }

        public void Log(string message)
        {
            Console.WriteLine(message);
            RemoteCall("Log", "Write", message);
        }

        public void AddSubscriberTime(Tuple<string, string, DateTime> sub)
        {
            foreach (var item in subsTime)
            {
                if ((item.Item1 == sub.Item1) && (item.Item2 == sub.Item2) && (item.Item3 == sub.Item3)) return;
            }
            subsTime.Add(sub);
            string msg = $"Сервис {sub.Item1} подписан на уведомление о достижении времени({sub.Item3}).";
            Log(msg);
        }

        public void AddSubscriberMult(Tuple<string, string> sub)
        {
            foreach (var item in subsMult)
            {
                if ((item.Item1 == sub.Item1) && (item.Item2 == sub.Item2)) return;
            }
            subsMult.Add(sub);
            string msg = $"Сервис {sub.Item1} подписан на уведомление об изменении мультипликатора.";
            Log(msg);
        }

        public DateTime SendTime()
        {
            string msg = $"Получен и выполняется запрос текущего времени.";
            Log(msg);
            return time;
        }

        public int SendMult()
        {
            string msg = $"Получен и выполняется запрос текущего мультипликатора.";
            Log(msg);
            return coef;
        }

        public void StartService()
        {
            cancelTokenSource = new CancellationTokenSource();
            token = cancelTokenSource.Token;
            task = new Task(() => Do(token));
            task.Start();
            Change_Coef(coef);
        }

        public void Do(CancellationToken token)
        {
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Время остановлено!");
                    foreach (var item in subsMult)
                    {
                        RemoteCall(item.Item1, item.Item2, 0);
                    }
                    return;
                }
                Thread.Sleep(1000 / coef);
                time = time.AddMinutes(1);
                if ((time.Minute == 0) && (time.Hour == 12)) Console.WriteLine("Полдень :^) " + time);
                foreach (var item in subsTime)
                {
                    if (time == item.Item3) RemoteCall(item.Item1, item.Item2, item.Item3);
                    string msg = $"Передано сообщение для {item.Item1} о достижении времени: {item.Item3}.";
                    Log(msg);
                    subsTime.Remove(item);
                }
            }
        }

        public void StopService()
        {
            cancelTokenSource.Cancel();
        }

        public void Show_Time()
        {
            Console.WriteLine(time);
        }

        public void Change_Coef(int coef)
        {
            this.coef = coef;
            string msg = $"Мультипликатор сменился на {coef}";
            Log(msg);
            foreach (var item in subsMult)
            {
                RemoteCall(item.Item1, item.Item2, coef);
                string msg1 = $"Подписчик {item.Item1} уведомлен о смене мультипликатора на {coef}";
                Log(msg1);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Config.IsLocal = false;
            //Config.RequestTimeout = 0; //TODO убрать
            TimeService time = new TimeService();
            time.Start(true);
            Console.WriteLine("Time is up!");

            time.StartService();

            while (true)
            {
                int k = Convert.ToInt32(Console.ReadLine());
                if (k == 0)   //показать
                {
                    time.Show_Time();
                    continue;
                }
                if (k == -1) // стоп
                {
                    time.StopService();
                    continue;
                }
                if (k == -2) // старт
                {
                    time.StartService();
                    continue;
                }
                time.Change_Coef(k);
            }
        }

    }
}
