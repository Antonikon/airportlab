﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform;
namespace GraphPath
{
    public enum NodeTypes
    {
        Road,
        Garage,
        FuelTank,
        PlaneParking,
        PassengerStorage,
        FieldIn,
        FieldOut,
        Exit
    }
    public struct Node
    {
        //coords for drawing, upper left of BB rectangle
        public float x { get; }
        public float y { get; }
        public NodeTypes type;
        public Node(float x, float y, NodeTypes type = NodeTypes.Road)
        {
            this.x = x;
            this.y = y;
            this.type = type;
        }
    }
    public class Graph
    {
        private Dictionary<int, Node> nodes = new Dictionary<int, Node>
        {
            { 1, new Node(90, 0, NodeTypes.FieldOut) },
            { 2, new Node(195, 0, NodeTypes.FieldIn) },
            { 3, new Node(90, 25) },
            { 4, new Node(120, 25) },
            { 5, new Node(145, 25) },
            { 6, new Node(170, 25) },
            { 7, new Node(195, 25) },
            { 8, new Node(90, 50) },
            { 9, new Node(120, 50) },
            { 10, new Node(140, 50) },
            { 11, new Node(170, 50) },
            { 12, new Node(195, 50) },
            { 13, new Node(65, 70) },
            { 14, new Node(115, 65) },
            { 15, new Node(140, 70) },
            { 16, new Node(155, 75) },
            { 17, new Node(175, 65) },
            { 18, new Node(200, 75) },
            { 19, new Node(0, 70, NodeTypes.Garage) },
            { 20, new Node(20, 70) },
            { 21, new Node(40, 85) },
            { 22, new Node(70, 85) },
            { 23, new Node(95, 85) },
            { 24, new Node(125, 90) },
            { 25, new Node(155, 90) },
            { 26, new Node(185, 90) },
            { 27, new Node(200, 100) },
            { 28, new Node(20, 95) },
            { 29, new Node(40, 105) },
            { 30, new Node(60, 100) },
            { 31, new Node(105, 105) },
            { 32, new Node(135, 100) },
            { 33, new Node(170, 110) },
            { 34, new Node(0, 120, NodeTypes.FuelTank) },
            { 35, new Node(20, 120) },
            { 36, new Node(50, 120) },
            { 37, new Node(85, 120) },
            { 38, new Node(115, 120) },
            { 39, new Node(145, 120) },
            { 40, new Node(200, 120) },
            { 41, new Node(85, 140, NodeTypes.PlaneParking) },
            { 42, new Node(115, 140, NodeTypes.PlaneParking) },
            { 43, new Node(145, 140, NodeTypes.PlaneParking) },
            { 44, new Node(170, 130) },
            { 45, new Node(190, 140, NodeTypes.PassengerStorage) },
            { 46, new Node(215, 120, NodeTypes.Exit) },
        };
        private Dictionary<(int node1, int node2), int> twoWayRoads = new Dictionary<(int node1, int node2), int>
        {
            {(1, 3), 3},
            {(3, 4), 4},
            {(4, 5), 3},
            {(5, 6), 3},
            {(6, 7), 3},
            {(7, 12), 3},
            {(2, 7), 3},
            {(3, 8), 3},
            {(8, 9), 4},
            {(9, 10), 2},
            {(10, 11), 4},
            {(11, 12), 3},
            {(8, 13), 4},
            {(8, 14), 4},
            {(10, 15), 2},
            {(12, 17), 3},
            {(12, 18), 3},
            {(13, 21), 4},
            {(14, 24), 4},
            {(15, 24), 3},
            {(17, 16), 2},
            {(16, 24), 5},
            {(19, 20), 2},
            {(20, 21), 3},
            {(20, 28), 3},
            {(28, 35), 3},
            {(34, 35), 2},
            {(29, 35), 3},
            {(21, 29), 2},
            {(21, 22), 4},
            {(21, 30), 3},
            {(35, 36), 4},
            {(22, 23), 3},
            {(23, 24), 4},
            {(30, 37), 4},
            {(36, 37), 5},
            {(24, 25), 4},
            {(24, 31), 3},
            {(24, 32), 2},
            {(24, 38), 5},
            {(31, 37), 3},
            {(37, 38), 4},
            {(37, 41), 2},
            {(38, 41), 5},
            {(38, 42), 2},
            {(38, 39), 4},
            {(38, 43), 5},
            {(41, 42), 4},
            {(42, 43), 4},
            {(32, 39), 2},
            {(39, 43), 2},
            {(25, 26), 4},
            {(26, 27), 2},
            {(18, 27), 3},
            {(27, 33), 4},
            {(33, 39), 3},
            {(39, 44), 4},
            {(27, 40), 2},
            {(40, 45), 3},
            {(44, 45), 2},
            {(40, 46), 1}
        };

        public IReadOnlyDictionary<int, Node> Nodes => nodes;
        public IReadOnlyDictionary<(int node1, int node2), int> Roads => twoWayRoads;
        public Graph() { }
        public Graph(Pathfinder pathFinder)
        {
            foreach (var item in twoWayRoads)
            {
                pathFinder.AddRoad(item.Key.node1, item.Key.node2, item.Value);
                pathFinder.AddRoad(item.Key.node2, item.Key.node1, item.Value);
            }
            pathFinder.Process();
        }

    }
}
