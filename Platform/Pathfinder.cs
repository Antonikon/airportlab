﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace Platform {
    internal class WayInfo {
        public int Length = int.MaxValue;
        public List<int> Way = new List<int>();

        public WayInfo() { }
        public WayInfo(int directWay) {
            Length = directWay;
        }

    }

    /// <summary>
    /// Класс для поиска пути в графе, оптимизирован для большого количества запросов по одному графу.
    /// </summary>
    public class Pathfinder {
        private Dictionary<(int From, int To), int> Roads = new Dictionary<(int, int), int>();
        private ConcurrentDictionary<(int From, int To), WayInfo> Result;

        /// <summary>
        /// Добавить путь из from в to с длинной length.
        /// </summary>
        public void AddRoad(int from, int to, int length) {
            Roads[(from, to)] = length;
        }

        /// <summary>
        /// Удалить введёные данные.
        /// </summary>
        public void Reset() {
            Roads.Clear();
            Result = null;
        }


        /// <summary>
        /// Обработать граф и рассчитать пути.
        /// </summary>
        public void Process() {
            var Nodes = new SortedSet<int>();
            Result = new ConcurrentDictionary<(int From, int To), WayInfo>();
            foreach(var road in Roads) {
                Nodes.Add(road.Key.From);
                Nodes.Add(road.Key.To);
                Result[road.Key] = new WayInfo(road.Value);
            }

            foreach (var k in Nodes) {
                foreach (var i in Nodes) {
                    foreach (var j in Nodes) {
                        if (!Result.ContainsKey((i, k))) continue;
                        if (!Result.ContainsKey((k, j))) continue;
                        var partA = Result[(i, k)];
                        var partB = Result[(k, j)];
                        if (Result.ContainsKey((i, j))) {
                            var old = Result[(i, j)];
                            if (partA.Length + partB.Length < old.Length) {
                                old.Length = partA.Length + partB.Length;
                                old.Way = new List<int>(partA.Way);
                                old.Way.Add(k);
                                old.Way.AddRange(partB.Way);
                            }
                        } else {
                            var way = new WayInfo();
                            way.Way = new List<int>(partA.Way);
                            way.Way.Add(k);
                            way.Way.AddRange(partB.Way);
                            way.Length = partA.Length + partB.Length;
                            Result[(i, j)] = way;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Возвращает путь из from в to.
        /// <see langword="null"/> если пути не существует.
        /// </summary>
        public List <int> GetRoad(int from, int to) {
            if (!Result.ContainsKey((from, to))) return null;
            var way = Result[(from, to)];
            var result = new List<int>(way.Way);
            result.Add(to);
            return result;
        }

        /// <summary>
        /// Возвращает суммарную длинну кратчайшего пути из from в to.
        /// -1 если пути не существует.
        /// </summary>
        public int GetWayLength(int from, int to) {
            if (!Result.ContainsKey((from, to))) return -1;
            return Result[(from, to)].Length;
        }

        /// <summary>
        /// Возвращает длинну одной дороги.
        /// </summary>
        public int GetRoadLength(int from, int to) {
            return Roads[(from, to)];
        }
    }
}