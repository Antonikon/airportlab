﻿using System;
using System.Threading;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;


namespace Platform.Network {
    public abstract class BasicEntity: IDisposable {
        public readonly string Name;

        private ConnectionFactory MQConnectionFactory;
        protected IConnection MQConnection;
        private IModel MQChannel;
        private EventingBasicConsumer MQConsumer;

        protected static JsonSerializerSettings SerializerSettings;
        protected static JsonSerializer Serializer;

        static BasicEntity() {
            SerializerSettings = new JsonSerializerSettings();
            SerializerSettings.MissingMemberHandling = MissingMemberHandling.Error;
            SerializerSettings.NullValueHandling = NullValueHandling.Include;

            Serializer = JsonSerializer.Create(SerializerSettings);
        }

        protected BasicEntity(string name, bool async, bool exclusive) {
            Name = name;

            MQConnectionFactory = new ConnectionFactory();
            MQConnectionFactory.VirtualHost = "/" + Config.Env;
            if (Config.IsLocal) MQConnection = MQConnectionFactory.CreateConnection("localhost");
            else {
                MQConnectionFactory.HostName = Config.RemoteHost;
                MQConnectionFactory.UserName = Config.User;
                MQConnectionFactory.Password = Config.GetPassword();
                MQConnection = MQConnectionFactory.CreateConnection();
            }
            MQChannel = MQConnection.CreateModel();
            MQChannel.QueueDeclare(name, false, exclusive, false);

            MQConsumer = new EventingBasicConsumer(MQChannel);
            if (async) {
                MQConsumer.Received += (sender, args) =>
                    ThreadPool.QueueUserWorkItem((object state) => OnMessageReceived(args));
            } else {
                MQConsumer.Received += (sender, args) => OnMessageReceived(args);
            }
        }

        ~BasicEntity() {
            Stop();
        }

        protected void Listen() {
            MQChannel.BasicConsume(Name, true, MQConsumer);
        }


        public void Stop() {
            if (MQChannel != null) {
                MQChannel.Close();
                MQChannel = null;
            }
            if (MQConnection != null) {
                MQConnection.Close();
                MQConnection = null;
            }
        }

        protected abstract void OnMessageReceived(BasicDeliverEventArgs args);

        public static void Log(int level, string line) {
            if (level < Config.LogLevel) return;
            string levelName;
            if (level == 0) levelName = "INFO";
            else if (level == 1) levelName = "WARNING";
            else levelName = "ERROR";
            Console.WriteLine($"[{levelName}][{DateTime.Now.ToString("hh:mm:ss")}] {line}");
        }

        public void Dispose() {
            Stop();
        }
    }

}
