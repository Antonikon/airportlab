﻿using System;
using System.Text;
using System.Collections.Concurrent;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using Platform.Network.Interfaces;
using Platform.Network.Protocol;

namespace Platform.Network {
    internal struct ResponceData {
        public JToken Result;
        public string Exception;
    }

    public class BasicClient : BasicEntity, IBasicClient {

        private object RequestIDLock = new object();
        private ulong RequestID = 0;

        private ConcurrentDictionary<ulong, ManualResetEvent> AwaitingResponses = new ConcurrentDictionary<ulong, ManualResetEvent>();
        private ConcurrentDictionary<ulong, ResponceData> Responses = new ConcurrentDictionary<ulong, ResponceData>();

        public BasicClient(string name) :
            base(name, true, true) {
            Listen();
        }

        private RequestMsg BuildRequest(string method, object args, bool needResponse) {
            RequestMsg msg;
            if (needResponse) {
                lock (RequestIDLock) {
                    msg.Id = RequestID;
                    RequestID++;
                }
                msg.ResponseTo = Name;
            } else {
                msg.Id = 0;
                msg.ResponseTo = null;
            }
            msg.Method = method;

            if (args is null) msg.Args = null; 
            else msg.Args = JToken.FromObject(args);
            return msg;
        }

        protected sealed override void OnMessageReceived(BasicDeliverEventArgs args) {
            var body = args.Body;
            var json = Encoding.UTF8.GetString(body);
            ResponseMsg msg;
            try {
                msg = JsonConvert.DeserializeObject<ResponseMsg>(json, SerializerSettings);
            } catch (Exception) {
                Log(1, "Bad response received");
                return;
            }

            ManualResetEvent sync;
            bool status = AwaitingResponses.TryRemove(msg.Id, out sync);
            if (!status) {
                Log(0, $"Unwanted response received: {msg.Id}");
                return;
            }

            ResponceData responce;
            responce.Result = msg.Result;
            responce.Exception = msg.Exception;

            Responses[msg.Id] = responce;
            sync.Set();
        }

        public void RemoteRequest(string service, string method, object args = null) {
            RemoteRequest<JToken>(service, method, args, Config.RequestTimeout);
        }

        public void RemoteRequest(string service, string method, object args, uint timeout) {
            RemoteRequest<JToken>(service, method, args, timeout);
        }

        public R RemoteRequest<R>(string service, string method, object args = null) {
            return RemoteRequest<R>(service, method, args, Config.RequestTimeout);
        }


        public R RemoteRequest<R>(string service, string method, object args, uint timeout) {

            //channel.QueueDeclarePassive(service);

            RequestMsg msg = BuildRequest(method, args, true);
            var json = JsonConvert.SerializeObject(msg, SerializerSettings);
            var body = Encoding.UTF8.GetBytes(json);

            var channel = MQConnection.CreateModel();
            channel.BasicPublish("", service, null, body);

            var sync = new ManualResetEvent(false);
            AwaitingResponses[msg.Id] = sync;

            bool status = true;
            if (timeout > 0) status = sync.WaitOne((int)timeout * 1000);
            else sync.WaitOne();

            ResponceData result;
            if (status) {
                Responses.TryRemove(msg.Id, out result);

                if (result.Exception == null) {
                    if (result.Result == null) return default(R);
                    try {
                        if (typeof(R).IsAssignableFrom(result.Result.GetType())) return (R)(object)result.Result;
                        else return result.Result.ToObject<R>();
                    } catch (Exception e) {
                        throw new InvalidOperationException($"Wrong response type '({typeof(R).FullName})' for '{service}->{method}'", e);
                    }
                } else {
                    throw new InvalidOperationException($"Exception in remote request: {result.Exception}");
                }
            } else {
                AwaitingResponses.TryRemove(msg.Id, out sync);
                Responses.TryRemove(msg.Id, out result);
                throw new InvalidOperationException($"Request '{method}' to '{service}' timeout");
            }
        }

        public void RemoteCall(string service, string method, object args) {
            //channel.QueueDeclarePassive(service);

            RequestMsg msg = BuildRequest(method, args, false);
            var json = JsonConvert.SerializeObject(msg, SerializerSettings);
            var body = Encoding.UTF8.GetBytes(json);

            var channel = MQConnection.CreateModel();
            channel.BasicPublish("", service, null, body);
        }
    }
}