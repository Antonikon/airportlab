﻿using System;
using System.Collections.Concurrent;
using Platform.Network.Interfaces;
using Platform.Network.Modules;

namespace Platform.Network {
    /// <summary>
    /// Базовый класс для создания сервиса, предоставляет функционал удалённых вызовов
    /// между сервисами.
    /// </summary>
    public class BasicService: IBasicService, IDisposable {

        private BasicClient Client;
        private BasicServer Server;

        //private List<BasicModule> Modules = new List<BasicModule>();
        private ConcurrentDictionary<Type, BasicModule> Modules = new ConcurrentDictionary<Type, BasicModule>();

        public string Name { get => Server.Name; }

        /// <param name="name">Имя сервиса, по которому к нему могут обращаться другие сервисы.</param>
        /// <param name="async">Если установлено значение <c>true</c> то запросы будут обрабатываться параллельно,
        /// иначе последовательно.</param>
        public BasicService(string name, bool async = false) {
            Client = new BasicClient(name + "_responses");
            Server = new BasicServer(name, async);
        }

        /// <summary>
        /// Добавляет модуль для сервиса.
        /// </summary>
        /// <typeparam name="T">Тип модуля.</typeparam>
        public void AddModule<T>() where T : BasicModule, new() {
            if (Modules.ContainsKey(typeof(T))) {
                throw new InvalidOperationException($"Module {typeof(T).FullName} already existis");
            }
            try {
                var mod = new T();
                Console.WriteLine($"Initializing module: {mod.Name}");
                mod.Initialize(this);
                Modules[typeof(T)] = mod;
                Console.WriteLine($"Module {mod.Name} initialized");
            } catch (Exception e) {
                Console.WriteLine($"Error during module initialization: {typeof(T).FullName}");
                Console.WriteLine(e);
                Console.WriteLine(e.Message);
            }
        }

        public T GetModule<T>() where T: BasicModule {
            return (T)Modules[typeof(T)];
        }

        /// <summary>
        /// Делает метод доступным для вызова по сети
        /// </summary>
        /// <param name="method">Название метода, которое будет использоваться для вызова</param>
        /// <param name="func">Ссылка на мето</param>
        /// <typeparam name="T">Тип принимаемого параметра</typeparam>
        public void Bind<T>(string method, Action<T> func) {
            Server.Bind<T>(method, func);
        }

        /// <summary>
        /// Делает метод доступным для вызова по сети
        /// </summary>
        /// <param name="method">Название метода, которое будет использоваться для вызова</param>
        /// <param name="func">Ссылка на метод</param>
        public void Bind(string method, Action func) {
            Server.Bind(method, func);
        }

        /// <summary>
        /// Делает метод доступным для вызова по сети
        /// </summary>
        /// <param name="method">Название метода, которое будет использоваться для вызова</param>
        /// <param name="func">Ссылка на мето</param>
        /// <typeparam name="R">Тип возвращаемого значения</typeparam>
        /// <typeparam name="T">Тип принимаемого параметра</typeparam>
        public void Bind<R, T>(string method, Func<T, R> func) {
            Server.Bind(method, func);
        }

        /// <summary>
        /// Делает метод доступным для вызова по сети
        /// </summary>
        /// <param name="method">Название метода, которое будет использоваться для вызова</param>
        /// <param name="func">Ссылка на мето</param>
        /// <typeparam name="R">Тип возвращаемого значения</typeparam>
        public void Bind<R>(string method, Func<R> func) {
            Server.Bind(method, func);
        }


        /// <summary>
        /// Запуск бесконечного цикла обработки запросов.
        /// </summary>
        /// <param name="background">Если true, то будет запущено без блокировки основной программы</param>
        public void Start(bool background) {
            Server.Start(background);
        }

        /// <summary>
        /// Остановить обработку запросов и отключится от очереди сообщений.
        /// </summary>
        public void Stop() {
            Server.Stop();
            Client.Stop();
        }


        /// <summary>
        /// Вызывает метод удалённого сервиса и ожидает ответа.
        /// В случае превышения заданного времени ожидания вызывает исключение.
        /// При возникновении исключения в удалённой функции, будет создано локальное исключение.
        /// </summary>
        // <returns>Результат выполнения удленной функции</returns>
        /// <param name="service">Имя сервиса</param>
        /// <param name="method">Имя метода</param>
        /// <param name="args">Аргументы</param>
        /// <param name="timeout">Максимальное время ожидания.
        /// При значении 0 время ожидания неограниченно. </param>
        /// <typeparam name="R">Тип возвращаемого значения</typeparam>
        public R RemoteRequest<R>(string service, string method, object args, uint timeout) {
            return Client.RemoteRequest<R>(service, method, args, timeout);
        }

        /// <summary>
        /// Вызывает метод удалённого сервиса и ожидает ответа.
        /// В случае превышения заданного времени ожидания вызывает исключение.
        /// При возникновении исключения в удалённой функции, будет создано локальное исключение.
        /// </summary>
        // <returns>Результат выполнения удленной функции</returns>
        /// <param name="service">Имя сервиса</param>
        /// <param name="method">Имя метода</param>
        /// <param name="args">Аргументы</param>
        /// <typeparam name="R">Тип возвращаемого значения</typeparam>
        public R RemoteRequest<R>(string service, string method, object args = null) {
            return Client.RemoteRequest<R>(service, method, args);
        }

        /// <summary>
        /// Вызывает метод удалённого сервиса и ожидает ответа.
        /// В случае превышения заданного времени ожидания вызывает исключение.
        /// При возникновении исключения в удалённой функции, будет создано локальное исключение.
        /// </summary>
        // <returns>Результат выполнения удленной функции</returns>
        /// <param name="service">Имя сервиса</param>
        /// <param name="method">Имя метода</param>
        /// <param name="args">Аргументы</param>       
        public void RemoteRequest(string service, string method, object args = null) {
            Client.RemoteRequest(service, method, args);
        }

        /// <summary>
        /// Вызывает метод удалённого сервиса и ожидает ответа.
        /// В случае превышения заданного времени ожидания вызывает исключение.
        /// При возникновении исключения в удалённой функции, будет создано локальное исключение.
        /// </summary>
        // <returns>Результат выполнения удленной функции</returns>
        /// <param name="service">Имя сервиса</param>
        /// <param name="method">Имя метода</param>
        /// <param name="args">Аргументы</param>
        /// <param name="timeout">Максимальное время ожидания.
        /// При значении 0 время ожидания неограниченно. </param>
        public void RemoteRequest(string service, string method, object args, uint timeout) {
            Client.RemoteRequest(service, method, args, timeout);
        }

        /// <summary>
        /// Вызывает метод удалённого сервиса без ожидания ответа или сообщения об ошибке.
        /// </summary>
        /// <returns>Результат выполнения удленной функции</returns>
        /// <param name="service">Имя сервиса</param>
        /// <param name="method">Имя метода</param>
        /// <param name="args">Аргумент</param>
        public void RemoteCall(string service, string method, object args = null) {
            Client.RemoteCall(service, method, args);
        }

        public void Dispose() {
            Stop();
        }
    }
}
