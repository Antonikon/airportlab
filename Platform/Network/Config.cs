﻿using System;
using System.IO;
using System.Threading;

namespace Platform.Network {
    /// <summary>
    /// Глобальные настройки для BasicClient, BasicServer, BasicService
    /// </summary>
    public static class Config {
        /// <summary>
        /// Время ожидания ответа на запрос по умолчанию.
        /// При значении 0 время ожидания неограниченно.
        /// </summary>
        public static uint RequestTimeout = 0;

        /// <summary>
        /// Настройка минимального уровня логированиия:
        /// 0 - INFO
        /// 1 - WARNING
        /// 2 - ERROR
        /// </summary>
        public static uint LogLevel = 0;

        /// <summary>
        /// При значении true используется локальный сервер сообщений, иначе удалённый.
        /// </summary>
        public static bool IsLocal = true;
        public static string RemoteHost = "82.146.32.243";
        public static string Env = "";
        public static string User = "public";
        private static string Password = null;
        private static string PasswordFile = "password.pass";


        static Config() {
            var result = ThreadPool.SetMaxThreads(32, 32);
            Console.WriteLine($"Max threads {result}"); 
            result = ThreadPool.SetMinThreads(32, 32);
            Console.WriteLine($"Min threads {result}");
            var args = Environment.GetCommandLineArgs();
            if (args[0].ToLower().Contains("nunit")) {
                Env = "test";
            }
        }

        public static string GetPassword() {
            if (Password != null) return Password;
            if (File.Exists(PasswordFile)) {
                Password = File.ReadAllText(PasswordFile);
            } else {
                Console.WriteLine("Введите пароль к удалённому серверу:");
                Password = Console.ReadLine();
                File.WriteAllText(PasswordFile, Password);
            }
            return Password;
        }
    }
}