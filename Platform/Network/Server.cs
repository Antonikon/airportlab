﻿using System;
using System.Text;
using System.Collections.Concurrent;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

using Platform.Network.Interfaces;
using Platform.Network.Protocol;


namespace Platform.Network {
    internal struct MethodInfo {
        public Func<object, object> Func;
        public Type ArgsType;
    }

    public class BasicServer : BasicEntity, IBasicServer {

        private ConcurrentDictionary<string, MethodInfo> Methods = new ConcurrentDictionary<string, MethodInfo>();

        public BasicServer(string name, bool async = false) :
            base(name, async, true) { }


        public void Bind<R, T>(string method, Func<T, R> func) {
            MethodInfo info;
            info.Func = (object arg) => func((T)arg);
            info.ArgsType = typeof(T);
            Methods[method] = info;
        }

        public void Bind<R>(string method, Func<R> func) {
            MethodInfo info;
            info.Func = (object arg) => func();
            info.ArgsType = null;
            Methods[method] = info;
        }

        public void Bind<T>(string method, Action<T> func) {
            Bind<object, T>(method, (T arg) => { func(arg); return null; });
        }

        public void Bind(string method, Action func) {
            MethodInfo info;
            info.Func = (object arg) => { func(); return null; };
            info.ArgsType = null;
            Methods[method] = info;
        }

        public void Start(bool background) {
            Listen();
            if (!background) {
                ManualResetEvent waiter = new ManualResetEvent(false);
                waiter.WaitOne();
            }
        }

        protected override void OnMessageReceived(BasicDeliverEventArgs args) {
            var body = args.Body;
            var json = Encoding.UTF8.GetString(body);

            RequestMsg msg;
            try {
                msg = JsonConvert.DeserializeObject<RequestMsg>(json, SerializerSettings);
            } catch (Exception) {
                Log(1, "Bad request received");
                return;
            }

            if (msg.Method == null) {
                Log(1, "Null methond called");
                return;
            }

            ResponseMsg response = new ResponseMsg();

            try {

                if (!Methods.ContainsKey(msg.Method)) {
                    throw new ArgumentException($"Unknown method '{msg.Method}' called");
                }

                var methodInfo = Methods[msg.Method];
                object methodArgs = null;

                if (msg.Args != null && methodInfo.ArgsType != null) {
                    if (methodInfo.ArgsType.IsAssignableFrom(typeof(JToken))) {
                        methodArgs = msg.Args;
                    } else {
                        try {
                            methodArgs = msg.Args.ToObject(methodInfo.ArgsType, Serializer);
                        } catch (Exception e) {
                            throw new ArgumentException($"Wrong arguments for '{msg.Method}'", e);
                        }
                    }
                }

                Log(0, $"Method '{msg.Method}' called");
                try {
                    if (msg.ResponseTo == null) {
                        methodInfo.Func(methodArgs);
                    } else {
                        object result = null;
                        result = methodInfo.Func(methodArgs);
                        if (result != null) response.Result = JToken.FromObject(result);
                    }
                } catch (Exception e) {
                    response.Result = null;
                    response.Exception = e.ToString();
                    throw new Exception($"Exception during executing '{msg.Method}' with '{msg.Args.ToString()}'", e);
                }
            } catch (ArgumentException  e) {
                response.Result = null;
                response.Exception = e.Message;

                Log(1, e.Message);
                Log(1, e.ToString());
            } catch (Exception e) {
                response.Result = null;
                response.Exception = e.Message;

                Log(2, e.Message);
                Log(2, e.ToString());
            }

            if (msg.ResponseTo != null) {
                response.Id = msg.Id;

                var responseJson = JsonConvert.SerializeObject(response, SerializerSettings);
                var responseBody = Encoding.UTF8.GetBytes(responseJson);

                var channel = MQConnection.CreateModel();
                channel.BasicPublish("", msg.ResponseTo, null, responseBody);
            }
        }
    }
}