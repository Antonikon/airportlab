﻿namespace Platform.Network.Modules {
    public abstract class BasicModule {
        public readonly string Name;

        protected BasicModule(string name) {
            Name = name;
        }

        public abstract void Initialize(BasicService service);
    }
}