﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Platform.Network.Modules {
    internal struct Callback {
        public string Method;
        public object Args;
        public bool Once;
    }

   internal struct SubscribeArgs {
        public string Event;
        public string Service;
        public string Method;
        public bool Once;
        public object Args;
    }

    internal struct UnsubscribeArgs {
        public string Service;
        public string Event;
        public string Method;
    }


    public static class DefaultEventSystemExtension {
        /// <summary>
        /// Подписывается на событие удалённого сервиса
        /// </summary>
        /// <param name="service">Название сервиса к которому подписываемся</param>
        /// <param name="targetEvent">Название события на которое подписываемся</param>
        /// <param name="callback">Публичное название нашей функции которая будет вызвана при наступлении события</param>
        /// <param name="callbackArgs">Аргументы которые буду переданы нашей фукнции</param>
        /// <param name="once">Если <c>true</c> то мы будем оповещены о событии только один раз, иначе до отписки.</param>
        public static void Subscribe(this BasicService sub, string service, string targetEvent, string callback, object callbackArgs, bool once = true) {
            SubscribeArgs args;
            args.Service = sub.Name;
            args.Event = targetEvent;
            args.Method = callback;
            args.Args = callbackArgs;
            args.Once = once;
            sub.RemoteRequest(service, "subscribe", args);
        }

        /// <summary>
        /// Отписывается от событий одёлнного сервиса
        /// Этот метод имеет смысл вызывать, если при подписке мы указали что подписываемся до ручно отписки
        /// или если хотим отменить подписку на еденичное событие если оно еще не произошло.
        /// </summary>
        /// <param name="sub">Sub.</param>
        /// <param name="service">Service.</param>
        /// <param name="targetEvent">Target event.</param>
        /// <param name="callback">Callback.</param>
        public static void Unsubscribe(this BasicService sub, string service, string targetEvent = "", string callback = "") {
            UnsubscribeArgs args;
            args.Service = sub.Name;
            args.Event = targetEvent;
            args.Method = callback;
            sub.RemoteRequest(service, "unsubscribe", args);
        }
    }

    /// <summary>
    /// Модуль, добавляющий функции для подписки, отписки на события и их вызова.
    /// </summary>
    public class DefaultEventSystem: BasicModule {
        private ConcurrentDictionary<string, ConcurrentDictionary<string, List <Callback>>> Subscriptions
        = new ConcurrentDictionary<string, ConcurrentDictionary<string, List<Callback>>>();

        private BasicService Service;

        public DefaultEventSystem():
            base("Default Event System") 
        { }

        public override void Initialize(BasicService service) {
            if (Service != null) throw new InvalidOperationException("Don't initialize module manually.");
            Service = service;
            Service.Bind<SubscribeArgs>("subscribe", Subscribe);
            Service.Bind<UnsubscribeArgs>("unsubscribe", Unsubscribe);
        }

        private void Subscribe(SubscribeArgs args) {
            if (args.Event == null || args.Service == null || args.Method == null) return;
            if (args.Event == "" || args.Service == "" || args.Method == "") return;

            if (!Subscriptions.ContainsKey(args.Event)) {
                Subscriptions[args.Event] = new ConcurrentDictionary<string, List<Callback>>();
            }
            var eventSubs = Subscriptions[args.Event];
            if (!eventSubs.ContainsKey(args.Service)) {
                eventSubs[args.Service] = new List<Callback>();
            }
            var callbackList = eventSubs[args.Service];
            Callback call;
            call.Method = args.Method;
            call.Args = args.Args;
            call.Once = args.Once;
            lock (callbackList) callbackList.Add(call);
            BasicEntity.Log(0, $"{args.Service} subscribes on {args.Event}");
        }

        private void RemoveMethod(string method, List <Callback> methods) {
            lock (methods) {
                var removing = new List<int>();
                for (var i = 0; i < methods.Count; i++) {
                    if (methods[i].Method == method) removing.Add(i);
                }
                removing.Reverse();
                foreach (var index in removing) {
                    methods.RemoveAt(index);
                }
            }
        }

        private void Unsubscribe(UnsubscribeArgs args) {
            if (args.Service == null) return;
            if (args.Service == "") return;
            if (args.Event == null || args.Event == "") {
                foreach (var even in Subscriptions) {
                    if (args.Method == null || args.Method == "") {
                        List<Callback> tmp;
                        even.Value.TryRemove(args.Service, out tmp);
                    } else {
                        if (!even.Value.ContainsKey(args.Service)) continue;
                        var methods = even.Value[args.Service];
                        RemoveMethod(args.Method, methods);
                    }
                }
                BasicEntity.Log(0, $"{args.Service} unsubscribes from {args.Event}");
            } else {
                if (!Subscriptions.ContainsKey(args.Event)) return;
                var subs = Subscriptions[args.Event];
                if (!subs.ContainsKey(args.Service)) return;

                if (args.Method == null || args.Method == "") {
                    List<Callback> tmp;
                    subs.TryRemove(args.Service, out tmp);
                } else {
                    var methods = subs[args.Service];
                    RemoveMethod(args.Method, methods);
                }
                BasicEntity.Log(0, $"{args.Service} unsubscribes from {args.Event}");
            }
        }

        /// <summary>
        /// Создать событие и оповестить подписчиков на него.
        /// </summary>
        /// <param name="name">Название события</param>
        public void EmitEvent(string name) {
            if (!Subscriptions.ContainsKey(name)) return;
            var subs = Subscriptions[name];
            foreach(var subscriber in subs) {
                var methods = subscriber.Value;
                lock (methods) {
                    var removing = new List<int>();
                    for (var i = 0; i < methods.Count; i++) {
                        var callback = methods[i];
                        Service.RemoteCall(subscriber.Key, callback.Method, callback.Args);
                        if (callback.Once) removing.Add(i);
                    }

                    removing.Reverse();
                    foreach (var index in removing) methods.RemoveAt(index);
                }
            }
        }
    }
}