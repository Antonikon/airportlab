﻿using Newtonsoft.Json.Linq;

namespace Platform.Network.Protocol {
    public struct ResponseMsg {
        public ulong Id;
        public JToken Result;
        public string Exception;
    }

    public struct RequestMsg {
        public string ResponseTo;
        public ulong Id;
        public string Method;
        public JToken Args;
    }
}