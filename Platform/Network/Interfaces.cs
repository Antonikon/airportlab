﻿using System;

namespace Platform.Network.Interfaces {
    public interface IBasicClient {
        void RemoteRequest(string service, string method, object args = null);
        void RemoteRequest(string service, string method, object args, uint timeout);

        R RemoteRequest<R>(string service, string method, object args = null);
        R RemoteRequest<R>(string service, string method, object args, uint timeout);

        void RemoteCall(string service, string method, object args = null);
    }

    public interface IBasicServer {

        void Bind(string method, Action func);
        void Bind<R, T>(string method, Func<T, R> func);
        void Bind<R>(string method, Func<R> func);
        void Bind<T>(string method, Action<T> func);


        void Start(bool background);
        void Stop();
    }

    public interface IEventSystem {
        void Subscribe<T>(string service, string targetEvent, string method, T args, bool single = true);
        void Unsubscribe(string service, string targetEvent = "", string method = "");
        void EmitEvent(string eventName);
        void EmitEvent<T>(string eventName, T args);
    }

    public interface IBasicService : IBasicClient, IBasicServer { }
    

}