﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Platform.Network;
using Platform;
using GraphPath;

namespace FollowMe
{
    //НОДЫ
    //38 - очередь
    //19 - гараж
    //1 -взлетная полоса
    //2 - посадочная полоса

    // TODO сделать общий счетчик до гуида и удаления ненужных машинок из листа

    public class FollowMe
    {
        public static int numFM = 0;
        public string GUID ;
        public int node;

        public string pl_GUID;
        public int pl_countPassengers;
        public string pl_flight;

        public FollowMe(string flight)
        {
            node = 19;
            pl_flight = flight;
            numFM += 1;
            GUID = "f_" + numFM;
        }

        public void GrabPlane(string GUID, int node, int countPassengers)
        {
            pl_GUID = GUID;
            pl_countPassengers = countPassengers;
        }

        public void DumpPlane()
        {
            pl_GUID = "";
            pl_countPassengers = 0;
            pl_flight = "";
        }
    }

    public class FollowMeService : BasicService
    {
        static Pathfinder pathfinder = new Pathfinder();
        readonly Graph graph = new Graph(pathfinder);

        int TimeMult;

        List<FollowMe> cars = new List<FollowMe>();

        public FollowMeService() : base("FollowMe")
        {
            Bind<(int, string)>("ParkToSlot", ParkToSlot);
            Bind<(int, string)>("EscortToTakeoff", EscortToTakeoff);
            Bind<string>("EscortToQueue", EscortToQueue);
            Bind<(int, string)>("EscortFromQueue", EscortFromQueue);
            Bind<int>("TimeMultChange", ChangeMult);
        }

        public void Log(string message)
        {
            //Console.WriteLine(message);
            //RemoteCall("Log", "Write", message);
        }

        public void LogProblem(string message)  
        {
            Console.WriteLine(message);
            RemoteCall("Log", "AddLog", new Tuple<int, string>(1, message));
        }

        public void SetTimeMult()
        {
            TimeMult = RemoteRequest<int>("Time", "WhatMult");
            RemoteCall("Time", "MultSubscribe", new Tuple<string, string>("FollowMe", "TimeMultChange"));
        }

        public void ChangeMult(int t)  // logi est
        {
            TimeMult = t;
            string msg = $"Получен новый мультипликатор: {TimeMult}";
            Log(msg);
        }

        public bool GMC(string GUID, bool type, int from, int to) //type: true - для запроса движения, false - для освобождения дороги
        {
            return RemoteRequest<bool>("ControlTransport", "SetRoad", (GUID, type, from, to));
        }

        void Driving(string f_GUID, string p_GUID,int from, int to, int p_countPassengers)  // logi est
        {
            List<int> path = pathfinder.GetRoad(from, to);
            Log($"Проложен оптимальный маршрут до пункта назначения.");
            path.Insert(0,from);
            Console.Write(f_GUID + " ");
            foreach (var item in path)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();

            for (int i = 0; i < path.Count-1; i++)
            {
                RemoteCall("GRAPHICS", "SetPosition", (f_GUID, path[i]));
                //Log($"Визуализатору сообщается о движении машинки {f_GUID}. Путь {path[i]} to {path[i+1]}.");
                if (p_GUID != "")
                {
                    RemoteCall("GRAPHICS", "SetPositionLoad", (p_GUID, path[i], p_countPassengers));
                    //Log($"Визуализатору сообщается о движении самолета {f_GUID}. Путь {path[i]} to {path[i + 1]}.");
                }
                while (true)
                {
                    if (GMC(f_GUID, true, path[i], path[i + 1])) break;
                    Thread.Sleep(50/TimeMult);
                }
                Log($"ControlTransport занял ребро между {path[i]} и {path[i + 1]}.");

                int lengthRoad = pathfinder.GetRoadLength(path[i], path[i + 1]);
                for (double j = 0; j <= lengthRoad; j+=0.4) 
                {
                    RemoteCall("GRAPHICS", "UpdatePosition", (f_GUID, path[i], path[i+1], (float)j/lengthRoad));
                    if (p_GUID != "")
                    {
                        RemoteCall("GRAPHICS", "UpdatePositionLoad", (p_GUID, path[i], path[i + 1], (float)j / lengthRoad, p_countPassengers));
                        j -= 0.2;
                    }
                    Thread.Sleep(160/TimeMult);
                }

                if (p_GUID != "")
                {
                    RemoteCall("GRAPHICS", "SetPositionLoad", (p_GUID, path[i + 1], p_countPassengers));
                }
                GMC(f_GUID, false, path[i], path[i + 1]);
                Log($"ControlTransport освободил ребро между {path[i]} и {path[i + 1]}.");
            }
        }
        
        void ParkToSlot_Task((int parkingId, string flight) data)
        {
            //cars.Add(new FollowMe(data.flight));
            FollowMe car = new FollowMe(data.flight);
            //int pos = cars.Count - 1;

            Driving(car.GUID, "", car.node, 2, 0);
            car.node = 2;

            try
            {
                var tuple = RemoteRequest<Tuple<string, int, int>>("Plane", "TakePlane", data.flight);
                car.GrabPlane(tuple.Item1, tuple.Item2, tuple.Item3);
                Log($"Взят самолет с GUID {tuple.Item1}.Сервис Plane, TakePlane.");
            }
            catch
            {
                LogProblem($"Упал запрос к сервису Plane, TakePlane.");
            }     

            Driving(car.GUID, car.pl_GUID, car.node, data.parkingId, car.pl_countPassengers);
            car.node = data.parkingId;

            RemoteCall("Plane", "LeavePlane", new Tuple<int, string>(data.parkingId, data.flight));       
            Log($"Откреплён самолет с GUID {car.pl_GUID}.Сервис Plane, TakePlane.");
            car.DumpPlane();

            RemoteCall("AGH", "PlaneParked", data.parkingId);
            Log($"Сообщил в сервис AGH, PlaneParked номер парковки самолета.");

            Driving(car.GUID, "", car.node, 19, 0);
            car.node = 19;

            RemoteCall("GRAPHICS", "Kill", car.GUID);
            Log($"Запрос в сервис GRAPHICS, Kill на удаление {car.GUID} с графа.");
        }

        void ParkToSlot((int parkingId, string flight) data)
        {
            // [D] 1) Создаю машинку в гараже
            // [D] 2) Еду до самолета(посадочной полосы) кратчайшим путём, соблюдая пдд, передаю данные передвижения визуализатору
            // [D] 3) RemoteRequest. Передаю самолету рейс. Получаю данные от самолета (TakePlane(стринг рейс)) [DONE]
            // [D] 4) Еду до ангара кратчайшим путем, соблюдая пдд, и передавая визуализатору также данные самолета
            // [D] 5) Отдаю в ангар самолет, пересылаю данные обратно Дане (Tuple<int, string>)
            // [D] 6) Сообщаю АГХ, что довез
            // [D] 7) Еду в гараж

            var task = Task.Factory.StartNew(() => ParkToSlot_Task(data));

            //Task task = new Task(() => ParkToSlot_Task(data));
            //task.Start();
        }

        void EscortToTakeoff_Task((int parkingId, string flight) data)
        {
            //cars.Add(new FollowMe(data.flight));
            FollowMe car = new FollowMe(data.flight);
            //int pos = cars.Count - 1;

            Driving(car.GUID, "", car.node, data.parkingId, 0);
            car.node = data.parkingId;

            try
            {
                var tuple = RemoteRequest<Tuple<string, int, int>>("Plane", "TakePlane", data.flight);
                car.GrabPlane(tuple.Item1, tuple.Item2, tuple.Item3);
                Log($"Взят самолет с GUID {tuple.Item1}.Сервис Plane, TakePlane.");
            }
            catch
            {
                LogProblem($"Упал запрос к сервису Plane, TakePlane.");
            }

            RemoteCall("AGH", "Taken", data.parkingId);
            Log($"Сообщил в сервис AGH, PlaneParked номер парковки самолета.");
            Driving(car.GUID, car.pl_GUID, car.node, 1, car.pl_countPassengers);
            car.node = 1;

            RemoteCall("Plane", "LeavePlane", new Tuple<int, string>(1, data.flight));
            Log($"Откреплён самолет с GUID {car.pl_GUID}.Сервис Plane, TakePlane.");
            car.DumpPlane();

            RemoteCall("AGH", "ReadyToTakeoff", data.flight);
            Log($"Сообщил в сервис AGH, ReadyToTakeoff, что самолет доставлен на полосу.");

            Driving(car.GUID, "", car.node, 19, 0);
            car.node = 19;

            RemoteCall("GRAPHICS", "Kill", car.GUID);
            Log($"Запрос в сервис GRAPHICS, Kill на удаление {car.GUID} с графа.");
        }

        void EscortToTakeoff((int parkingId, string flight) data)
        {
            // [D] 1) Создаю машинку в гараже
            // [D] 2) Еду до ангара с самолетом кратчайшим путём, соблюдая пдд
            // [D] 3) Беру данные от самолета
            // [D] 4) Еду до взлетной полосы кратчайшим путем, соблюдая пдд, и передавая визуализатору также данные самолета
            // [D] 5) Открепляю самолет, пересылаю данные обратно Дане
            // [D] 6) Сообщаю АГХ, что довез
            // [D] 7) Еду в гараж

            var task = Task.Factory.StartNew(() => EscortToTakeoff_Task(data));
            //Task task = new Task(() => EscortToTakeoff_Task(data));
            //task.Start();
        }

        void EscortToQueue_Task(string flight)
        {
            const int MaxCapacity = 50000;
            int counterMaxCapacity = 0;
            int pos;

            if (cars.Count == MaxCapacity)   // TODO проверить работоспособность замещения старых машинок
            {
                cars.Insert(counterMaxCapacity, new FollowMe(flight));
                pos = counterMaxCapacity;
                counterMaxCapacity++;
                if (counterMaxCapacity == MaxCapacity) counterMaxCapacity = 0;
            }
            else
            {
                cars.Add(new FollowMe(flight));
                pos = cars.Count - 1;
            }

            Driving(cars[pos].GUID, "", cars[pos].node, 2, 0);
            cars[pos].node = 2;

            try
            {
                var tuple = RemoteRequest<Tuple<string, int, int>>("Plane", "TakePlane", flight);
                cars[pos].GrabPlane(tuple.Item1, tuple.Item2, tuple.Item3);
                Log($"Взят самолет с GUID {tuple.Item1}.Сервис Plane, TakePlane.");
            }
            catch
            {
                LogProblem($"Упал запрос к сервису Plane, TakePlane.");
            }

            Driving(cars[pos].GUID, cars[pos].pl_GUID, cars[pos].node, 38, cars[pos].pl_countPassengers);
            cars[pos].node = 38;           
        }

        void EscortToQueue(string flight)
        {
            // [D] 1) Создаю машинку в гараже
            // [D] 2) Еду до самолета(посадочной полосы) кратчайшим путём, соблюдая пдд
            // [D] 3) Беру данные от самолета
            // [D] 4) Еду до нода с очередью(38) кратчайшим путем, соблюдая пдд, и передавая визуализатору также данные самолета

            var task = Task.Factory.StartNew(() => EscortToQueue_Task(flight));
            //Task task = new Task(() => EscortToQueue_Task(flight));
            //task.Start();
        }

        void EscortFromQueue_Task((int parkingId, string flight) data)
        {
            int size = cars.Count;
            for (int i = 0; i < size; i++)
            {
                if (cars[i].pl_flight == data.flight)
                {
                    Driving(cars[i].GUID, cars[i].pl_GUID, cars[i].node, data.parkingId, cars[i].pl_countPassengers);
                    cars[i].node = data.parkingId;

                    RemoteCall("Plane", "LeavePlane", new Tuple<int, string>(data.parkingId, data.flight));
                    Log($"Откреплён самолет с GUID {cars[i].pl_GUID}.Сервис Plane, TakePlane.");
                    cars[i].DumpPlane();

                    RemoteCall("AGH", "PlaneParked", data.parkingId);
                    Log($"Сообщил в сервис AGH, PlaneParked номер парковки самолета.");

                    Driving(cars[i].GUID, "", cars[i].node, 19, 0);
                    cars[i].node = 19;

                    RemoteCall("GRAPHICS", "Kill", cars[i].GUID);
                    Log($"Запрос в сервис GRAPHICS, Kill на удаление {cars[i].GUID} с графа.");
                }
            }
        }

        void EscortFromQueue((int parkingId, string flight) data)
        {
            // [D] 1) Беру машинку, соответствующую рейсу(planeId) самолета
            // [D] 2) Еду до ангара
            // [D] 3) Отдаю в ангар самолет, пересылаю данные обратно Дане
            // [D] 4) Сообщаю АГХ, что довез
            // [D] 5) Еду в гараж

            var task = Task.Factory.StartNew(() => EscortFromQueue_Task(data));
            //Task task = new Task(() => EscortFromQueue_Task(data));
            //task.Start();
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Config.IsLocal = false;
            //Config.RequestTimeout = 10; //TODO убрать
            FollowMeService fm = new FollowMeService();
            fm.Start(true);
            fm.SetTimeMult();
            Console.WriteLine("Service FM is up!");
        }
    }
}
