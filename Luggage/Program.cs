﻿using System;
using System.Collections.Generic;
using System.Threading;
using Platform.Network;
using Platform;
using GraphPath;

namespace Luggage
{
    public class Luggage_serv : BasicService
    {
        public static Pathfinder pathfinder = new Pathfinder();
        readonly Graph graph = new Graph(pathfinder);
        HashSet<Luggage_car> cars = new HashSet<Luggage_car>();

        public Luggage_serv() : base("Luggage")
        {
            Bind<Tuple<int, string>>("UnloadLuggage", UnloadLuggage);
            Bind<Tuple<int, string>>("LoadLuggage", LoadLuggage);

            Log("Luggage service started");
        }

        public void UnloadLuggage(Tuple<int, string> info) //задание на разгрузку самолета, в кортеже int aircraft_coordinate,, string flight
        {
            Luggage_car car = new Luggage_car(this, info.Item1, info.Item2, 1);
            lock (cars)
            {
                cars.Add(car);
            }
            Log($"Creating luggage car {car.GUID}");
        }

        public void LoadLuggage(Tuple<int, string> info) //задание на загрузку самолета, в кортеже int aircraft_coordinate, string flight, int luggage
        {
            Luggage_car car = new Luggage_car(this, info.Item1, info.Item2, 2);
            lock (cars)
            {
                cars.Add(car);
            }
            Log($"Creating luggage car {car.GUID}");
        }

        public void Log(string message)
        {
            Console.WriteLine(message);
            RemoteCall("Log", "Write", message);
        }

        public void Run()
        {
            while (true)
            {
                List<Luggage_car> removedCars = new List<Luggage_car>();
                lock (cars)
                {
                    DateTime current_time = RemoteRequest<DateTime>("Time", "WhatTime");

                    foreach (Luggage_car car in cars)
                    {
                        if (car.type == 1) //если машинка в пути
                        {
                            car.UpdatePosition(current_time);
                        }
                        else if (car.type == 2) //если ждет разрешения двигаться дальше (находится на узле)
                        {
                            car.MovePermission(current_time); //запрос к Службе управления наземным движением на разрешение двигаться дальше
                        }
                        else if (car.type == 3) //если выполняет задание
                        {
                            if (car.job_status == 1) car.StartWork(current_time);
                            else if (car.job_status == 2) car.DoWork(current_time);
                            else car.CreatPathBack(); //создание обратного 
                        }
                        else if (car.type == 5) //если вернулись в гараж
                        {
                            removedCars.Add(car);
                        }
                    }

                    foreach (var car in removedCars)
                    {
                        RemoteCall("GRAPHICS", "Kill", (car.GUID));
                        cars.Remove(car);
                    }

                }
                Thread.Sleep(100);
            }
        }
    }

    class Luggage_car
    {
        Luggage_serv serv;

        float speed = 0.03f; //скорость: значение * единицы расстояния в секунду
        int luggage_weight = 0;

        static int noOfObjects = 0; //номер созданной машины
        public readonly string GUID;
        string flight; //рейс

        public int type_job; //тип задания: 1 - разгрузка самолета, 2 - загрузка самолета 
        public int type_pathpart = 1; //тип отрезка пути (соответсвенно для разгрузки/загрузки): 1 - самолет/накопитель, 2 - утилизатор/самолет, 3 - гараж
        public int type; //тип движения: 1 - в пути, 2 - жду возможности ехать, 3 - выполняю задание, 4 - на обратном пути 5 - вернулись в гараж
        public int job_status = 1; //состояние обработки задания: 1 - не начала, 2 - в процессе, 3 - завершено

        int location; //узел графа, на котором находится машина в настоящий момент
        int nearest_coordinate; //узел графа, следующий на пути
        readonly int aircraft_coordinate; //узел графа, на котором находится самолет
        readonly int hoarder_coordinate; //узел графа, на котором находится накопитель
        readonly int utilizer_coordinate; //узел графа, на котором находится утилизатор багажа
        List<int> path; //путь машинки - список узлов графа

        DateTime start_time; //время начала пути из конкретного узла
        DateTime end_time; //время расчетного конца пути (достижения следующего узла)
        DateTime end_work; //время конца работы

        public Luggage_car(Luggage_serv _serv, int _aircraft_coordinate, string _flight, int _type_job)
        {
            serv = _serv;

            noOfObjects++;
            GUID = "l_" + noOfObjects;
            flight = _flight;

            type_job = _type_job;
            type = 2;

            location = 19; //19 - узел гаража
            hoarder_coordinate = 45; //узел накопителя
            utilizer_coordinate = 46; //узел утилизатора
            aircraft_coordinate = _aircraft_coordinate;

            if (type_job == 1) path = Luggage_serv.pathfinder.GetRoad(location, this.aircraft_coordinate);
            else path = Luggage_serv.pathfinder.GetRoad(location, this.hoarder_coordinate);

            nearest_coordinate = path[0];
            SendPosition();
        }

        public void UpdatePosition(DateTime current_time)
        {
            if (current_time > end_time)
            {
                FreeRoad();
                MoveNext();
                SendPosition();
            }
            else
            {
                SendProgress(current_time); //если машинка в пути отправляем визуализатору прогресс
            }
        }

        public void MoveNext() //смена узла на следующий из списка пути
        {
            location = nearest_coordinate;
            if (path.Count <= 1)
            {
                //приехали
                if (job_status != 3)
                {
                    type = 3; //делаем работу
                }
                else
                {
                    type = 5; //удаляемся
                    serv.Log("Removing car " + this.GUID);
                }
            }
            else
            {
                type = 2;
                path.RemoveAt(0);
                nearest_coordinate = path[0];
                serv.Log($"Waiting for road from {location} to {nearest_coordinate}");
            }
        }

        public void SendProgress(DateTime current_time) //отправку визуализатору прогресса во время движения по ребру графа
        {
            float progress = (float)(current_time.Subtract(start_time).TotalSeconds / end_time.Subtract(start_time).TotalSeconds);
            serv.RemoteCall("GRAPHICS", "UpdatePosition", (this.GUID, this.location, this.nearest_coordinate, progress));
        }

        public void SendPosition() //отправка информации об остановке на узле визуализатору
        {
            serv.RemoteCall("GRAPHICS", "SetPosition", (this.GUID, this.location));
        }

        public void FreeRoad() //освобождение дороги
        {
            serv.RemoteCall("ControlTransport", "FreeRoad", GUID);
            serv.Log("Freeing owned road");
        }

        public void MovePermission(DateTime currentTime) //ожидание разрешения на движение от Службы управления наземным движением
        {
            if (GroundMovementControl(true))
            {
                type = 1;
                start_time = currentTime;
                end_time = start_time.AddSeconds(Luggage_serv.pathfinder.GetRoadLength(location, nearest_coordinate) / speed);
                serv.Log($"Start moving form {location} to {nearest_coordinate}");
            }
        }

        private bool GroundMovementControl(bool type) //взаимодействие со Службой управления наземного движения
        {
            bool resolution; //ответ Службы управления наземного движения
            resolution = serv.RemoteRequest<bool>("ControlTransport", "SetRoad", (this.GUID, type, this.location, this.nearest_coordinate));
            return resolution;
        }

        public void CreatPathBack() //создание пути
        {
            if (type_job == 1)
            {
                if (type_pathpart == 2) //самолет-утилизатор --> утилизатор-гараж
                {
                    job_status = 3;
                    type_pathpart = 3;
                    path = Luggage_serv.pathfinder.GetRoad(location, 19);
                    nearest_coordinate = path[0];
                }
                if (type_pathpart == 1) //гараж-самолет --> самолет-утилизатор
                {
                    job_status = 2;
                    type_pathpart = 2;
                    path = Luggage_serv.pathfinder.GetRoad(location, 46);
                    nearest_coordinate = path[0];
                }
                type = 2;
            }
            if (type_job == 2)
            {
                if (type_pathpart == 2) //накопитель-самолет --> самолет-гараж
                {
                    job_status = 3;
                    type_pathpart = 3;
                    path = Luggage_serv.pathfinder.GetRoad(location, 19);
                    nearest_coordinate = path[0];
                }
                if (type_pathpart == 1) //гараж-накопитель --> накопитель-самолет
                {
                    job_status = 2;
                    type_pathpart = 2;
                    path = Luggage_serv.pathfinder.GetRoad(location, this.aircraft_coordinate);
                    nearest_coordinate = path[0];
                }
                type = 2;
            }
        }

        public void StartWork(DateTime current_time) //задача времени 
        {
            end_work = current_time.AddSeconds(360);
            job_status = 2;
            serv.Log($"Start doing work for {this.GUID}");
        }

        public void DoWork(DateTime current_time) //выполнение работы
        {
            if (type_job == 1) //разгружаем самолет
            {
                if (type_pathpart == 1) //забрать багаж с рейса
                {
                    job_status = 3;
                    this.luggage_weight = serv.RemoteRequest<int>("Plane", "SendLuggage", this.flight);
                    serv.RemoteCall("AGH", "LuggageUnloaded", this.aircraft_coordinate);
                    serv.Log($"Luggage unloaded from plane flight {this.flight} for {this.GUID}");
                }

                if (type_pathpart == 2) //загрзить багаж в накопитель
                {
                    job_status = 3;
                    serv.RemoteCall("Waitingroom", "LoadLugg", luggage_weight);
                    this.luggage_weight = 0;
                    serv.Log($"Luggage from plane flight {this.flight} in waitingroom for {this.GUID}");
                }

            }
            if (type_job == 2) //загружаем самолет
            {
                if (type_pathpart == 1) //забрать багаж из накопителя
                {
                    job_status = 3;
                    this.luggage_weight = serv.RemoteRequest<int>("Waitingroom", "SendLugg", flight);
                    serv.Log($"Luggage unloaded from waitingroom flight {this.flight} for {this.GUID}");
                }
                if (type_pathpart == 2) //загрузить багаж в самолет
                {
                    job_status = 3;
                    serv.RemoteCall("Plane", "LoadLugg", (flight, luggage_weight));
                    this.luggage_weight = 0;
                    serv.RemoteCall("AGH", "LuggageLoaded", this.aircraft_coordinate);
                    serv.Log($"Luggage loaded in plane flight {this.flight} for {this.GUID}");
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Config.IsLocal = false;
            Config.RequestTimeout = 0;
            Luggage_serv d = new Luggage_serv();
            d.Start(true);
            d.Run();
        }
    }
}
