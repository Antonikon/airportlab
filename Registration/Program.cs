﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Network;

namespace Registration
{
    public class Person //пассажир
    {
        public string name; //Name and Surname "John Brown"
        public string guidPerson; //generated GUID for person
        public string flight; //number of flight
        public int luggage; //laguage mass
        public int state; //1 - no boarding pass, 2 - has a boarding pass, 3 - chill zone, 4 - in the buss, 5 - on plane
        public int food; //1 - normal, 2 - vegan, 3 - McDonalds

        public Person()
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            name = GenerateName(rnd.Next(3, 5)) + " " + GenerateName(rnd.Next(5, 7));
            flight = "";
            guidPerson = "passenger_" + Guid.NewGuid().ToString();
            luggage = rnd.Next(1, 35);
            state = 1;
            food = rnd.Next(1, 4);
        }

        string GenerateName(int len)
        {
            Random r = new Random((int)DateTime.Now.Ticks);
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "e", "y" };
            string Name = "";
            Name += consonants[r.Next(consonants.Length)].ToUpper();
            Name += vowels[r.Next(vowels.Length)];
            int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                Name += consonants[r.Next(consonants.Length)];
                b++;
                Name += vowels[r.Next(vowels.Length)];
                b++;
            }
            return Name;
        }
    }

    class Program
    {
            //в cashier_list содержатся копии списков с людьми с билетами, нужен для проверки приходящих
            //словарь содержит рейсы ещё до начала регистрации на них, удаляется сразу после конца регистрации
        public static Dictionary<string, List<Person>> cashier_list = new Dictionary<string, List<Person>>(); 
            //в flight_lists_on_registration попадают только пассажиры, прошедшие регистрацию и готовые по её окончанию отправиться в накопитель
            //в словаре содержатся только те рейсы, на которые сейчас (в данный момент) есть регистрация
        public static Dictionary<string, List<Person>> flight_lists_on_registration = new Dictionary<string, List<Person>>();
            //словарь со списком еды для каждого рейса
        public static Dictionary<string, (int, int, int)> food_list = new Dictionary<string, (int, int, int)>();

        static void Main(string[] args)
        {
            Config.IsLocal = false;
            RegistrationService service = new RegistrationService();
            service.Start(true);

            while (true) { }
        }

        public class RegistrationService : BasicService
        {
            public RegistrationService() : base("Registration")
            {
                //Bind<Person>("ReceivePerson", Receive);
                //Bind<Tuple<string, int>>("StartSellingTickets", SellsStart);
                //Bind<string>("StopSellingTickets", SellsStop);

                Bind<Person>("PassengerRegistration", PassengerProcessing);
                Bind<string>("StartRegistration", RegistrationStart);
                Bind<string>("StopRegistration", RegistrationStop);
                Bind<Tuple<string, Person>>("GetFlightList", GetCopyList);
            }

            public void GetCopyList(Tuple<string, Person> copy_list)
            {
                string log_message = "";
                //cashier_list.Add(copy_list.Item1, copy_list.Item2);
                if (cashier_list.ContainsKey(copy_list.Item1))
                {
                    cashier_list[copy_list.Item1].Add(copy_list.Item2);
                    log_message = copy_list.Item2.guidPerson + " has been added to already existing flight in list " + copy_list.Item1;
                    //Log(log_message);
                    Console.WriteLine("{0} has been added to existing flight_list in list {1}", copy_list.Item2.guidPerson, copy_list.Item1);
                }
                else
                {
                    cashier_list.Add(copy_list.Item1, new List<Person>());
                    cashier_list[copy_list.Item1].Add(copy_list.Item2);
                    log_message = copy_list.Item2.guidPerson + " has been added to a new flight in list " + copy_list.Item1;
                    //Log(log_message);
                    Console.WriteLine("{0} has been added to a new flight_list in list {1}", copy_list.Item2.guidPerson, copy_list.Item1);
                }
                Console.WriteLine("The list from cashier has been get");
            }

            public void PassengerProcessing(Person p) //проверка людей, приходящих на регистрацию
            {
                Console.WriteLine("------------------\nDanya's {0} has flight -{1}-\n------------------", p.guidPerson, p.flight);
                if (flight_lists_on_registration.Count == 0)
                {
                    RemoteCall("Passenger", "PassengerReceive", p);
                    Console.WriteLine("\nNo registrations at all, {0} is sent back\n", p.guidPerson);
                }
                else
                {
                    string log_message = "";
                    if (flight_lists_on_registration.ContainsKey(p.flight))
                    {
                        bool check = false;

                        //проверка на наличие записи об этом пассажире в списке от кассы
                        foreach (Person person in cashier_list[p.flight])
                        {
                            //если id и билеты у человека совпадают с таковыми в списке кассы, то пропускаем его дальше на обработку, иначе выгоняем
                            if ((p.guidPerson == person.guidPerson) && (p.flight == person.flight))
                            {
                                Console.WriteLine("----------------------------\np.flight == person.flight\n---------------------------");
                                Console.WriteLine("----------------------------\n{0} == {1}\n---------------------------", p.guidPerson, person.guidPerson);
                                Console.WriteLine("----------------------------\n{0} == {1}\n---------------------------", p.flight, person.flight);
                                check = true;
                                break;
                            }
                        }

                        if ((p.state == 2) && (check == true))
                        {
                            if (p.luggage > 30) p.luggage = 30;
                            p.state = 3;
                            flight_lists_on_registration[p.flight].Add(p);

                            //придача кортежу еды нового значения, в зависимости от желания пассажира p.food
                            int k = p.food;
                            switch (k)
                            {
                                case 1:
                                    food_list[p.flight] = (food_list[p.flight].Item1 + 1, food_list[p.flight].Item2, food_list[p.flight].Item3);
                                    Console.WriteLine("The food type 1 has been added to food_list of flight {0}", p.flight);
                                    break;
                                case 2:
                                    food_list[p.flight] = (food_list[p.flight].Item1, food_list[p.flight].Item2 + 1, food_list[p.flight].Item3);
                                    Console.WriteLine("The food type 2 has been added to food_list of flight {0}", p.flight);
                                    break;
                                case 3:
                                    food_list[p.flight] = (food_list[p.flight].Item1, food_list[p.flight].Item2, food_list[p.flight].Item3 + 1);
                                    Console.WriteLine("The food type 3 has been added to food_list of flight {0}", p.flight);
                                    break;
                                default:
                                    log_message = "Passenger " + p.guidPerson + " has unknown food state " + p.food + " and won't get any";
                                    LogProblem(log_message);
                                    break;
                            }
                            RemoteCall("Passenger", "PassengerReceive", p);
                            log_message = "The passenger" + p.guidPerson + " has been added to flight " + p.flight;
                            //Log(log_message);
                            Console.WriteLine("The passenger {0} has been added to flight {1} and sent to Danya\n----------------------\n\n", p.guidPerson, p.flight);
                        }
                        else
                        {
                            RemoteCall("Passenger", "PassengerReceive", p);
                            log_message = "The passenger " + p.guidPerson + " has been denied in registration because he/she doesn't have a ticket";
                            //Log(log_message);
                            Console.WriteLine("The passenger {0} has been denied in registration because he/she doesn't have a ticket on {1}", p.guidPerson, p.flight);
                        }
                    }
                    else
                    {
                        RemoteCall("Passenger", "PassengerReceive", p);
                        log_message = "The passenger " + p.guidPerson + " has been denied in registration because he/she came when there was no registration";
                        //Log(log_message);
                        Console.WriteLine("The passenger {0} has been denied in registration because he/she came when there was no registration on {1}", p.guidPerson, p.flight);
                    }
                }
            }

            public void RegistrationStart(string flight_number) //старт регистрации на рейс, добавление нового элемента в словарь
            {
                    //уведомить Даню о начале регистрации
                RemoteCall("Passenger", "RegStart", flight_number);
                flight_lists_on_registration.Add(flight_number, new List<Person>());
                food_list.Add(flight_number, (0, 0, 0));
                Console.WriteLine("\nThe registration has been started for {0}\n", flight_number);
            }

            public void RegistrationStop(string flight_number) //окончание регистрации, отправка списка пассажиров рейса накопителю, отправка наземной службе списка еды
            {
                bool luggage = false;
                foreach (var mass in flight_lists_on_registration[flight_number])
                {
                    if (mass.luggage > 0)
                    {
                        luggage = true;
                        break;
                    }
                }

                    //пересылание БибеБобе кортеж с хавчиком
                RemoteCall("AGH", "RegistrationAndFood", (flight_number, food_list[flight_number].Item1, food_list[flight_number].Item2, food_list[flight_number].Item3, flight_lists_on_registration[flight_number].Count(), luggage));
                Console.WriteLine("\n\n\n!!!!!!!!!!\nSanya got {0} passengers", flight_lists_on_registration[flight_number].Count());
                Console.WriteLine("\nThe registration has been stopped for {0}", flight_number);
                Console.WriteLine("Food for {0} is sent to BIBA\n", flight_number);
                flight_lists_on_registration.Remove(flight_number);
                food_list.Remove(flight_number);
                cashier_list.Remove(flight_number);
            }

            public void Log(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "Write", message);
            }

            public void LogProblem(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "AddLog", new Tuple<int, string>(1, message));
            }
        }
    }
}
