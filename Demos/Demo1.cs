﻿using System;
using System.Threading;
using Platform.Network;

//В этом примере показывается как создавать сервисы и в 
//чём разница между многопоточным и однопоточным сервисом

namespace Demos {
    //Наследуемся от базового сервиса
    public class NumberService : BasicService {
        public NumberService(bool isAsynk) :
            base("test_number", async: isAsynk)
            //Указываем имя сервиса, по которому к нему будут обращаться
        {
            //Указываем что функцию GetNumber можно вызвать снаружи под именем get
            //Указываем что функция принимает и возвращает int
            Bind<int, int>("get", GetNumber);

        }

        public int GetNumber(int value) {
            Console.WriteLine();
            Console.WriteLine($"Обрабатываем запрос с аргументом {value} ...");

            Thread.Sleep(2000);

            Console.WriteLine($"Запрос с аргументом {value} обработан");
            return value;
        }
    }

    //Наследуемся от базового сервиса
    public class ServiceA: BasicService {
        public ServiceA():
            base("test_service")
            //Указываем имя сервиса, по которому к нему будут обращаться
        {}

        public void Run() {
            Console.WriteLine("Делаем запросы с ожиданием ответа");
            Thread.Sleep(2000);

            for (int i = 0; i < 3; i++) {
                //Функция RemoteRequest ожидает, пока не будет получен ответ
                //Вызываем функцию get у сервиса test_number c аргументом i
                var number = RemoteRequest<int>("test_number", "get", i);
                Console.WriteLine($"Получен ответ: {number}");
            }

            Console.WriteLine();
            Console.WriteLine("Делаем вызовы без ожидания результата");
            Thread.Sleep(3000);

            for (int i = 0; i < 3; i++) {
                Console.WriteLine($"Запрос с аргументом {i} отправлен");
                //Функция RemoteCall не ожидает ответа
                //Вызываем функцию get у сервиса test_number c аргументом i
                RemoteCall("test_number", "get", i);
            }

            Thread.Sleep(6500);
        }
    }

    public static class Demo1 {
        public static void Run() {
            //Раскомментировать чтобы использовать удалённый сервер для обмена сообщениями
            //При первом подключении будет запрошен пароль
            Config.IsLocal = false;
            {
                Console.WriteLine("Запускаем сервис в однопоточном режиме:");

                var a = new ServiceA();
                var b = new NumberService(false);

                //Запускаем потоки обработки запросов у каждого сервиса
                a.Start(true);
                b.Start(true);

                a.Run();

                Console.WriteLine("Нажмите любую кнопку для продолжения");
                Console.ReadLine();
                a.Stop();
                b.Stop();
            }
            {
                Console.Clear();
                Console.WriteLine("Запускаем сервис в многопоточном режиме:");

                var a = new ServiceA();
                var b = new NumberService(true);

                //Запускаем потоки обработки запросов у каждого сервиса
                a.Start(true);
                b.Start(true);

                a.Run();
                Console.WriteLine("Демонстрация завершена");
                Console.ReadLine();
            }
        }
    }
}