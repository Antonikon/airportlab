﻿using System;
using System.Threading;

using Platform.Network;
//Подключаем модули и расширения
using Platform.Network.Modules;

namespace Demos {
    public class Timer : BasicService {
        private int Counter = 0;

        public Timer() :
            base("test_timer") {
            //Загружаем модуль событий, который добавляет набор публичных функций
            AddModule<DefaultEventSystem>();

            Bind("get_time", GetTime);
        }

        public int GetTime() {
            return Counter;
        }

        public void Count() {
            while (true) {
                Counter++;
                //Сообщаем подписчикам о событии
                GetModule<DefaultEventSystem>().EmitEvent("tick");
                Thread.Sleep(2000);
            }
        }
    }

    public class Subscriber : BasicService {
        public Subscriber() :
            base("test_subscriber") {
            //Делаем метод доступным для внешнего вызова, что другой сервис смог его 
            //вызвать при наступлении события
            Bind<string>("event", OnEvent);

            //С помощью метода-расширения Subscribe подписываемся к сервису test_timer
            //на событие tick. Сообщаем, что при наступлении события необходимо
            //вызвать наш метод event с аргументом "сервис-счётчик"
            //Устанавливаем флаг false чтобы оставаться подписанным на событие до отписки
            this.Subscribe("test_timer", "tick", "event", "сервис-счётчик", false);
        }

        void OnEvent(string from) {
            int time = RemoteRequest<int>("test_timer", "get_time");
            Console.WriteLine($"Получено уведомление: {time} от {from}");

            //После наступления времени 5 отписываемся от уведомлений
            if (time == 5) this.Unsubscribe("test_timer");
        }
    }

    public static class Demo2 {
        public static void Run() {
            Config.RequestTimeout = 0;
            var t = new Timer();
            t.Start(true);

            var sub = new Subscriber();
            sub.Start(true);

            t.Count();
        }

    }
}