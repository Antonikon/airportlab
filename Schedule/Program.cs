﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Platform.Network;

namespace Schedule
{
    public class Flight
    {
        public DateTime DateFly;
        public DateTime DateArr;
        public string City;
        public string Time;
        public int Type;
        public int Capacity;
        public int StatusFly;
        public int StatusArr;

        public Flight(DateTime now, int min = 10080, int max = 86400)
        {
            Random rnd = new Random();
            int rndTime = rnd.Next(min, max);
            DateFly = now.AddMinutes(rndTime);
            DateArr = DateFly.AddMinutes(-rnd.Next(60, 120));
            string Race = GetCity();
            City = Race.Substring(0, Race.Length - 6);
            Time = Race.Substring(Race.Length - 5);
            Type = rnd.Next(9) == 0 ? 2 : 1;
            Capacity = rnd.Next(6, 12) * 5;
            if (Type == 1)
            {
                if (rndTime < 1440)
                {
                    StatusFly = 3;
                }
                else if (rndTime < 10080)
                {
                    StatusFly = 2;
                }
                else
                {
                    StatusFly = 1;
                }
            }
            else
            {
                StatusFly = 1;
            }
            StatusArr = 7;
        }

        public string GetCity()
        {
            string[] Sities = {"Ханты-Мансийск 03:10",
            "Уфа 02:05",
            "Самара 01:40",
            "Горно-Алтайск 04:15",
            "Тюмень 02:45",
            "Махачкала 03:00",
            "Сочи 02:10",
            "Краснодар 02:05",
            "Ставрополь 02:05",
            "Санкт-Петербург 01:25",
            "Курск 01:10",
            "Ростов на Дону 01:50",
            "Калининград 02:00",
            "Усинск 02:30",
            "Кельн 03:25",
            "Самарканд 03:50",
            "Анталья 03:50",
            "Рига 01:40",
            "Вена 02:55",
            "Будапешт 02:15" };
            Random rnd = new Random((int)DateTime.Now.Ticks);
            return Sities[rnd.Next(Sities.Length)];
        }
    }

    public class TimeStatus
    {
        public DateTime Time;
        public int NextStatus;
        public int Type;

        public TimeStatus(DateTime time, int next, int type)
        {
            Time = time;
            NextStatus = next;
            Type = type;
        }
    }

    public class Program
    {
        public static Dictionary<string, Flight> Schedule = new Dictionary<string, Flight>();
        public static Dictionary<string, TimeStatus> StatusesFly = new Dictionary<string, TimeStatus>(); //1- Долго до вылета. 2 - продажа билетов. 3 - регистрация. 4 - посадка. 5 - посадка окончена. 6 - вылет. 7 - долго до прилета. 8 - прилет.  
        public static Dictionary<string, TimeStatus> StatusesArr = new Dictionary<string, TimeStatus>(); //7 - долго до прилета. 8 - прилет.  
        public static List<string> Names = new List<string>();
        public static void Main(string[] args)
        {
            Config.IsLocal = false;
            ScheduleService service = new ScheduleService();
            service.Start(true);

            service.CreateSchedule();
        }

        public class ScheduleService : BasicService
        {

            public ScheduleService() : base("Schedule")
            {
                Bind<DateTime>("TimeChange", ItsTime);
                Bind<string>("PlaneOut", Fly);
                Bind<List<(DateTime, string, string, string, int)>>("GetBoard", GetBoard);
            }

            public void CreateSchedule(int count = 10, int min = 180, int max = 2880) 
            {
                string message = "Расписание из ";
                while (Schedule.Count <= count)
                {
                    CreateFlights(min, max);
                }
                message += (Schedule.Count * 2).ToString() + " рейсов составлено.";
                Log(message);
            }

            public void CreateFlights(int min, int max) 
            {
                DateTime Now = GetTime();
                Flight flight = new Flight(Now, min, max);
                string Name = GetName();
                Schedule.Add(Name, flight);
                if (flight.Type == 1)
                {
                    DateTime DepNextTime;
                    if (flight.StatusFly == 1)
                    {
                        DepNextTime = flight.DateFly.AddDays(-7);
                        StatusesFly.Add(Name, new TimeStatus(DepNextTime, 2, flight.Type));
                        SubTime(DepNextTime);
                    }
                    else if (flight.StatusFly == 2)
                    {
                        DepNextTime = flight.DateFly.AddDays(-1);
                        StatusesFly.Add(Name, new TimeStatus(DepNextTime, 3, flight.Type));
                        StartSale(Name);
                    }
                    else
                    {
                        DepNextTime = flight.DateFly.AddMinutes(-30);
                        StatusesFly.Add(Name, new TimeStatus(DepNextTime, flight.StatusFly + 1, flight.Type));
                        StartSale(Name);
                        StartCheckin(Name);
                    }
                }
                else
                {
                    StatusesFly.Add(Name, new TimeStatus(flight.DateFly, 6, flight.Type));
                    SubTime(flight.DateFly);
                }
                StatusesArr.Add(Name, new TimeStatus(flight.DateArr, 8, flight.Type));
                SubTime(flight.DateArr);
            }

            public string GetName()
            {
                Thread.Sleep(25);
                string Res;
                Random rnd = new Random();
                do
                {
                    Res = "";
                    Res += (char)rnd.Next(0x0041, 0x005A);
                    Res += (char)rnd.Next(0x0041, 0x005A);
                    Res += rnd.Next(1000, 9999).ToString();
                } while (Names.Contains(Res));
                return Res;
            }

            public DateTime GetTime() //Запросить время
            {
                return RemoteRequest<DateTime>("Time", "WhatTime"); 
            }

            public void SubTime(DateTime time) //Сказать времени сообщить мне, когда будет time
            {
                RemoteCall("Time", "TimeSubscribe", ("Schedule", "TimeChange", time)); 
            }

            public void ItsTime(DateTime time)
            {
                List<string> delArr = new List<string>();
                List<string> LStartSale = new List<string>();
                List<string> LStopSale = new List<string>();
                List<string> LStartCheckin = new List<string>();
                List<string> LStopCheckin = new List<string>();
                List<string> LStartLanding = new List<string>();
                List<string> LStopLanding = new List<string>();
                List<string> LFly = new List<string>();
                List<string> LArrival = new List<string>();
                foreach (KeyValuePair<string, TimeStatus> item in StatusesFly)
                {
                    if (DateTime.Compare(time, item.Value.Time) >= 0)
                    {
                        switch (item.Value.NextStatus)
                        {
                            case 2:
                                LStartSale.Add(item.Key);
                                break;
                            case 3:
                                LStartCheckin.Add(item.Key);
                                break;
                            case 4:
                                LStopSale.Add(item.Key);
                                LStartLanding.Add(item.Key);
                                break;
                            case 5:
                                LStopCheckin.Add(item.Key);
                                LStopLanding.Add(item.Key);
                                break;
                            case 6:
                                break;
                        }
                    }
                }
                foreach (KeyValuePair<string, TimeStatus> item in StatusesArr)
                {
                    if (DateTime.Compare(time, item.Value.Time) >= 0)
                    {
                        if (item.Value.NextStatus == 8)
                        {
                            LArrival.Add(item.Key);
                            delArr.Add(item.Key);
                        }
                    }
                }
                LStartSale.ForEach(StartSale);
                LStopSale.ForEach(StopSale);
                LStartCheckin.ForEach(StartCheckin);
                LStopCheckin.ForEach(StopCheckin);
                LStartLanding.ForEach(StartLanding);
                LStopLanding.ForEach(StopLanding);
                for (int i = 0; i < delArr.Count; i++)
                    StatusesArr.Remove(delArr[i]);
                LArrival.ForEach(Arrival);
            }

            public List<(DateTime, string, string, string, int)> GetBoard()
            {
                List<(DateTime, string, string, string, int)> Board = new List<(DateTime, string, string, string, int)>();
                DateTime Time = GetTime();
                foreach (KeyValuePair<string, Flight> flight in Schedule)
                {
                    // Наполняем табло
                    if (flight.Value.Type == 1 && flight.Value.StatusFly > 2)
                    {
                        Board.Add((flight.Value.DateArr, flight.Key, flight.Value.City, flight.Value.Time, flight.Value.StatusArr));
                        Board.Add((flight.Value.DateFly, flight.Key, flight.Value.City, flight.Value.Time, flight.Value.StatusFly));
                    }
                }
                Board.Sort((x, y) => DateTime.Compare(x.Item1, y.Item1));
                if (Board.Count > 30)
                    Board.RemoveRange(29, Board.Count - 30);
                return Board;
            }

            public void StartSale(string name)
            {
                Flight flight;
                TimeStatus status;
                if (Schedule.TryGetValue(name, out flight))
                {
                    string message = "Началась продажа билетов на рейс " + name;
                    //Передать данные о начале продаж.
                    RemoteCall("Cashier", "StartSellingTickets", (name, flight.Capacity));
                    RemoteCall("Board", "ChangeStatus", (name, 2));
                    flight.StatusFly = 2;
                    if (StatusesFly.TryGetValue(name, out status))
                    {
                        status.NextStatus = 3;
                        status.Time = flight.DateFly.AddDays(-1);
                        SubTime(flight.DateFly.AddDays(-1));
                    }
                    Log(message);
                }

            }

            public void StopSale(string name)
            {
                Flight flight;
                if (Schedule.TryGetValue(name, out flight))
                {
                    //Передать данные об окончании продаж.
                    RemoteCall("Cashier", "StopSellingTickets", name);
                    string message = "Закончилась продажа билетов на рейс " + name;
                    Log(message);
                }
            }

            public void StartCheckin(string name)
            {
                Flight flight;
                TimeStatus status;
                if (Schedule.TryGetValue(name, out flight))
                {
                    string message = "Началась регистрация на рейс " + name;
                    //Передать данные о начале регистрации.
                    RemoteCall("Registration", "StartRegistration", name);
                    RemoteCall("Board", "ChangeStatus", (name, 3));
                    flight.StatusFly = 3;
                    if (StatusesFly.TryGetValue(name, out status))
                    {
                        status.NextStatus = 4;
                        status.Time = flight.DateFly.AddMinutes(-30);
                        SubTime(flight.DateFly.AddMinutes(-30));
                    }
                    Log(message);
                }
            }

            public void StopCheckin(string name)
            {
                Flight flight;
                if (Schedule.TryGetValue(name, out flight))
                {
                    //Передать данные о конце регистрации.
                    RemoteCall("Registration", "StopRegistration", name);
                    string message = "Закончилась регистрация на рейс " + name;
                    Log(message);
                }
            }

            public void StartLanding(string name)
            {
                Flight flight;
                TimeStatus status;
                if (Schedule.TryGetValue(name, out flight))
                {
                    //Передать данные о начале посадки.
                    RemoteCall("Board", "ChangeStatus", (name, 4));
                    flight.StatusFly = 4;
                    if (StatusesFly.TryGetValue(name, out status))
                    {
                        status.NextStatus = 5;
                        status.Time = flight.DateFly.AddMinutes(-5);
                        SubTime(flight.DateFly.AddMinutes(-5));
                    }
                    string message = "Началась посадка на рейс " + name;
                    Log(message);
                }
            }

            public void StopLanding(string name)
            {
                Flight flight;
                TimeStatus status;
                if (Schedule.TryGetValue(name, out flight))
                {
                    //Передать данные о конце посадки.
                    RemoteCall("Board", "ChangeStatus", (name, 5));
                    flight.StatusFly = 5;
                    if (StatusesFly.TryGetValue(name, out status))
                    {
                        status.NextStatus = 6;
                        status.Time = flight.DateFly;
                    }
                    string message = "Закончилась посадка на рейс " + name;
                    Log(message);
                }
            }

            public void Fly(string name)
            {
                string message = "";
                string ProblemMessage = "";
                if(!Schedule.Remove(name))
                {
                    ProblemMessage += "Рейс " + name + " не может быть найден и удален.";
                } else if (!StatusesFly.Remove(name))
                {
                    ProblemMessage += "Статус рейса " + name + " не может быть найден и удален.";
                } else if(Names.Remove(name))
                {
                    ProblemMessage += "Имя " + name + " не может быть найдено и удалено.";
                } else
                {
                    message += "Рейс " + name + " вылетел.";

                }
                if (Schedule.Count < 70)
                {
                    int i;
                    for (i = 0; i < 10; i++)
                    {
                        CreateFlights(2880, 10080);
                    }
                    message += Environment.NewLine + "Создано " + (i*2) + " рейсов.";
                }
                RemoteCall("Board", "ChangeStatus", (name, 6));
                if (ProblemMessage != "")
                    LogPoblem(ProblemMessage);
                if (message != "")
                    Log(message);
            }

            public void Arrival(string name)
            {
                Flight flight;
                if (Schedule.TryGetValue(name, out flight))
                {
                    //Сообщить о прилете самолета. 
                    RemoteCall("Board", "ChangeStatus", (name, 8));
                    Thread.Sleep(1);
                    RemoteCall("Plane", "LandPlane", (flight.Capacity, flight.Type, name));
                    Thread.Sleep(1);
                    string message = "Рейс " + name + " прилетел.";
                    Log(message);
                }
            }

            public void Log(string mess)
            {
                Console.WriteLine(mess);
                //RemoteCall("Log", "Write", mess);
            }

            public void LogPoblem(string message)
            {
                Console.WriteLine("Проблема: " + message);
                //RemoteCall("Log", "AddLog", new Tuple<int, string>(1, message));
            }
        }
    }
}