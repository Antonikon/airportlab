﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

using Platform;

namespace UnitTests {
    [TestFixture()]
    public class PathfinderTest {

        [Test()]
        public void SimpleCase() {
            var pf = new Pathfinder();
            pf.AddRoad(1, 2, 1);
            pf.AddRoad(2, 1, 1);
            pf.Process();


            Assert.IsTrue(pf.GetRoad(1, 2).SequenceEqual(new List<int> { 2 }));
            Assert.IsTrue(pf.GetRoad(2, 1).SequenceEqual(new List<int> { 1 }));
            Assert.IsNull(pf.GetRoad(1, 3));
            Assert.IsTrue(pf.GetRoadLength(1, 2) == 1);
            Assert.IsTrue(pf.GetRoadLength(2, 1) == 1);
        }

        [Test()]
        public void DifferentNumbers() {
            var pf = new Pathfinder();
            pf.AddRoad(10, 100, 32);
            pf.AddRoad(100, 10, 3245);
            pf.Process();

            Assert.IsTrue(pf.GetRoad(10, 100).SequenceEqual(new List<int> { 100 }));
            Assert.IsTrue(pf.GetRoad(100, 10).SequenceEqual(new List<int> { 10 }));
            Assert.IsNull(pf.GetRoad(0, 1));
        }

        [Test()]
        public void LineCase() {
            var pf = new Pathfinder();
            for (int i = 0; i < 10; i++) {
                pf.AddRoad(i, i + 1, 1);
            }
            pf.Process();

            Assert.IsTrue(pf.GetRoad(0, 10).SequenceEqual(new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }));
            Assert.IsTrue(pf.GetRoad(5, 10).SequenceEqual(new List<int> {6, 7, 8, 9, 10 }));
            Assert.IsTrue(pf.GetWayLength(5, 6) == 1);
            Assert.IsTrue(pf.GetWayLength(0, 10) == 10);
        }

        [Test()]
        public void RectCase() {
            //         1
            //    0 - - - - 1
            //    | \20 15/ |
            //  4 |    4    |  2
            //    | /5  10\ |
            //    3 - - - - 2
            //         3
            var pf = new Pathfinder();
            pf.AddRoad(0, 1, 1);
            pf.AddRoad(1, 0, 1);
            pf.AddRoad(1, 2, 2);
            pf.AddRoad(2, 1, 2);
            pf.AddRoad(2, 3, 3);
            pf.AddRoad(3, 2, 3);
            pf.AddRoad(3, 0, 4);
            pf.AddRoad(0, 3, 4);

            pf.AddRoad(0, 4, 20);
            pf.AddRoad(4, 0, 20);
            pf.AddRoad(1, 4, 15);
            pf.AddRoad(4, 1, 15);
            pf.AddRoad(2, 4, 10);
            pf.AddRoad(4, 2, 10);
            pf.AddRoad(3, 4, 5);
            pf.AddRoad(4, 3, 5);

            pf.Process();

            // 0 -> 4: 3, 4
            // 0 -> 2: 1, 2
            // 2 -> 0: 1, 0
            // 3 -> 0: 0
            // 3 -> 4: 4
            // 0 -> 3: 3
            // 2 -> 4: 3, 4

            Assert.IsTrue(pf.GetRoad(0, 4).SequenceEqual(new List<int> {3, 4}));
            Assert.IsTrue(pf.GetRoad(0, 2).SequenceEqual(new List<int> { 1, 2 }));
            Assert.IsTrue(pf.GetRoad(2, 0).SequenceEqual(new List<int> { 1, 0 }));
            Assert.IsTrue(pf.GetRoad(3, 0).SequenceEqual(new List<int> { 0}));
            Assert.IsTrue(pf.GetRoad(3, 4).SequenceEqual(new List<int> { 4 }));
            Assert.IsTrue(pf.GetRoad(0, 3).SequenceEqual(new List<int> { 3 }));
            Assert.IsTrue(pf.GetRoad(2, 4).SequenceEqual(new List<int> { 3, 4 }));

            Assert.IsTrue(pf.GetWayLength(0, 4) == 9);
            Assert.IsTrue(pf.GetWayLength(0, 2) == 3);
            Assert.IsTrue(pf.GetWayLength(2, 0) == 3);
            Assert.IsTrue(pf.GetWayLength(3, 0) == 4);
            Assert.IsTrue(pf.GetWayLength(3, 4) == 5);
            Assert.IsTrue(pf.GetWayLength(0, 3) == 4);
            Assert.IsTrue(pf.GetWayLength(2, 4) == 8);
            Assert.IsTrue(pf.GetWayLength(3, 4) == 5);
        }

        [Test()]
        public void UnequalLength() {
            var pf = new Pathfinder();
            pf.AddRoad(1, 2, 1);
            pf.AddRoad(2, 3, 1);
            pf.AddRoad(3, 1, 1);
            pf.AddRoad(2, 1, 10);
            pf.AddRoad(3, 2, 10);
            pf.AddRoad(1, 3, 10);
            pf.Process();

            Assert.IsTrue(pf.GetRoad(1, 3).SequenceEqual(new List<int> { 2, 3 }));
            Assert.IsTrue(pf.GetRoad(2, 1).SequenceEqual(new List<int> { 3, 1 }));
            Assert.IsTrue(pf.GetRoad(3, 2).SequenceEqual(new List<int> { 1, 2 }));
        }
    }
}
