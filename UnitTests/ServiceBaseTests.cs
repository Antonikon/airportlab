﻿using NUnit.Framework;
using Platform.Network;
using Newtonsoft.Json.Linq;
using System;

namespace UnitTests {
    internal struct StructArg {
        public int A;
        public string B;
    }

    internal class TestService: BasicService {
        private int Member = 0;

        public TestService():
            base("test_service") 
        {
            Bind("empty", EmptyMethod);
            Bind<string>("simple_arg", SimpleArgMethod);
            Bind<int>("simple_return", SimpleReturnMethod);
            Bind<string, int>("simple", SimpleMethod);
            Bind<StructArg, StructArg>("comp", ComplicatedMethod);
        }

        public void EmptyMethod() {
            Member++;
        }

        public void SimpleArgMethod(string arg) {
            arg = arg + arg;
        }

        public int SimpleReturnMethod() {
            return Member;
        }

        public string SimpleMethod(int number) {
            return number.ToString();
        }

        public StructArg ComplicatedMethod(StructArg arg) {
            arg.B = arg.A.ToString();
            return arg;
        }
    }


    [TestFixture()]
    public class ServiceBaseTests {
        [Test()]
        public void TestBaseFunctions() {
            if (Environment.GetEnvironmentVariable("MQTEST") != "true") return;
            var a = new StructArg();
            a.A = 10;
            a.B = "Ten";
            using (var t = new TestService()) {
                t.Start(true);
                t.RemoteCall("test_service", "empty");
                t.RemoteCall("test_service", "simple_arg", "lalal");
                t.RemoteCall("test_service", "simple_return");
                t.RemoteCall("test_service", "simple", 5);
                t.RemoteCall("test_service", "comp", a);


                t.RemoteRequest("test_service", "empty");
                t.RemoteRequest("test_service", "simple_arg", "lalal");
                int result1 = t.RemoteRequest<int>("test_service", "simple_return");
                Assert.IsTrue(result1 == 2);

                string result2 = t.RemoteRequest<string>("test_service", "simple", 5);
                Assert.IsTrue(result2 == "5");

                var result3 = t.RemoteRequest<StructArg>("test_service", "comp", a);
                Assert.IsTrue(result3.A == 10);
                Assert.IsTrue(result3.B == "10");

                var result4 = t.RemoteRequest<JToken>("test_service", "comp", a);
                Assert.IsTrue((int)((JObject)result4).GetValue("A") == 10);

                var result5 = t.RemoteRequest<JObject>("test_service", "comp", a);
                Assert.IsTrue((int)result5.GetValue("A") == 10);
            }
        }
    }
}