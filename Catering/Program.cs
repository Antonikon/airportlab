﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Platform.Network;
using Platform;
using GraphPath;

namespace Catering
{
    public class Test : BasicService
    {
        public Test():base("Test")
        {
            Bind<Tuple<int,int,int,int>>("LoadFood", SendAirplaneFood);
        }
        public void SendAirplaneFood(Tuple<int,int,int,int> food)
        {
            RemoteCall("Catering", "set_job", (food.Item1, food.Item2,food.Item3,food.Item4));
        }
    }
    public class Serv : BasicService
    {
        public static Pathfinder pf = new Pathfinder();
        Graph graph = new Graph(pf);
        HashSet<Catering> cars = new HashSet<Catering>();

        public Serv() : base("Catering") {//как обращаются другие
            Bind<Tuple<int, int, int, int>>("set_job", SetJob);

        }

        public void SetJob(Tuple<int,int,int,int> airplanefood)//функция,получающая парковочное место самoлёта и кол-во топлива
        {
            Catering c = new Catering(this,airplanefood.Item1,airplanefood.Item2,airplanefood.Item3,airplanefood.Item4);
            lock (cars)
            {
                cars.Add(c);
            }
            Log($"Creating catering car {c.id_car}");
        }
        
        public void Log(string message)
        {
            Console.WriteLine(message);
            RemoteCall("Log", "Write", message);
        }
        public void Run()
        {
            while (true)
            {
                List<Catering> removedCars = new List<Catering>();
                lock (cars)
                {
                    //DateTime current_time = DateTime.Now;
                    DateTime current_time = RemoteRequest<DateTime>("Time", "WhatTime");
                    foreach (var car in cars)
                    {
                        if (car.Type == 1) //если машинка в пути
                        {
                            car.UpdatePosition(current_time);
                        }
                        else if (car.Type == 2) //если ждет разрешения двигаться дальше (находится на узле)
                        {
                            car.MovePermission(current_time); //запрос к Службе управления наземным движением на разрешение двигаться дальше
                        }
                        else if (car.Type == 3) //если выполняет задание
                        {
                            if (car.Job_status == 1) car.StartWork(current_time);
                            else if (car.Job_status == 2) car.DoWork(current_time);
                            else car.CreatPathBack(); //создание обратного 
                        }
                        else if (car.Type == 5) //если вернулись в гараж
                        {
                            removedCars.Add(car);//если вернулись в гараж
                        }
                    }
                    foreach (var car in removedCars)
                    {
                        RemoteCall("GRAPHICS", "Kill", (car.id_car));
                        cars.Remove(car);
                        Log($"Catering car {car.id_car} is removed");
                    }

                }
                Thread.Sleep(100);
            }

        }
    }
    public class Catering
    {
        Serv serv;
        static int numcars;
        public readonly string id_car;
        int Food1;
        int Food2;
        int Food3;

        public int Type { set; get; }//тип движения: 1 - в пути, 2 - жду возможности ехать, 3 - выполняю задание, 5 - вернулись в гараж
        public int Job_status { set; get; } = 1;//состояние обработки задания: 1 - не начата, 2 - в процессе, 3 - завершено

        static int Garage = 19;

        int location = Garage;
        int nearest_coordinate;
        List<int> path= new List<int>();

        int Airplane;
        float Speed = 0.03f;

        DateTime start_time; //время начала пути из конкретного узла
        DateTime end_time; //время расчетного конца пути (достижения следующего узла)
        DateTime end_work; //время конца работы

        public Catering(Serv _serv,int airplane,int food1,int food2,int food3)
        {
            serv = _serv;

            numcars++;
            id_car = "c_" + numcars;
            Type = 2;

            Airplane = airplane;
            Food1 = food1;
            Food2 = food2;
            Food3 = food3;

            this.serv.Log($"Catering car {id_car} is ready to go to parking spot {Airplane} to load food types {Food1},{Food2},{Food3}");
            
            path = Serv.pf.GetRoad(location, Airplane);

            nearest_coordinate = path[0];
            SendPosition();
        }
        public void UpdatePosition(DateTime current_time)
        {
            if (current_time > end_time)
            {
                FreeRoad();
                MoveNext();
                SendPosition();
            }
            else
            {
                SendProgress(current_time); //если машинка в пути отправляем визуализатору прогресс
            }
        }

        public void MoveNext() //смена узла на следующий из списка пути
        {
            location = nearest_coordinate;
            if (path.Count <= 1)
            {
                //приехали
                if (Job_status != 3)
                {
                    Type = 3; //делаем работу
                }
                else
                {
                    Type = 5; //удаляемся
                    serv.Log("Removing car " + id_car);
                }
            }
            else
            {
                Type = 2;
                path.RemoveAt(0);
                nearest_coordinate = path[0];
                serv.Log($"Waiting for road from {location} to {nearest_coordinate}");
            }
        }

        public void SendProgress(DateTime current_time) //отправку визуализатору прогресса во время движения по ребру графа
        {
            float progress = (float)(current_time.Subtract(start_time).TotalSeconds / end_time.Subtract(start_time).TotalSeconds);
            serv.RemoteCall("GRAPHICS", "UpdatePosition", (id_car, location, nearest_coordinate, progress));
            //serv.Log($"Visualizer knows that {id_car} is on the edge of node {location} to {nearest_coordinate}, progress {progress}");
        }

        public void SendPosition() //отправка информации об остановке на узле визуализатору
        {
            serv.RemoteCall("GRAPHICS", "SetPosition", (id_car, location));
            serv.Log($"Visualizer knows that {id_car} is in node {location}");
        }

        public void FreeRoad() //освобождение дороги
        {
            serv.RemoteCall("ControlTransport", "FreeRoad", id_car);
            serv.Log("Freeing owned road");
        }

        public void MovePermission(DateTime currentTime) //ожидание разрешения на движение от Службы управления наземным движением
        {
            if (GroundMovementControl(true))
            {
                Type = 1;
                start_time = currentTime;
                end_time = start_time.AddSeconds(Serv.pf.GetRoadLength(location, nearest_coordinate) / Speed);
                serv.Log($"Catering car {id_car} has started moving form {location} to {nearest_coordinate}");
            }
        }

        private bool GroundMovementControl(bool type) //взаимодействие со Службой управления наземного движения
        {
            bool resolution; //ответ Службы управления наземного движения
            resolution = serv.RemoteRequest<bool>("ControlTransport", "SetRoad", (id_car, type, location, nearest_coordinate));
            return resolution;
            //return true;
        }

        public void CreatPathBack() //создание пути от Самолета до Гаража
        {
            path = Serv.pf.GetRoad(location, Garage);
            nearest_coordinate = path[0];
            Type = 2;
        }

        public void StartWork(DateTime current_time) //задача времени 
        {
            end_work = current_time.AddSeconds(360);//360
            Job_status = 2;
            serv.Log($"Catering car {id_car} is loading airplane on parking spot {Airplane} with {Food1},{Food2},{Food3} types of food");
        }

        public void DoWork(DateTime current_time) //выполнение работы
        {
            if (current_time >= end_work)
            {
                Job_status = 3;
                serv.Log($"Catering car {id_car} has loaded airplane on parking spot {Airplane} with {Food1},{Food2},{Food3} types of food");
                serv.RemoteCall("AGH", "FoodLoaded", Airplane);
                serv.Log($"Catering car {id_car} has informed Ground handeling about loading airplane on parking spot {Airplane} with {Food1},{Food2},{Food3} types of food");
            }
        }
    }
    class Program
    {
        
        static void Main(string[] args)
        {
            Config.IsLocal = false;
            
            Serv serv = new Serv();
            serv.Start(true);
            Task mainCycle = Task.Factory.StartNew(serv.Run);

            /*Test test = new Test();
            test.Start(true);
            Tuple<int, int, int, int> p= new Tuple<int, int, int, int>( 40, 1, 2, 3 );
            test.SendAirplaneFood(p);*/

            Console.WriteLine("Service is up!");
        }
    }
}
