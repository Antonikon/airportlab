﻿using System;
using System.Collections.Generic;
using System.Threading;
using Platform.Network;
using Platform;
using GraphPath;

namespace FollowMe2
{
    public class FollowMe_serv : BasicService
    {
        bool flag_gofromqueue = false; //разрешение выезжать из очереди не получено
        int parking_node;
        string flight;

        public static Pathfinder pathfinder = new Pathfinder();
        readonly Graph graph = new Graph(pathfinder);
        HashSet<FollowMe_car> cars = new HashSet<FollowMe_car>();

        public FollowMe_serv() : base("FollowMe")
        {
            Bind<Tuple<int, string>>("ParkToSlot", ParkToSlot);
            Bind<Tuple<int, string>>("EscortToTakeoff", EscortToTakeoff);
            Bind<string>("EscortToQueue", EscortToQueue);
            Bind<Tuple<int, string>>("EscortFromQueue", EscortFromQueue);

            Bind<Tuple<int, string>>("UnloadLuggage", UnloadLuggage);
            Bind<Tuple<int, string>>("LoadLuggage", LoadLuggage);

            Log("FollowMe service started");
        }

        public void ParkToSlot(Tuple<int, string> info) //задание на перевозку самолета с посадочной на парковку, в кортеже координата куда везти, номер рейса
        {
            FollowMe_car car = new FollowMe_car(this, info.Item1, info.Item2, 1);
            lock (cars)
            {
                cars.Add(car);
            }
            Log($"Creating luggage car {car.GUID}");
        }

        public void EscortToQueue(string flyet) //задание на перевозку самолета с посадочной полосы в очередь, в кортеже рейс
        {
            FollowMe_car car = new FollowMe_car(this, 2, flyet, 2);
            lock (cars)
            {
                cars.Add(car);
            }
            Log($"Creating luggage car {car.GUID}");
        }

        public void EscortToTakeoff(Tuple<int, string> info) //задание на перевозку самолета с парковки на взлетную полосу, в кортеже координата откуда везти (номер парковки), номер рейса
        {
            FollowMe_car car = new FollowMe_car(this, info.Item1, info.Item2, 3);
            lock (cars)
            {
                cars.Add(car);
            }
            Log($"Creating luggage car {car.GUID}");
        }

        public void EscortFromQueue(Tuple<int, string> info) //разрешение двигаться вместе с самолетом из очереди на парковку, в кортеже номер парковки, рейс
        {
            this.flag_gofromqueue = true;
            this.parking_node = info.Item1;
            this.flight = info.Item2;
        }   

        public void UnloadLuggage(Tuple<int, string> info) //задание на разгрузку самолета, в кортеже int aircraft_coordinate,, string flight
        {
            FollowMe_car car = new FollowMe_car(this, info.Item1, info.Item2, 1);
            lock (cars)
            {
                cars.Add(car);
            }
            Log($"Creating luggage car {car.GUID}");
        }

        public void LoadLuggage(Tuple<int, string> info) //задание на загрузку самолета, в кортеже int aircraft_coordinate, string flight, int luggage
        {
            FollowMe_car car = new FollowMe_car(this, info.Item1, info.Item2, 2);
            lock (cars)
            {
                cars.Add(car);
            }
            Log($"Creating luggage car {car.GUID}");
        }

        public void Log(string message)
        {
            Console.WriteLine(message);
            RemoteCall("Log", "Write", message);
        }

        public void Run()
        {
            if (!this.flag_gofromqueue)
            {
                while (true)
                {
                    List<FollowMe_car> removedCars = new List<FollowMe_car>();
                    lock (cars)
                    {
                        DateTime current_time = RemoteRequest<DateTime>("Time", "WhatTime");

                        foreach (FollowMe_car car in cars)
                        {
                            if (car.type == 1) //если машинка в пути
                            {
                                car.UpdatePosition(current_time);
                            }
                            else if (car.type == 2) //если ждет разрешения двигаться дальше (находится на узле)
                            {
                                car.MovePermission(current_time); //запрос к Службе управления наземным движением на разрешение двигаться дальше
                            }
                            else if (car.type == 3) //если выполняет задание
                            {
                                if (car.job_status == 1) car.StartWork(current_time);
                                else if (car.job_status == 2) car.DoWork(current_time);
                                else car.CreatPathBack(); //создание обратного 
                            }
                            else if (car.type == 5) //если вернулись в гараж
                            {
                                removedCars.Add(car);
                            }
                        }

                        foreach (var car in removedCars)
                        {
                            RemoteCall("GRAPHICS", "Kill", (car.GUID));
                            cars.Remove(car);
                        }

                    }
                    Thread.Sleep(100);
                }
            }
            else
            {
                lock (cars)
                {
                    foreach (FollowMe_car car in cars)
                    {
                        if (car.flight == this.flight)
                        {
                            car.destination_coordinate = this.parking_node;
                            car.go_from_parking_to_queue = true;
                        }
                    }
                }
                this.flag_gofromqueue = false;
            }
        }
    }

    class FollowMe_car
    {
        FollowMe_serv serv;

        float speed = 0.03f; //скорость: значение * единицы расстояния в секунду

        static int noOfObjects = 0; //номер созданной машины
        public readonly string GUID;
        public string flight; //рейс
        string GUID_plane; //гуид перевозимого самолета
        int plane_people;
        public bool go_from_parking_to_queue = false; //разрешение выезжать из очереди

        public int type_job; //тип задания: 1 - с посадочной на парковку, 2 - с посадочной в очередь, 3 - с парковки на взлетную
        public int type; //тип движения: 1 - в пути, 2 - жду возможности ехать, 3 - выполняю задание, 4 - на обратном пути, 5 - вернулись в гараж
        public int job_status = 1; //состояние обработки задания: 1 - не начала, 2 - в процессе, 3 - завершено

        public int type_pathpart = 1; //тип отрезка пути по type_job: 1 - гараж-посадочная/посадочная-стоянка(destination), 2 - гараж-посадочная/посадочная-очередь/очередь-стоянка, 3 - гараж-стоянка/стоянка-взлетная

        int location; //узел графа, на котором находится машина в настоящий момент
        int nearest_coordinate; //узел графа, следующий на пути
        public int destination_coordinate; //место назаначения по type_job: 1 - стоянка, на которую отвезти, 2 - очередь, 3 - стоянка, с которой забрать)
        readonly int queue_coordinate = 38; //координата очереди
        readonly int garage_coordinate = 19; //координата гаража
        readonly int takeoff_coordinate = 1; //координата взлетной полосы
        readonly int landing_coordinate = 2; //координата посадочной полосы

        List<int> path; //путь машинки - список узлов графа

        DateTime start_time; //время начала пути из конкретного узла
        DateTime end_time; //время расчетного конца пути (достижения следующего узла)
        DateTime end_work; //время конца работы

        public FollowMe_car(FollowMe_serv _serv, int _destination_coordinate, string _flight, int _type_job)
        {
            serv = _serv;

            noOfObjects++;
            GUID = "f_" + noOfObjects;
            flight = _flight;

            type_job = _type_job;
            type = 2;

            location = 19; //19 - узел гаража
            destination_coordinate = _destination_coordinate;

            if (type_job == 1 || type_job == 2) path = FollowMe_serv.pathfinder.GetRoad(location, this.landing_coordinate);
            else path = FollowMe_serv.pathfinder.GetRoad(location, this.destination_coordinate);

            nearest_coordinate = path[0];

            if (this.plane_people == 0) SendPosition();
            else SendPositionLoad();
        }

        public void UpdatePosition(DateTime current_time)
        {
            if (current_time > end_time)
            {
                FreeRoad();
                MoveNext();
                if (this.plane_people == 0) SendPosition();
                else SendPositionLoad();
            }
            else
            {
                if (this.plane_people == 0) SendProgress(current_time); //если машинка в пути отправляем визуализатору прогресс
                else SendProgressLoad(current_time);                
            }
        }

        public void MoveNext() //смена узла на следующий из списка пути
        {
            location = nearest_coordinate;
            if (path.Count <= 1)
            {
                //приехали
                if (job_status != 3)
                {
                    type = 3; //делаем работу
                }
                else
                {
                    type = 5; //удаляемся
                    serv.Log("Removing car " + this.GUID);
                }
            }
            else
            {
                type = 2;
                path.RemoveAt(0);
                nearest_coordinate = path[0];
                serv.Log($"Waiting for road from {location} to {nearest_coordinate}");
            }
        }

        public void SendProgress(DateTime current_time) //отправку визуализатору прогресса во время движения по ребру графа
        {
            float progress = (float)(current_time.Subtract(start_time).TotalSeconds / end_time.Subtract(start_time).TotalSeconds);
            serv.RemoteCall("GRAPHICS", "UpdatePosition", (this.GUID, this.location, this.nearest_coordinate, progress));
        }

        public void SendPosition() //отправка информации об остановке на узле визуализатору
        {
            serv.RemoteCall("GRAPHICS", "SetPosition", (this.GUID, this.location));
        }

        public void SendProgressLoad(DateTime current_time) //отправку визуализатору прогресса во время движения по ребру графа
        {
            float progress = (float)(current_time.Subtract(start_time).TotalSeconds / end_time.Subtract(start_time).TotalSeconds);
            serv.RemoteCall("GRAPHICS", "UpdatePositionLoad", (this.GUID, this.location, this.nearest_coordinate, progress, this.plane_people));
        }

        public void SendPositionLoad() //отправка информации об остановке на узле визуализатору
        {
            serv.RemoteCall("GRAPHICS", "SetPositionLoad", (this.GUID, this.location, this.plane_people));
        }

        public void FreeRoad() //освобождение дороги
        {
            serv.RemoteCall("ControlTransport", "FreeRoad", GUID);
            serv.Log("Freeing owned road");
        }

        public void MovePermission(DateTime currentTime) //ожидание разрешения на движение от Службы управления наземным движением
        {
            if (GroundMovementControl(true))
            {
                type = 1;
                start_time = currentTime;
                end_time = start_time.AddSeconds(FollowMe_serv.pathfinder.GetRoadLength(location, nearest_coordinate) / speed);
                serv.Log($"Start moving form {location} to {nearest_coordinate}");
            }
        }

        private bool GroundMovementControl(bool type) //взаимодействие со Службой управления наземного движения
        {
            bool resolution; //ответ Службы управления наземного движения
            resolution = serv.RemoteRequest<bool>("ControlTransport", "SetRoad", (this.GUID, type, this.location, this.nearest_coordinate));
            return resolution;
        }

        public void CreatPathBack() //создание пути
        {
            if (type_job == 1) //посадочная --> стоянка(destination)
            {
                job_status = 3;
                type_pathpart = 2;
                path = FollowMe_serv.pathfinder.GetRoad(location, this.destination_coordinate);
                nearest_coordinate = path[0];
                
                type = 2;
            }
            if (type_job == 2)
            {
                if (type_pathpart == 2) //посадочная-очередь --> очередь-стоянка
                {
                    job_status = 3;
                    type_pathpart = 3;
                    path = FollowMe_serv.pathfinder.GetRoad(location, this.destination_coordinate);
                    nearest_coordinate = path[0];
                }
                if (type_pathpart == 1) //гараж-посадочная --> посадочная-очередь
                {
                    job_status = 2;
                    type_pathpart = 2;
                    path = FollowMe_serv.pathfinder.GetRoad(location, this.queue_coordinate);
                    nearest_coordinate = path[0];
                }
                type = 2;
            }
            if (type_job == 1) //стоянка --> взлетная
            {
                job_status = 3;
                type_pathpart = 2;
                path = FollowMe_serv.pathfinder.GetRoad(location, this.landing_coordinate);
                nearest_coordinate = path[0];

                type = 2;
            }
        }

        public void StartWork(DateTime current_time) //задача времени 
        {
            end_work = current_time.AddSeconds(360);
            job_status = 2;
            serv.Log($"Start doing work for {this.GUID}");
        }

        public void DoWork(DateTime current_time) //выполнение работы
        {
            if (type_job == 1) //отвозим самолет на стоянку с посадочной
            {
                if (type_pathpart == 1) //забрать самолет с посадочной
                {
                    job_status = 3;

                    var tuple = serv.RemoteRequest<Tuple<string, int, int>>("Plane", "TakePlane", this.flight); //забираю самолет c посадочной
                    this.GUID_plane = tuple.Item1;
                    this.plane_people = tuple.Item3; //количество человек для визуализатора

                    serv.Log($"FollowMe take plane {this.flight} from landing strip for {this.GUID}");
                }

                if (type_pathpart == 2) //везти самолет на стоянку
                {
                    job_status = 3;

                    serv.RemoteCall("Plane", "LeavePlane", new Tuple<int, string>(this.location, this.flight)); //оставляю самолет на парковке
                    this.plane_people = 0;
                    serv.RemoteCall("AGH","PlaneParked", this.location);
                    serv.Log($"FollowMe leave plane {this.flight} on parking {this.location} for {this.GUID}");
                }

            }
            if (type_job == 2) //везу самолет с посадочной в очередь (а затем на парковку)
            {
                if (type_pathpart == 1) //забрать самолет с посадочной
                {
                    job_status = 3;

                    var tuple = serv.RemoteRequest<Tuple<string, int, int>>("Plane", "TakePlane", this.flight); //забираю самолет c посадочной
                    this.GUID_plane = tuple.Item1;
                    this.plane_people = tuple.Item3; //количество человек для визуализатора

                    serv.Log($"FollowMe take plane {this.flight} from landing strip for {this.GUID}");
                }
                if(type_pathpart == 2) //стоять в очереди
                {
                    if (this.go_from_parking_to_queue)
                    {
                        job_status = 3;
                    }
                }
                if (type_pathpart == 3) //привезла из очереди на парковку
                {
                    job_status = 3;

                    serv.RemoteCall("Plane", "LeavePlane", new Tuple<int, string>(this.location, this.flight)); //оставляю самолет на посадочной
                    this.plane_people = 0;
                    serv.RemoteCall("AGH", "PlaneParked", this.location);
                    serv.Log($"FollowMe leave plane {this.flight} on landing strip for {this.GUID}");
                }
            }
            if (type_job == 3) //везу самолет со стоянки на взлетную
            {
                if (type_pathpart == 1) //забрать самолет со стоянки
                {
                    job_status = 3;

                    var tuple = serv.RemoteRequest<Tuple<string, int, int>>("Plane", "TakePlane", this.flight); //забираю самолет cо стоянки
                    this.GUID_plane = tuple.Item1;
                    this.plane_people = tuple.Item3; //количество человек для визуализатора
                    serv.RemoteCall("AGH", "Taken", this.location);

                    serv.Log($"FollowMe take plane {this.flight} from parking {this.location} for {this.GUID}");
                }

                if (type_pathpart == 2) //везти самолет на взлетную
                {
                    job_status = 3;

                    serv.RemoteCall("Plane", "LeavePlane", new Tuple<int, string>(this.location, this.flight)); //оставляю самолет на взлетной
                    this.plane_people = 0;

                    serv.RemoteCall("AGH", "ReadyToTakeoff", this.flight);
                    serv.Log($"FollowMe leave plane {this.flight} on take-off strip for {this.GUID}");
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Config.IsLocal = false;
            Config.RequestTimeout = 0;
            FollowMe_serv d = new FollowMe_serv();
            d.Start(true);
            d.Run();
        }
    }
}
