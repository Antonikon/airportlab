﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Platform.Network;

namespace Waitingroom
{
    class Program
    {
        public static List<Person> WaitingInAP = new List<Person>();
        public static int LuggageMass = 0;

        static void Main(string[] args)
        {
            Config.IsLocal = false;
            WaitingroomService service = new WaitingroomService();
            service.Start(true);
            try
            {
                WaitingInAP = service.RemoteRequest<List<Person>>("Passenger", "SendToWaitingroom");
                string message = "Waitingroom is now filled with " + WaitingInAP.Count + " People";
                service.Log(message);
            }
            catch
            {
                string message = "Person is offline";
                service.LogProblem(message);
            }

            while (true)
            {
                List<int> toRemove = new List<int>();

                for (int i = 0; i < WaitingInAP.Count; i++)
                {
                    if ((WaitingInAP[i].state == 4) && (WaitingInAP[i].luggage == 0))
                    {
                        service.RemoteCall("Passenger", "PassengerReceive", WaitingInAP[i]);
                        toRemove.Add(i);
                    }
                }
                for (int i = toRemove.Count - 1; i >= 0; i--)
                {
                    WaitingInAP.RemoveAt(toRemove[i]);
                }
                Thread.Sleep(1);
            }
        }

        public class WaitingroomService : BasicService
        {
            public WaitingroomService() : base("Waitingroom")
            //Указываем имя сервиса, по которому к нему будут обращаться
            {
                Bind<List<Person>>("ReceivePassengers", Receive);
                Bind<List<string>, string>("SendPassengers", SendPass);
                Bind<int, string>("SendLugg", SendLugg);
                Bind<List<string>>("LoadPass", LoadPass);
                Bind<int>("LoadLugg", LoadLugg);
            }

            public int SendLugg(string s)
            {
                int mass = 0;
                for (int i = 0; i < WaitingInAP.Count; i++)
                {
                    if (WaitingInAP[i].flight == s)
                    {
                        mass+=WaitingInAP[i].luggage;
                        WaitingInAP[i].luggage = 0;
                    }
                }

                string message = "Waitingroom gave " + mass + " kg of luggage to Luggage Transporter for flight " + s;
                Log(message);

                return mass;
            }

            public List<string> SendPass(string s)
            {
                List<string> send = new List<string>();
                for (int i = 0; i < WaitingInAP.Count; i++)
                {
                    if ((WaitingInAP[i].flight == s) && (send.Count < 30) && (WaitingInAP[i].state != 4))
                    {
                        send.Add(WaitingInAP[i].guidPerson);
                        WaitingInAP[i].state = 4;
                    }
                }

                string message = "Waitingroom gave " + send.Count + " Passengers to bus for flight " + s;
                Log(message);
                return send;
            }

            public void LoadLugg(int i)
            {
                string message = "Waitingroom received " + i + " kg of luggage from Luggage Transporter";
                Log(message);
                int lugg = i;
            }

            public void LoadPass(List<string> l)
            {
                string temp = l[0];
                string message = "Waitingroom received " + l.Count + " Passengers from Bus";
                Log(message);
                RemoteCall("Passenger", "Clear", temp);
            }

            public void Receive(List<Person> l)
            {
                WaitingInAP = new List<Person>(l);
                string message = "Waitingroom is now filled with " + WaitingInAP.Count + " People";
                Log(message);
            }

            public void Log(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "Write", message);
            }

            public void LogProblem(string message)
            {
                Console.WriteLine(message);
                RemoteCall("Log", "AddLog", new Tuple<int, string>(1, message));
            }
        }

        public class Person //пассажир
        {
            public string name; //Name and Surname "John Brown"
            public string guidPerson; //generated GUID for person
            public string flight; //number of flight
            public int luggage; //laguage mass
            public int state; //1 - no boarding pass, 2 - has a boarding pass, 3 - chill zone, 4 - in the buss, 5 - on plane
            public int food; //1 - normal, 2 - vegan, 3 - McDonalds

            public Person()
            {
                Random rnd = new Random((int)DateTime.Now.Ticks);
                name = GenerateName(rnd.Next(3, 5)) + " " + GenerateName(rnd.Next(5, 7));
                flight = "";
                guidPerson = "passenger_" + Guid.NewGuid().ToString();
                luggage = rnd.Next(1, 35);
                state = 1;
                food = rnd.Next(1, 4);
            }

            string GenerateName(int len)
            {
                Random r = new Random((int)DateTime.Now.Ticks);
                string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "t", "v", "w", "x" };
                string[] vowels = { "a", "e", "i", "o", "u", "e", "y" };
                string Name = "";
                Name += consonants[r.Next(consonants.Length)].ToUpper();
                Name += vowels[r.Next(vowels.Length)];
                int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
                while (b < len)
                {
                    Name += consonants[r.Next(consonants.Length)];
                    b++;
                    Name += vowels[r.Next(vowels.Length)];
                    b++;
                }
                return Name;
            }
        }
    }
}
